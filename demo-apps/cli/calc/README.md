# calc-cli

This is the simplest demo of a prun applicaiton.

It does not work with any external services or write any files.

All it does process simple arithmetic calculations provided by command parameters.

it uses the calc-lib package (from `./fixtures/calc-lib`) as the BL layer, however, the BL does not have to be a package, it can be a module that is local to the project, like `lib/cmd.js` and `bin/calc.js` are.

## Demonstrated conventions

The convetions in this project are:
 - **modules form:** all modules in ESM form (explicit `"type":"module"` in `package.json`)
 - **structure:**
   - all files in the root are config or misc files.
   - code files should be in `lib`, `bin`, or `test`, depending on their role.
 - **tests setup:** bare-bones `node:test` and `node:assert`
 - **test strategy:** e2e-first
 - **coverage setup:** 100% test coverage using `c8`
 - **lint setup:** as a part of the monorepo
 - **execution style:** adds its own CLI command, and can be shipped to npm (see `"bin"` section in `package.json`)

# install

If you are consuming it from npm - then you would:

`npm i -g @prun/demo-calc-cli pino-prettier` 

# Usage

It is advised to `npm i -g pino-prettier` , as `pino-prettier` is used to beautify the json default format of the stdout.

## addition
command: `calc add 19 21 2 | pino-prettier`;
expect:
```
INFO [17:30:51.958]: calc-cli : starting up
INFO [17:30:51.965]: calc-cli : calc results
    args: [ 19, 21, 2 ]
    result: 42
```

## factoring
command: `calc factor 2 3 7 | pino-prettier`;
expect:
```
INFO [17:30:51.958]: calc-cli : starting up
INFO [17:30:51.965]: calc-cli : calc results
    args: [ 2, 3, 7 ]
    result: 42
```
