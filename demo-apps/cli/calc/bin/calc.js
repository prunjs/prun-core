#!/usr/bin/env node

// HACK:
//  user code should not be have to deal with the implementation details below.
import { fileURLToPath } from 'node:url';
import { join } from 'node:path';

async function runWithConfig(configRelativeTo, meta, process = global.process) {
  const currentModule = fileURLToPath(meta.url);
  const prunrc = join(currentModule, configRelativeTo);

  const configSwitch = process.argv.indexOf('--config');
  if (configSwitch !== -1) throw new Error('unsupported switch: --config');

  process.argv.push('--config', prunrc);

  await import('@prun/prun/bin/cli.js');
}
//  this should move into the @prun/prun package.
//  we want to wrap it with a more polished API, like:
//no-hack:
// import { runWithConfig } from '@prun/prun/..somewhere??../cli'
//HACKEnd

await runWithConfig('../.prunrc', import.meta);
