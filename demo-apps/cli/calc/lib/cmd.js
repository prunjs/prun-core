export default ({ opp, args, ...rest }, { logger, calc }) => {
  const log = logger.of('cmd');
  log.debug({ opp, args, rest }, 'loaded');

  return {
    compute() {
      log.info('running compute', { opp, args });

      const result = calc[opp](...args);

      log.info('result obtained', { result });

      //TRICKY: we dont' want the logger of the command,
      //        we want the main logger
      //TBD: do we want this here, or as a part of `prun/lib/usr-app/runner`?
      logger.info({
        args,
        result,
      }, 'calc results');

      return result;
    },
  };
};

export function validate(options, {
  config: { cliArgs },
  bus,
  errors: { calcError },
}) {
  const [opp, ...args] = cliArgs;
  const badArgs = args.filter(isNaN);

  const reject = !opp && 'What would you like to calculate?'
    || !args.length && 'No arguments found for the operation'
    || badArgs.length && 'One or more non numeric arithmetic arguments detected';

  if (reject) throw calcError(reject, { opp, cliArgs, badArgs });

  //TRICKY: the calc module is not initiated in that time,
  // because modules initiations start after all config validations pass.
  bus.on('module-initiated:calc', ({ module: calc }) => {
    if (!calc[opp]) throw calcError('operation not supported by internal calc module', { opp, args });
  });

  return {
    ...options,
    opp,
    args,
  };
}

export function errors({ errors: { PrunError } }) {
  const description = [
    'Synopsis: prun <opp> [...<args>]',
    '  opp: mandatory, arithmetic operation. Supports: add, factor',
    '  args: mandatory, one or more numeric arguments',
  ];

  return [
    class CalcError extends PrunError {
      constructor(reject, meta) {
        super(reject, { description, ...meta, reject });
      };
    },
  ];
}
