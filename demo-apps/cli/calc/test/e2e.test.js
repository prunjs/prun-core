import assert from 'node:assert';

import { before, describe, it } from 'node:test';
import { inspect, promisify } from 'node:util';
import { exec } from 'node:child_process';


const run = promisify(exec);

describe('calc-cli', () => {
  describe('when trying to inject your own config', () => {
    const ctx = setupCase({ args: ['--config', 'my/file'] });

    it('should exit with error asking what the user wants to compute', () => {
      assert.match(ctx.stderr, /unsupported switch/i);
    });
  });

  describe('when called with no operation', () => {
    const ctx = setupCase({ args: [] });

    it('should exit with error asking what the user wants to compute', () => {
      assert.match(ctx.stdout, /what would you like to calculate/i);
    });
  });

  describe('when called with operation that is not supported', () => {
    const ctx = setupCase({ args: ['no-such', '1', '2'] });

    it('should exit with error asking what the user wants to compute', () => {
      assert.match(ctx.stdout, /operation not supported/i);
    });
  });

  describe('when called with no arguments', () => {
    const ctx = setupCase({ args: ['add'] });

    it('should exit with error asking what the user wants to compute', () => {
      assert.match(ctx.stdout, /no arguments found for the operation/i);
    });
  });

  describe('when called with a non-numeric argument', () => {
    const ctx = setupCase({ args: ['add', 'two', '2'] });

    it('should exit with error asking what the user wants to compute', () => {
      assert.match(ctx.stdout, /non numeric arithmetic arguments/i);
    });
  });

  describe('when called operation: add and valid numeric arguments', () => {
    const ctx = setupCase({ args: ['add', '19', '21', '2'] });

    it('should not fail', () => shouldNotFail(ctx));
    it('should state the result', () => {
      assert.match(ctx.stdout, /result: 42/i);
    });
  });

  describe('when called operation: factor and valid numeric arguments', () => {
    const ctx = setupCase({ args: ['factor', '2', '3', '7'] });
    it('should not fail', () => shouldNotFail(ctx));
    it('should state the result', () => {
      assert.match(ctx.stdout, /result: 42/i);
    });
  });
});

function setupCase({ args }) {
  const ctx = { args };
  const cmd = `node bin/calc.js ${args.join(' ')}`.trim();

  before(async () => {
    try {
      const { stdout, stderr } = await run(cmd);
      Object.assign(ctx, { stderr, stdout });
    } catch (err) {
      ctx.stderr = err.stderr;
      ctx.stdout = err.stdout;
      ctx.err = err;
    }
  });

  return ctx;
}
function shouldNotFail({ err }) {
  err && Should.fail(
    inspect(Object.assign(err.stack.split('\n'), err)).slice(1, -1),
    ' <expected no error> ',
    'command should not fail',
  );
}
