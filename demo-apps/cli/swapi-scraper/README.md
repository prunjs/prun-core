# swapi-scraper

This is a CLI demo of a prun CLI job. The job queries data from
[Star-Wars API](https://swapi.dev/).

## Features Summary
 - User passes a query regarding to what root entities to traverse from
 - By default, data is presented in stdout in yaml, however user can give a path
   to write to
 - User can specify a depth for link-traversing down the entity graph
 - Scraped data is cached locally to be resued in future queries
 - User can ask to force-update local store of queried entities, or use only
   cache

## Demonstrated Conventions

The convetions demontrated in this project are:

 - **modules form:** mixed mode of ESM (mjs) and CommonJS (cjs). File ending 
   determines its format (no `"type"` in `package.json`).
 - **structure:**
   - all files in the root are config or misc. files. 
   - code files should be in `lib`, `bin`, depending on their role.- unit-tests
     in spec files next to the source files
 - **tests setup:** `mocha`, `should` and `@prun/bdd-helper`
 - **test strategy:** mixed. Some coverage relays on e2e, some on unit-tests.
 - **lint setup:** as a part of the monorepo
 - **execution style:** ran from the project directory using `prun` (not shipped
   to npm, and does not introduces CLI commands).

# usage
## install

If you do not wish to run `npm i` in the root of the repo, you may run `npm i`
in this project directory.

## API
Synopsis:
   ```
   prun [...switches for prun] -- [--local|--update] [--all|--depth <n:number>] [--out <filePath>] <queries>`
   ```

### Queries arguments

All cli argument that are not associated with a cli-switch act as the set of
queries, no matter where they appear.

The `<queries>` expression:
 - mandatory - running without any query produces an error.
 - built of one or more `<query>` expressions.

A `<query>`:
 - is in the form of `<entity>[/<filter>]`.
 - supported entities: `people|starships|vehicles|films|species|planets|all`.

A `<filter>`:
 - is in the form of `<field>=<predicate>` or `<field>=~<predicate>`
 - a totally numeric `filter` is understood as an `id=<predicate>` filter
 - `=` is for equality, `=~` is for case-insensitive match
 
### CLI switches
Switches for the scraper app should be provided after `--` to differentiate them from switches to `prun` itself.

  - `--depth <n:number>`, number - number of generations from the point 
    of view of queried entities. Entities that are `n` steps or less from 
    the queried entities - should be rendered as objects when they are found. 
    The rest should be rendeed as queries. Mutually exclusive with `--all`.
    (default: `2`)
  - `--all`, boolean - effect is like `--depth 99`. (default: `false`)
  - `--max`, number - applicable only when query does not specify ids. Specifies
    the maximum root entities. Does not affect connections, linked entitis are
    limited using `--depth`. Mutually exclusive with `--depth`.
    (default: `Infinity`)
  - `--out` path to the output file for the result. (default: `-`, i.e - stdout)
  - `--local`, boolean - do not fetch from upstream entities that are not already
    found in local store. Mutially exclusive with `--update`. (default: `false`)
      - linked entities that ARE found in local store should be appended
      - linked entities that are NOT found in local store - should be mentioned
        as a `<query>` expression, even if they are logically within the stated 
        `--depth`
  - `--update`, boolean - do not use cache, even if it is populated.
    Mutually exclusive with `--local`. (default: `false`)

### Examples
```sh
# list all species without any further traversing
prun -- species --depth 1

# the profile of Luke Skywalker and any of his directly linked entities,
#  ignoring any present cached data
prun -- people/1 --depth 2 --update

# the first 3 planets with 'n' in the name (Naboo, Endor, and Coruscant)
#  relaying only on data from scraped local cache store
prun -- planets/name=~n --local --max 3

# if no switches are used, then `--` is not necessary
# list all species with default traversal depth
prun species/1
```
