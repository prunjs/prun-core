import { readFile, writeFile } from 'node:fs/promises';

import globalYaml from 'js-yaml';

export default (
  { savePath },
  { logger, yaml = globalYaml },
) => {
  const log = logger.of('yaml-port');
  log.info('initializing');

  return {
    load: async (source = savePath) => {
      log.info({ source }, 'loading from yaml');

      const buf = await readFile(source);
      log.debug({ sizeInBytes: buf.length }, 'obtained buffer from file');

      const data = yaml.load(buf);
      log.debug('parsed successfully');

      return data;
    },
    save: async (data, target = savePath) => {
      log.info({ target }, 'dumping to yaml');

      const str = yaml.dump(data);
      log.debug('stringified successfully');

      await writeFile(target, str);
      log.debug('saved successfully');
    },
  };
};

export const validate = (options, { errors: { /*configError*/ } }) => {
  //TBD: savePath should be found on disk with write permissions
  options.savePath;
};
