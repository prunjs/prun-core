# prun / bdd-helper
A test helper for runners with bdd ui (describe, before, afterAll, it).

It's tested with `mocha` and `vitest`.

**NOTE:**
It should work with `jest`, however, I'm having problems running `jest` with vanilla JS and ESM (TBD).
