export default {
  verbose: true,
  testMatch: [
    '**/test/**/*.test.js',
    '**/test/**/*.e2e.js',
  ],
  setupFiles: ['<rootDir>/test/util/setup-jest.js'],
  transform: {},
};
