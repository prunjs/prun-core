/* global it, describe */
import { inspect } from 'node:util';
import { join } from 'node:path';
import { readFileSync } from 'node:fs';

import Should from 'should';
import evalPath from 'lodash/get.js';

const { version } = JSON.parse(readFileSync(join(import.meta.dirname, '../package.json')));

const claim = (v, opts = undefined) => { //eslint-disable-line no-undefined
  if ('string' == typeof opts) opts = { prop: opts };
  return Object
    .entries(claim)
    .filter(entry => 'function' == typeof entry[1])
    .reduce(
      (bound, [claimOption, f]) => Object.assign(bound, {
        [claimOption]: (...a) => {
          if (opts) a.push(opts);
          return f(v, ...a);
        },
      }),
      {},
    );
};

Object.assign(claim, {
  isFunction,
  shouldNotFail,
  isPrunFactory,
  hasApi,
  hasMethods: hasApi,
  hasProps,
  behaviors,
  behavior,
  version,
});

export default claim;

function getSUT(val, prop) {
  return prop
    ? evalPath(val, prop)
    : val;
}

function isFunction(val, arity, { prop } = {}) {
  const SUT = getSUT(val, prop);
  const assert = Should(SUT).be.a.Function();
  if ('number' == typeof arity) assert.have.property('length', arity);
}

function shouldNotFail(ctx) {
  it('should not fail', () => {
    if (!ctx.err) return;

    throw Object.assign(
      new Error('Unexpected Error instance'),
      {
        code: 'ERR_ASSERTION',
        expected: '',
        actual: JSON.stringify({
          name: ctx.err.name,
          stack: String(ctx.err.stack ?? '').split('\n'),
          ...ctx.err,
        }, null, 2),
      },
    );
  });
}

function isPrunFactory(
  val,
  { arity = 2, hasValidator = false, hasCustomErrors = false, mapsEnv = false } = {},
  { prop } = {},
) {
  if (arity < 0 || arity > 2) {
    throw new Error('bdd/isPrunFactory - arity of prun factories may be between 0 to 2');
  }
  //TBD: input checks for  mapsEnv:bool|Array<strOptionName>

  const title = arity //eslint-disable-line no-nested-ternary
    ? arity === 1
      ? 'should be an prun factory function that expects only (ioc)'
      : 'should be an prun factory function that expects (options, ioc)'
    : 'should be an prun factory function that doesnt expect arguments';

  it(title, () => {
    isFunction(val.default || val, arity, { prop });
  });

  if (hasValidator) {
    it('should expose a .validate function', () => {
      const SUT = getSUT(val, prop);
      isFunction(SUT, null, { prop: 'validate' });
    });
  }

  if (hasCustomErrors) {
    it('should expose a .errors factory function', () => {
      const SUT = getSUT(val, prop);
      isFunction(SUT, null, { prop: 'errors' });
    });
  }

  if (mapsEnv) {
    it('should expose an .env map', () => {
      const SUT = getSUT(val, prop);
      Should(SUT).have.property('env');
    });
    if (mapsEnv !== true) {
      describe('.env mappers:', () => {
        const SUT = getSUT(val, prop);

        //TBD: support mapsEnv as an object?

        mapsEnv.forEach(option => {
          it(`should map options.${option}`, () => {
            Should(SUT).have.property('env')
              .have.property(option);
          });
        });
      });
    }
  }
}

/**
  @param {any} val - the SUT or an object that will hold the SUT
  @param {Array|Object} api - a list of methods, or a map of method-name : arity
  @param {String} [options.prop] - name of property to evaluate on SUT on test time
  @returns {undefined}
 */
function hasApi(val, api, { prop } = {}) {
  if (Array.isArray(api)) api = api.reduce((api, name) => Object.assign(api, { [name]: true }), {});
  Object.entries(api).forEach(([api, arity]) => {
    const title = 'number' == typeof arity
      ? `should have api: .${api}(${arity})`
      : `should have api: .${api}()`;
    it(title, () => {
      const SUT = getSUT(val, prop);
      Should(SUT).have.property(api);
      isFunction(SUT, arity, { prop: api });
    });
  });
}

function hasProps(val, api, { prop, path = [] } = {}) {
  if (prop && !path.length) path.push(prop);
  if (Array.isArray(api)) api = api.reduce((api, name) => Object.assign(api, { [name]: 'any' }), {});
  Object.entries(api).forEach(([api, type]) => {
    const isStringOrRegex = type => 'string' == typeof type || type instanceof RegExp;
    const apiPath = [...path, api].join('.');
    const title = 'any' === type //eslint-disable-line no-nested-ternary
      ? `should have property: .${apiPath}`
      : isStringOrRegex(type)
        ? `should have property: .${apiPath}, ${'string' == typeof type ? `as "${type}"` : `matching ${type}`}`
        : `should have property: .${apiPath}, as ${type.name}`;
    if ('object' == typeof type && !(type instanceof RegExp)) {
      //TRICKY: the difference between prop and path:
      //  - prop - subpath on SUT evaluated on test time
      //  - path - used for recursive definitions of required props
      hasProps(val, type, { prop, path: path.concat([api]) });
      return;
    }

    it(title, () => {
      const SUT = getSUT(val, path.join('.'));
      const myPath = path.length ? [''].concat(path).join('.') : '';
      if (!SUT) Should.fail(null, null, `expected ${inspect(val)} to have property ${myPath}`);
      if (!(api in SUT)) Should.fail('<missing!>', apiPath, `expected SUT${myPath} to have property ${api}`);
      const assert = Should(SUT[api]);//.have.property(api);
      if ('any' === type) return null;

      if ('string' == typeof type) return assert.type(type);

      if (type instanceof RegExp) return assert.match(type);

      if ('function' == typeof type) return assert.instanceof(type);

      return assert.eql(type);
    });
  });
}

function behaviors(val, cases, opts) {
  cases.forEach(({ title, ...oCase }) => {
    describe(title, () => {
      behavior(val, oCase, opts);
    });
  });
}

function behavior(
  val,
  {
    options = {},
    ioc = {},
    before: setup,
    awaiting,
    ready,
    after: teardown,
    args = [options, ioc],
    expect: { reject, result, type, api, props, describe: and } = {},
  },
  { prop } = {},
) {
  const {
    beforeAll,
    before = beforeAll,
    afterAll,
    after = afterAll,
    it,
  } = global;

  const ctx = {
    options,
    ioc,
    args,
  };

  before(async () => {
    const SUT = ctx.SUT = getSUT(val, prop);
    isFunction(SUT);
    if ('function' == typeof setup) setup(ctx);
    try {
      ctx.result = await SUT(...args);
    } catch (e) {
      ctx.err = ctx.result = e;
    }
    if ('function' == typeof awaiting) awaiting(ctx);

    if ('function' == typeof ready) ready(ctx);
  });

  switch (true) { //eslint-disable-line default-case
  case reject instanceof RegExp:
    it(`should result with an error with message like ~${reject}`, () => {
      Should(ctx.err)
        .be.an.Error()
        .have.property('stack')
        .match(reject);
    });
    break;
  case 'string' == typeof reject:
    it(`should result with an error with message that includes: ${reject}`, () => {
      Should(ctx.err)
        .be.an.Error()
        .have.property('stack')
        .containEql(reject);
    });
    break;
  case !reject:
    shouldNotFail(ctx);
    break;
  }

  if (type || api || props || result || and) {
    describe('the result', () => {
      if (type) it(`should be of type ${type}`, () => Should(typeof ctx.result).eql(type));

      if (api || props) {
        describe('form', () => {
          api && claim(ctx, 'result').hasApi(api);
          props && claim(ctx, 'result').hasProps(props);
        });
      }

      if (result) toResultSuite(result);
    });

    if ('function' == typeof and) describe('and', () => and(ctx));
  }

  if ('function' == typeof teardown) after(teardown);

  return ctx;

  function toResultSuite(result) {
    Object.entries(result).forEach(([title, test]) => {
      if ('function' == typeof test) {
        it(title, () => test(ctx.result));
        return;
      }

      if (!test || 'string' == typeof test) {
        it(test ? `${title} >> ${test}` : title);
        return;
      }

      describe(title, () => {
        toResultSuite(test);
      });
    });
  }
}
