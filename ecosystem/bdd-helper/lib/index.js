import claim from './claim.js';
import iocFactory from './ioc.js';
import loggerFactory from './logger.js';

export const logger = loggerFactory();

export { default as claim } from './claim.js';
export { default as iocFactory } from './ioc.js';
export { default as loggerFactory } from './logger.js';

export default {
  claim,
  loggerFactory,
  logger,
  iocFactory,
};
