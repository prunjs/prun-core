import busFactory from '@prun/prun/lib/core/bus.js';
import errorsFactory from '@prun/prun/lib/core/errors/index.js';
import hrtimeFactory from '@prun/prun/lib/core/hrtime.js';

import loggerFactory from './logger.js';

export default ({
  errors = [],
  config: {
    isTty = false,
    cwd = '.',
    name = 'mock-pkg',
    version = '1.2.3',
    pkg = { name, version },
    ...config
  } = {},
  mock = {},
  mocks = mock,
  ...rest
} = {}) => {
  const context = {
    watch: f => console.log('mockCtx.watch') || f(),
    populateValuesFromContextKeys: contextToHeaders => Object
      .entries(contextToHeaders)
      .reduce((map, [mappedName, contextKey]) => {
        const value = context[contextKey];
        if (value !== undefined && value !== null) map[mappedName] = value; // eslint-disable-line no-undefined
        return map;
      }, {}),
  };

  return {
    // - core components -
    process,
    errors:     errorsRegistry(errors),
    bus:        busFactory(),
    hrtime:     hrtimeFactory({ process: { hrtime: process.hrtime } }),
    context,
    logger:     loggerFactory(),
    config:     {
      isTty,
      cwd,
      pkg,
      ...config,
    },
    // - project components -
    ...mocks,
    ...rest,
  };

  function errorsRegistry(errors) {
    if ('function' == typeof errors) errors = errors();
    if (!Array.isArray(errors)) {
      throw new Error('bdd-helper/ioc - errors must be an array of errors, or a function that returns one');
    }

    const registry = errorsFactory();
    errors = errors.map(err => {
      if ('function' == typeof err) return err;

      if ('string' != typeof err) {
        throw new Error(
          'bdd-helper/ioc - entries in errors array can be either classes or string of classe names to mock',
        );
      }

      const tmp = {
        [err]: class extends registry.PrunError {
          constructor(...a) {
            super();
            this.ctorArgs = a;
          }
        },
      };
      return tmp[err];
    });

    registry.register(errors);
    return registry;
  }
};
