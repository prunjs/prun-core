import { format, inspect } from 'node:util';

export default loggerFactory;

function loggerFactory() {
  let entries = new Entries();

  return Object.assign(instance('root'), {
    reset() {
      entries = new Entries();
    },
  });

  function instance(caller) {
    return ['trace', 'debug', 'info', 'warn', 'error', 'fatal'].reduce(
      (mockLogger, level) => Object.assign(mockLogger, {
        [level](meta, msg, ...splat) {
          if ('string' == typeof meta) [meta, msg, splat] = [{}, meta, msg ? [msg].concat(splat) : splat];
          entries.push({ ix: entries.length, caller, level, msg: format(msg, ...splat), meta });
        },
      }),
      setReadOnlyEntriesProperty({
        of: caller => instance(caller),
        child(childMeta) {
          const logger = this; //eslint-disable-line consistent-this
          return ['trace', 'debug', 'info', 'warn', 'error', 'fatal'].reduce(
            (childLogger, level) => Object.assign(
              childLogger,
              {
                [level](entryMeta, msg, ...splat) {
                  if ('string' == typeof entryMeta) {
                    [entryMeta, msg, splat] = [{}, entryMeta, msg ? [msg].concat(splat) : splat];
                  }
                  logger[level]({ ...childMeta, ...entryMeta }, msg, ...splat);
                },
              },
            ),
            setReadOnlyEntriesProperty({ ...this }),
          );
        },
      }),
    );

    function setReadOnlyEntriesProperty(v) {
      return Object.defineProperty(v, 'entries', {
        get: () => entries,
        enumerable: true,
        configurable: false,
      });
    }
  }
}

class Entries extends Array {
  of(name) {
    return name instanceof RegExp
      ? this.filter(e => e.caller.match(name))
      : this.filter(e => name === e.caller);
  }
  level(level) {
    return this.filter(e => level === e.level);
  }
  msg(msg) {
    return msg instanceof RegExp
      ? this.filter(e => e.msg && e.msg.match(msg))
      : this.filter(e => msg === e.msg);
  }
  msgInclude(msg) {
    return this.filter(e => e.msg && e.msg.includes(msg));
  }
  log({ log } = console) {
    log(inspect(this, { depth: 99, colors: true }));
  }
  reset() {
    this.length = 0;
  }
}
