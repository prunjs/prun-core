import Should from 'should';
import claim from '../lib/claim.js';

import SUT from '../lib/logger.js';

describe('lib/logger', () => {
  it('should be a factory function', () => {
    claim.isFunction(SUT);
  });

  describe('when called', () => {
    const ctx = {};
    before(() => {
      ctx.SUT = SUT();
    });

    describe('returned instance', () => {
      describe('form', () => {
        describe('logger api', () => {
          claim.hasApi(ctx, ['trace', 'debug', 'info', 'warn', 'error', 'fatal', 'child', 'of'], { prop: 'SUT' });
        });

        describe('test helper api', () => {
          claim(ctx, 'SUT').hasApi(['reset']);
          claim(ctx, 'SUT').hasProps({ entries: Array });
        });
      });

      describe('.child(meta)', () => {
        describe('when provided with some meta', () => {
          let logger;
          before(() => {
            let e = 1000;
            logger = SUT();
            logger.info({ ok: true }, 'king');
            logger.of('name').info({ entry: e++ }, 'father');
            logger.of('name').child({ child: true, gen: 2 })
              .info({ entry: e++ }, 'son');
            logger.of('name').child({ child: true, gen: 2 })
              .child({ grandChild: true, gen: 3 })
              .info({ entry: e++ }, 'grandson');
          });
          it('should augment the child meta on any of its entries', () => {
            Should(logger.entries.msg('son')[0])
              .have.property('meta')
              .have.properties({ entry: 1001, gen: 2, child: true });
          });
          it('should preserve context by genrations, the later cascades', () => {
            Should(logger.entries.msg('grandson')[0])
              .have.property('meta')
              .have.properties({
                child: true, //<-- inherited
                gen: 3, //<-- cascading, from grandchild context
                grandChild: true, //<-- from grandchild context
                entry: 1002, //<-- local to emition method
              });
          });
          it('should not affect entries that do not belong to the child', () => {
            Should(logger.entries.msg('father')[0])
              .have.property('meta')
              .not.have.properties(['gen', 'child', 'grandChild']);

            Should(logger.entries.msg('son')[0])
              .have.property('meta')
              .not.have.properties(['grandChild']);
          });
        });
      });

      describe('.reset()', () => {
        describe('when called after logs were captured', () => {
          let logger;
          before(() => {
            logger = SUT();
            logger.info({ foo: 'bar' }, 'before');
            logger.info('before');
            logger.info('was born');
            logger.of('child1').info('before');
            logger.info('before %s formatted', 'str');
            logger.of('child2').debug('before');
            logger.of('child2').of('child3')
              .warn('before');
            logger.warn({ bar: 'baz' }, 'before');

            logger.reset();
            logger.info('after');
          });
          describe('.entries', () => {
            it('should discard any entry before that and keep capturing', () => {
              Should(logger.entries.msg(/^before/).length).eql(0);
              Should(logger.entries.length).eql(1);
            });
          });
        });
      });
      describe('.entries', () => {
        let entries;
        let logger;
        before(() => {
          logger = SUT();
          logger.info({ foo: 'bar' }, 'msg');
          logger.info('str only');
          logger.info('was born');
          logger.of('child1').info('was born');
          logger.info('was %s formatted', 'str');
          logger.of('child2').debug('im here');
          logger.of('child2').of('child3')
            .warn('was born');
          logger.warn({ bar: 'baz' }, 'meta formatted: 1 %s 3', 2);

          ctx.entries = entries = logger.entries; //eslint-disable-line prefer-destructuring
        });
        after(() => logger.reset());

        it('should be an array', () => {
          Should(entries).be.an.Array();
        });

        it('should not be assignable', () => {
          Should(() => {
            'use strict';

            logger.entries = 'throws';
          }).throw(/has only a getter/);
        });

        describe('added entries API', () => {
          describe('form', () => {
            claim(ctx, 'entries').hasApi({
              of: 1,
              level: 1,
              msg: 1,
              msgInclude: 1,
              log: 0,
            });
          });

          describe('.of(caller)', () => {
            describe('when called with a string', () => {
              it('returns all entries of the exact caller name', () => {
                Should(entries.of('root').length).eql(5);
              });
            });
            describe('when called with regexp', () => {
              it('returns all entries of the caller names that match it', () => {
                Should(entries.of(/chi/).length).eql(3);
              });
            });
          });
          describe('.level(level)', () => {
            it('returns all entries of the caller name', () => {
              Should(entries.level('warn').length).eql(2);
            });
          });
          describe('.msg(str)', () => {
            describe('when called with a string', () => {
              it('returns all entries with msg identical to provided value', () => {
                Should(entries.msg('was born').length).eql(3);
              });
            });
            describe('when called with regexp', () => {
              it('returns all entries with msg identical to provided value', () => {
                Should(entries.msg(/^was/).length).eql(4);
              });
            });
          });
          describe('.msgInclude(str)', () => {
            it('returns all entries with msg identical to provided value', () => {
              Should(entries.msgInclude('o').length).eql(6);
            });
          });
          describe('.log(console)', () => {
            it('uses the console to print itself, described', () => {
              const con = console;
              const origLog = con.log;
              const mockLog = a => { mockLog.calledWith = a; };

              con.log = mockLog;
              try {
                entries.log();
              } finally {
                con.log = origLog;
              }

              Should(mockLog.calledWith)
                .be.ok()
                .be.a.String();
            });
          });
          describe('and', () => {
            it('filtering apis can be chained', () => {
              const { length: found, constructor: ctor } = entries
                .level('warn')
                .of('child3');

              Should(found).eql(1);
              Should(ctor).equal(entries.constructor);
            });
          });
        });
      });
    });
  });
});
