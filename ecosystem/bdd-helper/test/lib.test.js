import Should from 'should';

import claimModule from '../lib/claim.js';
import iocModule from '../lib/ioc.js';
import loggerModule from '../lib/logger.js';

import SUT from '../lib/index.js';

describe('prun-bdd-helper', () => {
  describe('default export', () => {
    it('should export ./lib/claim as default.claim', () => {
      Should(SUT).have.property('claim', claimModule);
    });

    it('should export ./lib/logger as default.logger', () => {
      Should(SUT).have.property('loggerFactory', loggerModule);
    });

    it('should export a logger instance as default.logger', () => {
      Should(SUT)
        .have.property('logger')
        .have.properties(['trace', 'debug', 'info', 'warn', 'error', 'fatal', 'child', 'of']);
    });

    it('should export ./lib/ioc as default.iocFactory', () => {
      Should(SUT).have.property('iocFactory', iocModule);
    });
  });
});
