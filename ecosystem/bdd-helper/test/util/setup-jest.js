import Should from 'should';

const { beforeAll, afterAll } = global;

global.Should = Should;

global.before = beforeAll;
global.after = afterAll;
