import Should from 'should';

import { afterAll, beforeAll } from 'vitest';

global.Should = Should;

global.before = beforeAll;
global.after = afterAll;
