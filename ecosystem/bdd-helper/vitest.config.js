import { defineConfig } from 'vitest/config';

export default defineConfig({
  test: {
    reporters: ['verbose'],
    include: [
      'test/**/*.test.js',
      'test/**/*.e2e.js',
    ],
    globals: true,
    setupFiles: ['test/util/setup-vitest.js'],
  },
});
