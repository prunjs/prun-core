const { configs } = require('@eslint/js');
const { node: nodeGlobals } = require('globals');

const markdown = require('./markdown');
const yamlConfig = require('./yaml');
const jsonConfig = require('./json');

const yamlText = require('fs').readFileSync(`${__dirname}/.eslintrc.yaml`); //eslint-disable-line no-sync
const { rules } = require('js-yaml').load(yamlText);

module.exports = [
  configs.recommended,
  {
    name: 'prun:ecmaVersion/2022',
    languageOptions: {
      ecmaVersion: 2022,
    },
  },
  {
    name: 'prun:rules',
    rules,
  },
  {
    name: 'prun:globals/node',
    files: ['**/*.js', '**/*.mjs', '**/*.cjs'],
    languageOptions: {
      globals: nodeGlobals,
    },
  },
  {
    name: 'prun:globals/tests',
    files: [
      '**/*.e2e.mjs',
      '**/*.test.mjs',
      '**/*.spec.mjs',
      '**/*.e2e.cjs',
      '**/*.test.cjs',
      '**/*.spec.cjs',
      '**/*.e2e.js',
      '**/*.test.js',
      '**/*.spec.js',
    ],
    languageOptions: {
      globals: {
        describe: 'readonly',
        it: 'readonly',
        before: 'readonly', //in mocha
        beforeAll: 'readonly', //in vitest/jest
        beforeEach: 'readonly',
        after: 'readonly', //in mocha
        afterAll: 'readonly', //in vitest/jest
        afterEach: 'readonly',
        Should: 'readonly',
      },
    },
  },
  {
    name: 'prun:source-type/module',
    files: ['**/*.mjs'],
    languageOptions: {
      sourceType: 'module',
    },
  },
  ...yamlConfig,
  ...jsonConfig,
  ...markdown,
];
