module.exports = [
  ...require('eslint-plugin-jsonc').configs['flat/recommended-with-json'],
  {
    name: 'prun:json/ignores',
    ignores: [
      'package-lock.json',
      '**/package-lock.json',
      '.nyc_output',
      '**/.nyc_output',
      '**/coverage',
      'coverage',
    ],
  },
  {
    name: 'prun:json/overrides',
    files: ['*.json', '**/*.json'],
    rules: {
      'max-len': 'off',
      'jsonc/indent': ['error', 2],
      'jsonc/key-name-casing': 'off',
    },
  },
];
