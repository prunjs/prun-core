const markdown = require('eslint-plugin-markdown');

module.exports =  markdown.configs.recommended
  .concat([
    {
      name: 'prun:markdown/code-blocks/js',
      files: ['**/*.md/*.js'],
      rules: {
        'no-console': 'off',
        'import/no-unresolved': 'off',
        'require-await': 'off',
        'no-unreachable': 'off',
      },
    },
  ]);
