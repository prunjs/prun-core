/* eslint no-octal: "off" */
const { spawn } = require('node:child_process');

const c = spawn('eslint', ['test/fx', '--fix'], { stdio: 'inherit' });

const RED = '\x1b[1;31m';
const YELLOW = '\x1b[1;33m';
const GREEN = '\x1b[1;32m';
const NC = '\x1b[0m';

c.on('exit', () => {
  process.exitCode = c.exitCode;

  process.exitCode
    ? console.log(`[${RED}X${NC}] ${YELLOW}@prun/eslint-config${NC} Test ${RED}FAILED${NC}`)
    : console.log(`[${GREEN}V${NC}] ${YELLOW}@prun/eslint-config${NC} Test passed`);
});
