module.exports = [
  ...require('eslint-plugin-yml').configs['flat/recommended'],
  {
    name: 'prun:yml/overrides',
    files: ['*.yaml', '**/*.yaml', '*.yml', '**/*.yml'],
    rules: {
      'lines-around-comment': 'off',
      'max-len': 'off',
    },
  },
];
