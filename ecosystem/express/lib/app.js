module.exports = ({
  powerBy = false,
}, {
  express = require('express'),
  config: { env },
  //logger,
  mw,
  routing,
}) => {
  const app = express();
  app.set('env', env);
  powerBy || app.disable('x-powered-by');

  app.use(mw);
  app.use(routing);

  return app;
};
