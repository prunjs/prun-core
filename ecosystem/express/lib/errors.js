//TBD: add to prun/errors RuntimeError, and distinct between config & runtime

module.exports = ({ errors: { PrunError, PrunConfigError } }) => [
  class BadRequestError extends PrunError {
    constructor(meta) {
      super('Validation Error', {
        description: [
          'this is a runtime error.',
          'this happens when a route specification includes validatiopn rules which',
          'the incoming request has failed to pass.',
        ],
        ...meta,
      });
    }
  },
  class NoSuchEndpointError extends PrunError {
    constructor(meta) {
      super('No Such Endpoint', {
        description: [
          'this is a user runtime error.',
          'this error is provided when the server ',
          'the incoming request has failed to pass.',
        ],
        ...meta,
      });
    }
  },
  class NoSuchEntityError extends PrunError {
    constructor(entity) {
      super('No Such Entity', {
        description: [
          'this is a runtime error.',
          'this error is provided when the entity specified',
          'by the request is not found in the data sources',
        ],
        entity,
      });
    }
  },
  class NoSuchWebViewError extends PrunConfigError {
    constructor(meta) {
      super('view name that is not a part of configuration for this endpoint', {
        description: [
          'this is a developer error.',
          'this error appears when setView(name) is called with a view name that is not found',
          'anywere in the views configured for the route:',
          '- views map logical names to view implementations.',
          '- baked in views provide a generic minimal set of basic views.',
          '- svc routes and route views may, in turn, add aliases to existing views, add its own',
          '  view implementations or cascade existing views with alias or implementation',
        ],
        ...meta,
      });
    }
  },
  class InvalidResponseHeaderError extends PrunError {
    constructor(meta) {
      super('Invalid Response Header', {
        description: [
          'this is a developer error',
          'this error occurs when project code tries to set an invalud http header',
          'this error should be found in development time.',
          'when project code handles headers dynamically - it should assure that header',
          'names and values are valid, or expect to handle such errors',
        ],
        ...meta,
      });
    }
  },
];
