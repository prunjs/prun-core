/*
  example
    layers:
      interfaces:
        web:
          module: '@prun/exrpess'
          options:
            formats:
            - json       # a baked in formatter
            - ejs        # example of formatter from ioc, from layer: web-components
            #TBD:
            #- templated: # a baked in formatter
            #    engine:       handlebars
            #    compile:      true
            #    templatesDir: path1

      web-components:
        ejs:
          module: '@prun/express-ejs'
          options:
            templatesDir: ./templates
              OR
            templateFiles: [ path1, path2, path3 ]
              OR
            templates:
              <name>: <templateStr>
 */
module.exports = ({ formats }, ioc) => {
  const {
    logger,
    errors: { configError },
    mime = require('mime'),
    jsonStringify = require('safe-json-stringify'),
    //inspect = require('node:util').inspect,
  } = ioc;

  const log = logger.of('@prun/express:formatters');
  //built in formatters
  formatsRegistry('json', {
    contentType: 'application/json; charset=utf-8',
    format: jsonStringify,
  });

  // formatsRegistry('inspect', {
  //   contentType: 'text/plain; charset=utf-8',
  //   format: v => inspect(v, { depth: 99 }),
  // });

  //string => array
  if ('string' == typeof formats) formats = formats.trim().split(/\s*,\s*/);
  //array => map
  if (Array.isArray(formats)) formats = formats.reduce((map, name) => ({ ...map, [name]: null }), {});

  //load from IOC / baked in
  if (formats) {
    Object.entries(formats)
      .forEach(([format, options]) => {
        let formatter = ioc[format];
        if (formatter) {
          if (null !== options) throw iocFormatterWithOptionsConfigError({ formatter, options });
        } else {
          log.debug({ formatter, options }, 'loading baked-in formatter');
          formatter = builtInFormatter(format, options);
        }
        formatsRegistry(format, formatter);
      });
  }

  if (log.isLevelEnabled('info')) {
    log.info({ formatters: Object.keys(formatsRegistry) }, 'loaded');
  }

  return formatsRegistry;

  function formatsRegistry(name, config) {
    const formatter = 'string' == typeof config
      ? formats[formatter]
      : config;

    if (!formatter) {
      throw configError('broken formatter alias', {
        description: [
          'this is a developer error',
          'this error happens when a project tries to register a formatter alias',
          'pointing to a mapper that is not present in the formatters registry',
        ],
        name,
        present: Object.keys(formatsRegistry),
        alias: config,
      });
    }

    validateFormatter(formatter);

    formatsRegistry[name] = formatter;
  }

  function builtInFormatter(format, options) {
    try {
      return require(`./${format}`)(options || {}, ioc);
    } catch (err) {
      throw configError('invalid formatter', {
        description: [
          'this is a developer error',
          'this error happens when the @prun/express options try to specify a formatter',
          'that is not found on the IOC nor in the baked-in formatters',
        ],
        triedFormat: format,
        present: Object.keys(formatsRegistry),
        cause: err,
      });
    }
  }

  function validateFormatter({ contentType, format }) {
    const reject =
      'function' != typeof format && 'formatter.format is not a function'
      || 'AsyncFunction' === format.constructor.name && 'formatter.format is an async function'
      || !contentType && 'formatter.contentType is missing'
      || 'string' != typeof contentType && 'formatter.contentType is not a string'
      || !mime.getExtension(contentType) && 'formatter.contentType is not a valid mimetype string';

    if (reject) {
      throw configError(`invalid formatter: ${reject}`, {
        description: [
          'this is a developer error',
          'this error happens when trying to register a custom formatter that does not comply',
          'with formatter API:',
          '- formatter.contentType must be a valid MIMETYPE string',
          '- formatter.format must be a SYNCHRONOUS function',
        ],
        contentType,
        format: typeof format.format,
      });
    }
  }

  function iocFormatterWithOptionsConfigError({ formatter, options }) {
    return configError('formatters config with options to a formatter on ioc', {
      from: '@prun/express',
      description: [
        'this message is emitted when formatters config tries to provide options to a',
        'formatter whose name is found on the ioc.',
        'ioc components are initiated with their own options.',
        '- to configure options for built-in formatters side by side with custom formatters',
        '  provide `null` as the options for custom formatters from ioc.',
        '- to attach only custom formatters from ioc - you may pass them as an array of',
        '  strings or a simple csv string, whitespaces allowed.',
      ],
      formatter,
      options,
    });
  }
};
