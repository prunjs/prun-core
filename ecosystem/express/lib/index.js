interfaceFactory.errors = require('./errors');
interfaceFactory.validate = validate;
module.exports = interfaceFactory;

interfaceFactory.env = {
  'http.port': 'HTTP_PORT',
  'https.port': 'HTTPS_PORT',
  'https.key': 'HTTPS_KEY',
  'https.cert': 'HTTPS_CERT',
  bodyLimit: 'BODY_LIMIT',
};

function interfaceFactory(options, ioc) {
  ioc.logger = ioc.logger.child({ interface: '@prun/express' });
  const {
    logger,
    bus,
    errors,
  } = ioc;
  const { PrunConfigError } = errors;
  const { http = { port: 8081 }, https } = options;

  const log = logger.of('@prun/express', { options });
  log.debug({ iocNames: Object.keys(ioc), http }, 'initializing');

  ioc.modulesFor = modulesFor;

  log.debug('creating formatters');
  ioc.formatters = require('./formatters')(options, ioc);

  log.debug('creating views');
  ioc.views = require('./views')(options, ioc);

  log.debug('creating mw');
  ioc.mw = require('./mw')(options, ioc);

  log.debug('creating routes');
  ioc.routing = require('./routing')(options, ioc);

  log.debug('creating app');
  ioc.app = require('./app')(options, ioc); //uses mw, routing

  log.debug('creating servers');
  ioc.svr = require('./svr')({ http, https }, ioc);

  bus.on('shutdown', ioc.svr.stop);

  log.info('initialized');

  return {
    run: () =>
      ioc.svr.start()
        .catch(err => Promise.reject(
          Object.assign(err, {
            message: `Failed to start servers: ${err.message}`,
            by: '@prun/express',
            options,
          }),
        )),
  };

  //TBD: maybe should belong to @prun/prun core?
  function modulesFor(option, { mandatory = false } = {}) {
    let iocNames = options[option] || [option];
    if (!Array.isArray(iocNames)) iocNames = [iocNames];

    const missing = [];
    const pulled = [];

    iocNames.forEach(module => {
      const iocModule = ioc[module];
      iocModule
        ? pulled.push(iocModule)
        : missing.push(module);
    });

    if (mandatory && missing.length) {
      throw new PrunConfigError('missing ioc names', {
        missing,
        iocNames,
        option,
      });
    }

    return pulled;
  }
}

function validate(options, ioc) {
  const {
    defaultReqLogLevel = 'info',
    muteReqLog: mutedPaths = [],
  } = options;
  const {
    logger,
    config: { logger: { levels: loggerLevels } },
    errors: { configError },
  } = ioc;

  options.mutedPaths = mutedPaths.map(rule => {
    if ('string' == typeof rule) return { search: rule, type: 'startsWith' };

    if ('match' === rule.type) {
      try {
        rule.search = new RegExp(rule.search, rule.flags);
      } catch (e) {
        rule.pathRegExpErr = e.message;
      }
    }

    if (!rule.type) rule.type = 'startsWith';

    if (rule.method) {
      if (Array.isArray(rule.method)) rule.method = rule.method.join('|');
      try {
        rule.method = new RegExp(rule.method, 'i');
      } catch (e) {
        rule.methodRegExpErr = e.message;
      }
    }

    return rule;
  });

  const strProto = String.prototype;
  const badMutedPaths = options.mutedPaths.filter(
    ({ type, search, regExpErr, methodRegExpErr }) =>
      regExpErr
            || methodRegExpErr
            || !search
            || 'function' != typeof strProto[type],
  );

  if (badMutedPaths.length) {
    throw configError('Invalid elements in options.muteReqLog', {
      description: [
        'This error is thrown when options.muteReqLog for prun/express contains an invalid element.',
        'Valid elements should contain:',
        '  - search - a string',
        '  - type - any built in string method',
        '  - flags - relevant only when type is \'match\'',
        'Notes:',
        ' - when the element is a string - it\'s uderstood as { search: element, type: \'startsWith\' }',
        ' - when type is \'match\' - pattern is built as new RegExp(pattern, flags)',
        ' - The path will be muted when the \'type\' method returns truthful value when called on req.url',
        '   with the search as a sole argument.',
      ],
      badMutedPaths,
    });
  }

  if (!loggerLevels['request-log']) loggerLevels['request-log'] = defaultReqLogLevel;
  if (!(loggerLevels['request-log'] in logger.levels.values)) {
    throw configError('Invalid request-log logger level', {
      description: [
        'this error happens when the log level provided for `request-log` is not',
        'supported by the logger',
      ],
      supportedLevels: Object.keys(logger.levels.values),
      'request-log': loggerLevels['request-log'],
    });
  }

  //TBD - make sure all named iocNames are present
  //TBD - make sure the rest of facotry options are valid and present
}
