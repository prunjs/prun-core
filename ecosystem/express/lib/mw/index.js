module.exports = ({
  bodyParserLimit,
  methodOverride    = false,
  correlationHeader = 'x-correlation-id',
  reqId,
}, {
  config,
  logger,
  hrtime,
  context,
  modulesFor,
  mw: projectMw    = modulesFor('mw'),
  bodyParser       = require('body-parser'),
  cookieParser     = require('cookie-parser'),
  methodOverrideMw = require('method-override'),
  bootstrap        = require('./request-bootstraper'),
}) => {
  const log = logger.of('@prun/express:mw');

  const mw = [
    bootstrap({ correlationHeader, reqId }, { config, logger, context, hrtime }),
    cookieParser(),
    bodyParser.json({ limit: bodyParserLimit }),
    bodyParser.urlencoded({ extended: true, limit: bodyParserLimit }),
  ];

  if (methodOverride) mw.push(methodOverrideMw());

  //TRICKY: even though express knows to handle nested mw arrays,
  //   flatified list is faster to execute, plus - I want to log them
  const flatified = mw.reduce(flatify, []);
  if (flatified.length) mw.push(...flatified);

  //TBD: swagger & static - depending on env?

  log.info({
    methodOverride,
    correlationHeader,
    projectMw: Boolean(projectMw),
    mwCount: flatified.length,
  }, 'initialized');

  return flatified;

  function flatify(flat, cur) {
    if (Array.isArray(cur)) return cur.reduce(flatify, flat);
    flat.push(cur);
    return flat;
  }
};
