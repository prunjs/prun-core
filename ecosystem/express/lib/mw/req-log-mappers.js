module.exports = {
  reqView,
  resView,
};

function reqView({
  httpVersion,
  method,
  url,
  originalUrl = url,
  forwardedSecure,
  correlationId = () => 'n/a',
  headers,
  body,
  complete,
}) {
  return {
    httpVersion,
    method,
    url: originalUrl,
    forwardedSecure,
    correlationId: correlationId(),
    headers,
    body,
    complete,
  };
}

function resView({ finished, statusCode, statusMessage, _header, view = 'n/a', viewModel, body }) {
  if (!finished) return { finished };

  const head = _header
    .trim()
    .replace(/\r/g, '')
    .split('\n');
  const replyLine = head.shift() || 'n/a';
  const response = {
    replyLine,
    protocol: replyLine.replace(/ .*/, ''),
    statusCode,
    statusMessage,
    headers: head.reduce((headers, header) => {
      const firstColon = header.indexOf(':');
      headers[header.slice(0, firstColon)] = header.slice(firstColon + 1);
      return headers;
    }, {}),
    view,
  };

  if (viewModel) response.viewModel = viewModel;
  if (body) response.body = body;
  return response;
}
