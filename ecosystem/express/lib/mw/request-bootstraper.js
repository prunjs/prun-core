module.exports = ({
  correlationHeader,
  reqId: {
    hyperid = { urlSafe: true, fixedLength: false },
    uuid = false,
  } = {},
}, {
  hrtime,
  context,
  logger,
  onFinished = require('on-finished'),
  newReqId   = uuid ? require('uuid').v4 : require('hyperid')(hyperid),
  reqLog: {
    reqView,
    resView,
  } = require('./req-log-mappers'),
}) => (req, res, next) => {
  const dur = hrtime();
  req.id = newReqId();

  //TBD: this line is too specific
  req.forwardedSecure = 'https' === req.headers['x-forwarded-proto'];

  augmenViewFunctions(req, res);

  augmentWithLoggerProxies(req, res);

  incomingOutgoingRequestLog(req, res);

  context.watch(() => {
    context.correlationId = req.get(correlationHeader) || req.id;
    req.correlationId = () => context.correlationId;
    next();
  });

  function augmentWithLoggerProxies(req, res) {
    const idNum = req.id.substr(req.id.lastIndexOf('-') + 1);
    let log = logger.of('request-log');
    configure({ caller: `req#${idNum}`, reqId: req.id });

    const proxy = new Proxy({}, {
      ownKeys: () => Object.keys(log),
      get: (o, m) => log[m],
      set: (o, k, v) => {
        switch (k) {
        case 'level':
          configure({ level: v });
          break;
        default:
          log[k] = v;
        }
        return v;
      },
    });

    const logPropDescr = {
      enumerable: false,
      configurable: true,
      get: () => proxy,
      set: newLog => log = newLog, //eslint-disable-line no-return-assign
    };

    Object.defineProperty(req, 'log', logPropDescr);
    Object.defineProperty(res, 'log', logPropDescr);

    function configure({ level, redact, serializers = {}, ...meta }) {
      log = log.child(meta, { level, serializers, redact });
      log.configure = configure;
      log.instance = log;
      return log;
    }
  }

  function augmenViewFunctions(req, res) {
    req.logView = () => reqView(req);
    res.logView = () => resView(res);
  }

  function incomingOutgoingRequestLog(req, res) {
    const reqLog = req.log.instance; //TRICKY: separate level of reqLog from appLog

    reqLog.debug({
      req:  reqView(req), //TRICKY: req is not part of logger meta because we want a log-time snapshot
    }, 'incoming request');

    onFinished(res, () => {
      if (res.muteReqLog) return;

      reqLog.info({
        inMs: dur(),
        req: reqView(req),
        res: resView(res),
      }, 'request served');
    });
  }
};
