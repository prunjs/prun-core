module.exports = () => {
  const routes = [];
  const defaults = { views: {} };

  const builder = {
    defaults: (...d) => {
      Object.assign(defaults, ...d);
      return builder;
    },
    build: () => routes,
  };

  //load all supported http methods...
  const methods = ['use', 'put', 'get', 'delete', 'post', 'head', 'options'];
  methods.forEach(method => {
    builder[method] = (path, rawOptions, handler) => {
      if ('function' == typeof rawOptions) [rawOptions, handler] = [{}, rawOptions];

      const {
        log: logCfg,
        views,
        ...options
      } = rawOptions;

      const route = {
        ...defaults,
        handler,
        ...options,
        log: { ...defaults.log, ...logCfg },
        views: { ...defaults.views, ...views },
        path,
        method,
      };

      routes.push(route);

      return builder;
    };
  });

  return builder;
};
