module.exports = ({
  healthCheck: {
    path: healthCheckPath = '/health-check',
    handlerIoc: healthCheckHandlerIoc = 'healthCheck',
    mute: muteHealthcheckReqLog = true,
  } = {},
  defaultErrorView = 'genericError',
  mutedPaths,
  //TBD: allow configure default views map for the entire web-interface
}, ioc) => {
  const {
    errors,
    config: {
      envType,
      pkg,
      logger: { levels: { default: defaultLogLevel } },
    },
    logger,
    modulesFor,
    formatters,
    views,
    [healthCheckHandlerIoc]: healthcheckHandler,
    router      = require('express').Router(),
    getVal      = require('lodash.get'),
    toJSON      = require('safe-json-stringify'),
    validations = require('./validations')({}),
    replyFcty   = require('./reply'),
    responder   = require('./responder'),
  } = ioc;

  const {
    PrunError,
    configError,
    badRequestError,
  } = errors;

  const log = logger.of('@prun/express:routing');
  const mounted = [];

  log.info('initializing');

  validateDefaultErrorView(defaultErrorView);

  mountProjectRoutes();

  mountHealthcheckRoute();

  mountNotFoundHandler();

  mountErrorHandler();

  mounted.push(
    `[*] ${healthCheckPath}`,
    '[*] <not-found>',
    '[*] <error handler>',
  );
  //TRICKY: on purpose - directly on root logger
  logger.info({ mounted }, 'mounted web endpoints');

  return router;

  function validateDefaultErrorView(defaultErrorView) {
    if (!views[defaultErrorView]) {
      throw configError('Invalid default error view', {
        description: [
          'this is a develop time error',
          'this error is thrown when the defaultErrorView option points to a view that is not',
          'found in the views registry at the time the @prun/express web interface initiates',
        ],
        defaultErrorView,
        foundViews: Object.keys(views),
      });
    }
  }

  function mountProjectRoutes() {
    const routers = modulesFor('routes', { mandatory: true })
      .map(routes => {
        return 'function' == typeof routes.build
          ? routes.build()
          : routes;
      });
    log.debug({ routers }, 'mouting routes');

    routers.forEach(router => {
      router.forEach(route => mountRoute(route));
    });
  }

  function mountRoute(routeCfg) {
    //validate routeCfg
    const {
      endpoint,
      routerMethod,
      muteReqLog,
      logCfg,
      createReply,
      validate,
      mw,
      controller,
      handler,
    } = normalizeRouteCfg(routeCfg, ioc);

    const { method, path } = endpoint;
    mounted.push(`${method} ${path}${validate ? '' : ' (no req validation)'}`);

    if (mw.length) {
      mw.push((err, req, res, next) => { //eslint-disable-line max-params
        req.log.trace('error in leading mw');
        next(Object.assign(err, { endpoint, routeCfg }));
      });
    }

    mw.push((req, res, next) => {
      const reqLog = req.log.configure(logCfg);
      reqLog.debug('in route');

      //TRICKY: muteReqLog is computed in mount time,
      //   but paths mounted with `.use(path, mw)` can be identified only on runtime
      res.muteReqLog = muteReqLog ?? isMuteReqLog(req.method, req.originalUrl);

      const reply = createReply(res);

      (async () => {
        let model;
        try {
          if (validate) {
            reqLog.debug('in validation');
            const rejects = validate && validate(req);
            if (rejects) throw badRequestError({ endpoint, rejects });
          } else {
            reqLog.debug('no validation in route');
          }

          reqLog.debug('in handler');
          model = await Reflect.apply(handler, controller, [req, reply]);
        } catch (err) {
          model = Object.assign(err, { endpoint });
        }

        try {
          reqLog.debug({ model: model ?? 'N/A!' }, 'handler returned model');

          handleModel({ model, req, res, reply });
          return null;
        } catch (err) {
          reqLog.trace({ err, endpoint }, 'caught error');
          throw Object.assign(err, { endpoint });
        }
      })().catch(next);
    });

    router[routerMethod](path, mw);
  }

  function normalizeRouteCfg({ //eslint-disable-line complexity
    method: routerMethod = 'use',
    path,
    validate: schemas = {},
    views: routeViews,
    log: {
      level: logLevel = defaultLogLevel,
      ...logCfg
    } = {},
    muteReqLog = isMuteReqLog(routerMethod, path),
    mw = [],
    controller, // -> for those who cannot live without `this`
    handler, // -> async handler OR methodName
    type = 'json',
  }, ioc) {
    routerMethod = routerMethod.toLowerCase();
    const method = routerMethod === 'use' ? '[*]' : routerMethod.toUpperCase();
    const endpoint = { method, path };

    //validate controller
    if ('string' == typeof controller) {
      const controllerName = controller;
      controller = getVal(ioc, controllerName);
      if (!controller) {
        throw configError('Invalid route controller', {
          description: [
            'this is a development time error.',
            'this error happens when a project defines a route with a controller provided',
            'as an IOC name, but the IOC name is not found',
            '(the IOC name may be a path to a nested attribute)',
          ],
          endpoint,
          controllerName,
        });
      }
    }

    //validate handler
    if ('string' == typeof handler) {
      const methodName = handler;
      if (!controller) {
        throw configError('Invalid route - method name without controller', {
          description: [
            'this is a development time error.',
            'this error happens when a project defines a route with a handler provided',
            'as a method name on the controller, but the controller not provided or not',
            'found',
          ],
          endpoint,
          methodName,
        });
      }

      handler = controller[methodName];
      if ('function' != typeof handler) {
        throw configError('Invalid route method-name handler', {
          description: [
            'this is a development time error.',
            'this error happens when a project defines a route with a handler provided',
            'as a method name, but the provided controller does not implement such method',
          ],
          endpoint,
          methodName,
          valueFound: handler,
          //TBD: do we want to list all supported attributes, including inheritted attributes?
        });
      }
    }

    if ('function' != typeof handler) {
      throw configError('Invalid route - no handler', {
        description: [
          'this is a development time error.',
          'this error happens when a project defines a route without a handler.',
          'as a minimum - a noop should be provided explicitly',
        ],
        endpoint,
      });
    }

    //validate formatter
    const formatter = formatters[type];
    if (!formatter) {
      throw configError('Invalid route output type', {
        description: [
          'this is a developer error.',
          'this error happens when a project define a route with a type that has no',
          'registered implementation on the formatters registry',
        ],
        endpoint,
        registeredTypes: Object.keys(formatters),
        triedFormat: type,
      });
    }

    //validate views
    if (routeViews) {
      //resolve aliases && validate against circular aliasing
      const providedViews = { ...routeViews };
      const circularViewAliases = Object.keys(routeViews).filter(alias => {
        let name = alias;
        let count = 20;
        while (--count && routeViews[name]) name = routeViews[name];
        if (!count) return true;
        routeViews[alias] = name;
        return false;
      });
      if (circularViewAliases.length) {
        throw configError('Invalid route views - circular aliasing found', {
          description: [
            'this is a developer error',
            'this error happens when a project defines a route with views map that contains',
            'aliases which cannot be resolved due to circula aliasing',
          ],
          endpoint,
          views: providedViews,
        });
      }

      const invalidViewNames = Object
        .entries(routeViews)
        .map(([name, alias]) => { return 'boolean' == typeof alias ? name : alias; })
        .filter(name => !views[name]);
      if (invalidViewNames.length) {
        throw configError('Invalid route views - alias not found', {
          description: [
            'this is a developer error.',
            'this error happens when a project defines a route with views map that tries',
            'to reference views that are not found in the view registry',
          ],
          endpoint,
          foundViews: Object.keys(views),
          invalidViewNames,
          routeViews,
        });
      }
    }

    //validate req-log level for the route
    if (!(logLevel in logger.levels.values)) {
      throw configError('Invalid route log level', {
        description: [
          'this is a developer error.',
          'this error happens when a project provides for a route a log level that is not',
          'supported by the logger.',
        ],
        endpoint,
        logLevel,
        supportedLevels: Object.keys(logger.levels.values),
      });
    }

    //validate logSerializers
    const { serializers: logSerializers } = logCfg;
    if (logSerializers) {
      const invalidSerializers = Object
        .keys(logSerializers)
        .filter(prop => { return 'function' != typeof logSerializers[prop]; });
      if (invalidSerializers.length) {
        throw configError('Invalid route logSerializers', {
          description: [
            'this is a developer error.',
            'this error happens when a project provides a map of custom log serializers for',
            'a route, but one of the values in that map is not a function',
          ],
          endpoint,
          invalidSerializers,
        });
      }
    }

    //TBD: validate mw array - as array of functions with arrity of (req,res,next) or (err,res,res,next)

    Object.assign(logCfg, { level: logLevel, endpoint });

    const validate = validations(schemas);
    const createReply = replyFcty(
      { endpoint, formatter, routeViews, defaultErrorView },
      { errors, views },
    );

    return {
      endpoint,
      logCfg,
      muteReqLog,
      validate,
      mw: [...mw],
      controller,
      handler,
      formatter,
      views: routeViews,
      createReply,
      routerMethod,
    };
  }

  function isMuteReqLog(method, path) {
    return mutedPaths?.find(
      rule =>
        path[rule.type](rule.search)
                && (!rule.method || method.match(rule.method)),
    );
  }

  function handleModel({ model, req, res, reply }) {
    const { log: reqLog } = req;
    const isVerbose = reqLog.isLevelEnabled('debug');
    const isDebug = reqLog.isLevelEnabled('debug');

    reqLog.debug(isVerbose ? { isVerbose, model } : { isVerbose }, 'in route handle model');

    res.model = model; //TRICKY: logged from errorhandler in case render/respond fails

    if (model instanceof Error) {
      if (!(model instanceof PrunError)) {
        reqLog.error({ err: model, req: req.logView() }, 'unexpected error in route');
      }
    }

    const webView = reply.conclude(model);

    if (isDebug) {
      const meta = { isVerbose, view: { ...webView } };
      if (!isVerbose) meta.view.body = undefined; //eslint-disable-line no-undefined
      reqLog.debug(meta, 'in route responder');
    }

    responder(res)(webView);
  }

  function mountHealthcheckRoute() {
    mountRoute({
      path: healthCheckPath,
      views: {},
      muteReqLog: muteHealthcheckReqLog,
      handler: async (req, reply) => {
        if ('function' !== typeof healthcheckHandler) return pkg;

        let info;
        try {
          info = await healthcheckHandler(req);
        } catch (err) {
          reply.view = 'healtcheckError';
          info = { ...err, error: `${err.name}: ${err.message}` };
        }

        return Object.assign(info, pkg);
      },
    });
  }

  function mountNotFoundHandler() {
    const { noSuchEndpoint } = views;
    router.use((req, res) => responder(res)(noSuchEndpoint({ req })));
  }

  function mountErrorHandler() {
    //TBD: body beautification is a code that should be reused for all json views
    const isProd = 'production' === envType;
    const stringify = isProd
      ? toJSON
      : v => toJSON(v, null, 2);

    //eslint-disable-next-line no-unused-vars
    router.use((err, req, res, next) => { //eslint-disable-line max-params
      const { log: reqLog } = req;
      const meta = { err };
      if (reqLog.isLevelEnabled('debug') || log.isLevelEnabled('debug')) {
        'function' == typeof req.logView && (meta.req = req.logView());
        //'function' == typeof res.logView && (meta.res = res.logView());
        meta.model = res.viewModel;
      }
      reqLog.error(meta, 'violent error in route');

      if (res.finished) return;

      //TBD: the stack suppression has to be done also in all error views.
      //  - can this be consolidated to one place?
      const { name, stack, endpoint, ...errInfo } = err.toJSON();
      const body = res.body = {
        error: 'there has been an error during this request',
        code: 'ERR_UNEXPECTED_LOW_LEVEL_ERROR',
        cause: isProd ? { error: name } : { error: name, stack, ...errInfo },
        endpoint,
      };

      //TRICKY: this is a low-level fallback, we should not use responder here.
      if (!res.headersSent) {
        res.status(500);
        res.type('json');
      }

      res.end(stringify(body));
    });
  }
};
