module.exports = ({
  endpoint,
  formatter,
  routeViews,
  defaultErrorView,
}, {
  http: { validateHeaderValue, validateHeaderName } = require('http'),
  camelize = require('lodash.camelcase'),
  errors,
  views,
}) => res => {
  let headers = {};
  let view = views[routeViews.ok] || views.ok;
  let viewName = 'ok';
  //TBD: a state member for cookie, or just over header?

  const headersProxy = new Proxy({}, {
    ownKeys: () => Object.keys(headers),
    get: h => headers[h],
    set: (t, k, v) => setHeader(k, v),
    deleteProperty: (t, k) => Reflect.deleteProperty(headers, k),
  });

  const prop = d => ({ configurable: true, enumerable: true, ...d });
  return Object.defineProperties({
    setCookie,
    clearCookie,

    //TBD: do we want to allow both  these APIs and the headers proxy?
    setHeader,
    setHeaders,
    setView,

    conclude,

    raw: res,
  }, {
    view: prop({
      get: () => viewName,
      set: setView,
    }),
    headers: prop({
      get: () => headersProxy,
      set: setHeaders,
    }),
    cookie: prop({
      get: () => {
        throw errors.notImplementedError('read from reply.cookie');
      },
      set: () => {
        throw errors.error('cannot assign to reply.cookie', {
          description: [
            'this is a developer error.',
            'this error happens when project code tries to set to reply.cookie',
            'use reply.setCookie({ name, value, [path,][, domain][, expiry] })',
          ],
        });
      },
    }),
  });

  function setView(name, defaultName) {
    while (routeViews[name]) name = routeViews[name];
    view = views[name];
    if (!view && defaultName) view = views[name = defaultName];
    if (!view) {
      throw errors.noSuchWebViewError({
        endpoint,
        foundViews: Object.keys(views),
        missingView: name,
      });
    }
    viewName = name;
  }

  function setHeader(k, v) {
    try {
      validateHeaderName(k);
      validateHeaderValue(k, v);
    } catch ({ message: reject, code }) {
      throw errors.invalidResponseHeaderError({
        endpoint,
        reject,
        triedHeader: k,
        triedValue: v,
        code,
      });
    }
    headers[k] = v;
  }

  function setHeaders(newHeaders) {
    headers = {};
    Object.entries(newHeaders).forEach(([k, v]) => setHeader(k, v));
  }

  function setCookie(name, cookie) {
    throw errors.notImplementedError('reply.setCookie', { name, cookie });
  }

  function clearCookie(name) {
    throw errors.notImplementedError('reply.clearCookie', { name });
  }

  function conclude(model = 'NA') {
    if (model instanceof Error) {
      const normalizedName = camelize(
        model.name
          .replace(/Error$/, '')
          .replace(/^Prun/, ''),
      );

      setView(normalizedName, defaultErrorView);
    }
    res.log.debug({ model, view }, 'concluding...');
    //TBD: need to let custom views do their own formatting?
    //TBD: need to let view definitions name template / render config

    const { status, headers: viewHeaders, type, mapped, isStream } = view(model);

    const body = isStream ? model : type === 'text' ? mapped : formatter.format(mapped);

    return {
      status,
      headers: {
        'content-type': formatter.contentType,
        ...viewHeaders,
        ...headers,
      },
      mapped,
      body,
    };
  }
};
