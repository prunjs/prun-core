const { Stream } = require('stream');

module.exports = res => {
  return function respond({ status, headers, mapped, body = mapped }) {
    //status
    status && res.status(status);

    //headers
    headers && res.set(headers);

    //body
    body instanceof Stream
      ? stream(body)
      : send(body, mapped);
  };

  function stream(body) {
    res.body = '<stream>';
    body.pipe(res);
  }

  function send(body, mapped) {
    //TRICKY: should be configurable - we may not always want to do that...
    // the goal is to support body-logging in the request-on-finish
    // I'm also not sure this is the correct place
    if (mapped !== body) res.viewModel = mapped;
    res.body = body;
    //TRICKYEnd

    //TRICKY: express res.send implies setting `content-type` when it has
    //  not been set explicitly
    res.send(body);
  }
};
