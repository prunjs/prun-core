module.exports = ({
  Ajv = require('ajv'),
  ajv = new Ajv(),
  supportedValidators = ['params', 'query', 'headers', 'body'],
}) => {
  return schema => {
    const validations = [];

    supportedValidators.forEach(target => schema[target] && validations.push({
      target,
      validator: ajv.compile(schema[target]),
    }));

    return validations.length
      ? validate(validations)
      : null;
  };

  function validate(validations) {
    return req => {
      let ok = true;
      const rejects = validations.reduce((rejects, { validator, target }) => {
        validator(req[target]);
        if (validator.errors) {
          ok = false;
          rejects[target] = validator.errors;
        }
        return rejects;
      }, {});

      if (ok) return null;

      return rejects;
    };
  }
};
