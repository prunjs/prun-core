/*
  This module concerns with managing http & https servers.
*/
module.exports = ({
  http,
  https,
}, {
  app,
  logger,
  http: { createServer: httpSvr } = require('http'),
  https: { createServer: httpsSvr } = require('https'),
  //config: { env },
}) => {
  const log = logger.of('@prun/express:svr');
  const { assign } = Object;
  const { promisify } = require('util');
  const handle = app.handle.bind(app);
  const svr = {
    http:
      !http || http.off
        ? noopSvr()
        : assign(httpSvr(getHttpOptions(http), handle), { options: http }),
    https:
      !https || https.off
        ? noopSvr()
        : assign(httpsSvr(getHttpsOptions(https), handle), { options: https }),
  };

  log.debug('initiated');

  return { start, stop };

  async function start() {
    log.info('starting');
    const { http, https } = svr;
    await Promise.all([
      promisify(cb => http.listen(http.options, e => cb(e)))(),
      promisify(cb => https.listen(https.options, e => cb(e)))(),
    ]);

    const addresses = {
      http: http.address(),
      https: https.address(),
    };

    //TRICKY: this goes specifically on the *root* logger
    logger.info(addresses, 'web interface started');
  }

  function stop() {
    logger.info('stopping web interface');
    svr.http.close();
    svr.https.close();

    //TBD - do we need to flush or timeout requests?
    // - can we hook on last response processed and log it?
    // - do we want to time it?
  }

  function getHttpOptions(options) {
    //TBD: I think the only thing that might be valuable here is
    // the header size limit
    // see - https://nodejs.org/api/http.html#http_http_createserver_options_requestlistener
    return options;
  }

  function getHttpsOptions(options) {
    options = getHttpOptions(options);
    //TBD: apply defaults
    //TBD: normalize
    //TBD: validate

    //see -https://nodejs.org/api/tls.html#tls_tls_createserver_options_secureconnectionlistener
    //see - https://nodejs.org/api/tls.html#tls_tls_createsecurecontext_options
    return options;
  }

  function noopSvr() {
    return {
      address: () => null,
      listen(o, cb) { cb(); },
      close: () => null,
    };
  }
};
