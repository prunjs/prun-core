module.exports = ({ defineView: view, defaultHeaders: headers }) => ({
  ok: view({ headers }),
  accepted: view(
    { code: 201, headers },
  ),
  badRequest: view(
    { code: 400, headers },
    ({ rejects }) => ({
      error: 'Invalid Request',
      message: 'the request failed validation on one or more of its parts',
      rejects,
    }),
  ),
  noSuchEndpoint: view(
    { code: 404, headers },
    //TRICKY: cannot expect res unwrapped, because it is a Stream
    ({ req }) => ({
      error: 'No Such Endpoint',
      message: 'this is not the endpoint you\'re looking for',
      endpoint: endpoint(req),
    }),
  ),
  noSuchEntity: view(
    { code: 404, headers },
    ({ endpoint, entity }) => ({
      error: 'Entity Not Found',
      message: 'the entity query does not match any results',
      endpoint,
      entity,
    }),
  ),
  noSuchWebView: view(
    { code: 501, headers },
    ({ name, message, stack = '', callStack = stack.split('\n'), ...rest }) => ({
      error: name,
      message,
      ...rest,
      hint: callStack[callStack.findIndex(s => s.includes('/reply.js')) + 1],
    }),
  ),
  healtcheckError: view(
    { code: 500, headers },
  ),
  genericError: view(
    { code: 500, headers },
    err => {
      const { name: error, message, ...rest } = err.toJSON();

      //TBD: decide where & how to eliminate stack trace based on env
      //TRICKY: not sure it's part of mapper logic
      //   it cant repeat in every error mapper...
      //   maybe need something deeper

      return {
        error,
        message,
        err: rest,
      };
    },
  ),
});

function endpoint({ originalUrl: url, method }) {
  return {
    url,
    method,
  };
}
