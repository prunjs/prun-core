const { assign } = Object;
const { Stream } = require('stream');
const assert = require('assert');

module.exports = (
  {
    headers: defaultHeaders,
  },
  {
    logger,
    modulesFor,
    defaultViews = require('./default-views'),
    views: projectViews = modulesFor('views'),
  },
) => {
  const log = logger.of('express:views');

  viewsRegistry(defaultViews({ defineView, defaultHeaders }), 'baked-in');
  viewsRegistry(projectViews, 'project');

  return viewsRegistry;

  function viewsRegistry(newViews, source) {
    const found = Object.keys(viewsRegistry);
    if (!Array.isArray(newViews)) newViews = [newViews];

    //TBD: maybe we need to be more transparent about views cascading each other?
    // - should we can work harder and emit info/warn on overrun views?
    newViews = newViews.length ? Object.assign(...newViews) : {};

    Object.entries(newViews).forEach(([name, mapper]) => {
      viewsRegistry[name] = model => ({
        type: mapper.type,
        status: mapper.code,
        headers: mapper.headers,
        isStream: model instanceof Stream,
        mapped:
                    //TBD: maybe that's a mistake? in case views need to be a tranform stream?
                    //     - is it a mapper job or a format job?
                    model instanceof Stream
                      ? model
                      : mapper(model),
      });
    });

    if (log.isLevelEnabled('info')) {
      const meta = found.length
        ? { source, found, updated: Object.keys(newViews) }
        : { source };
      log.info(meta, 'views updated: ', source);
    }
  }
};

assign(module.exports, { defineView });

function defineView(options, bodyMapper = undefined) { //eslint-disable-line no-undefined
  //TRICKY: accepted forms: view(opts, f) | view(f, opts) | view({bodyMapper: f, ...opts})
  if ('function' == typeof options) [options, bodyMapper] = [bodyMapper, options];
  const asIs = m => m;
  const {
    code = 200,
    headers = {},
    type = 'json',
    bodyMapper: f = bodyMapper || asIs,
  } = options || {};
  assert.equal(
    f.length,
    1,
    'number of named args for handler is wrong. bodyMapper should be a function that expects 1 argument: model',
  );
  return assign(f, { code, headers, type });
}

