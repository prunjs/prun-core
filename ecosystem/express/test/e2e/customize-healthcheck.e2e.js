const Should = require('should');
const { request, svrSetup } = require('./util');
const expectedHeaders =  {
  'content-type': 'application/json; charset=utf-8',
  'content-length': /^\d+$/,
};

describe('a project with a custom healthceck handler under custromized ioc-name', () => {
  const setup = svrSetup({ fx: 'customize-healthcheck' });
  before(setup);
  after(setup.teardown);

  describe('when hit on a healthcheck path', () => {
    describe('and healthcheck does not include rejects', () => {
      request({ url: '/is-alive' }).responds({
        status: 200,
        headers: expectedHeaders,
        body: {
          'should contain name of the project package': body => {
            Should(body).have.property('name', '@prun/express.customized-healthcheck');
          },
          'should contain version of the project package': body => {
            Should(body).have.property('version', '1.7.6');
          },
          'should contain the data from the custom health-check handler': body => {
            Should(body).have.properties(['one', 'two']);
          },
        },
      });
    });

    describe('and healthcheck does include rejects', () => {
      request({ url: '/is-alive?fail=true' }).responds({
        status: 500,
        headers: expectedHeaders,
        body: {
          'should contain name of the project package': body => {
            Should(body).have.property('name', '@prun/express.customized-healthcheck');
          },
          'should contain version of the project package': body => {
            Should(body).have.property('version', '1.7.6');
          },
          'should contain the data from the custom health-check handler': body => {
            Should(body).have.properties(['one', 'two']);
          },
        },
      });
    });
  });
});
