module.exports = ({}, { one, two, hrtime, logger, errors: { PrunError } }) => {
  const log = logger.of('custom healthcheck');
  log.debug('initialized');

  return async ({ query = {} }) => {
    const dur = hrtime();
    log.debug('healthcek start');

    const result = await Promise.all([one.hc(), two.hc()])
      .then(([one, two]) => ({ one, two, dur: dur() }));

    if (query.fail) {
      log.warn({ query }, 'failing healthckeck for test purposes');
      throw new PrunError('Healthceck failed', result);
    }

    log.debug({ result }, 'healthceck end');
    return result;
  };
};
