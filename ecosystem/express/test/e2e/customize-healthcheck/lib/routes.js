module.exports = (options, {
  //TRICKY:
  routes = require('../../../../router')(),
  // - in project code:
  //routes = require('@prun/express/router')(),
  //TRICKYEnd
}) => {
  const dummyHanlder = () => ({ dummy: true });

  return routes
    .get('/some/path', dummyHanlder);
};
