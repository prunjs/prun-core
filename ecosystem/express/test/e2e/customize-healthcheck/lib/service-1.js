module.exports = ({}, { logger }) => {
  const log = logger.of('service-1');
  const start = Date.now();

  log.debug('initialized');

  return {
    hc: () => Promise.resolve({ ok: true, uptimeMs: Date.now() - start }),
  };
};
