module.exports = ({}, { logger }) => {
  const log = logger.of('service-2');
  let sampled = 0;
  log.debug('initialized');

  return {
    hc: () => Promise.resolve({ ok: true, sampled: ++sampled }),
  };
};
