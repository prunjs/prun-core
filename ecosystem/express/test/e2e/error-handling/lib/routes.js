module.exports = (options, {
  routes = require('@prun/express/router')(),
  errors: { prunError },
}) => {
  //routes for muteReqLog tests
  routes
    .get('/throws/2nd-level-error', {

    }, () => {
      throw prunError('I did that on purpose', { because: 'just because' });
    });

  //routes with custom serializers
  routes
    .get('/route-log-level', {
      log: { level: 'debug' },
      handler: req => {
        req.log.debug('debugged!');
        return { dummy: true };
      },
    })
    .get('/custom-redaction', {
      log: {
        redact: ['foo.bar.top', 'list[*].secret'],
      },
      handler: req => {
        req.log.warn({
          foo: { a: 1, b: 2, bar: { a: 'a', b: 'b', top: 'secret' } },
          list: [
            { name: 'one', secret: 'shhhh1' },
            { name: 'two', secret: 'shhhh2' },
            { name: 'six', secret: 'shhhh6' },
          ],
        }, 'custom-redaction');

        return { dummy: true };
      },
    })
    .get('/custom-serializers', {
      log: {
        serializers: {
          one: one => ({ only: one.only }),
          view: view => ({ reply: { view } }),
          headers: () => '[Redacted]',
          //hide foo.bar.top - the hard way, work around https://github.com/pinojs/pino/issues/831
          foo: foo => ({
            ...foo,
            bar: {
              ...foo.bar,
              top: '[Redacted]',
            },
          }),
        },
      },
      handler: (req, reply) => {
        req.log.warn({
          view: reply.view,
          headers: reply.headers,
          one: { one: 1, two: 2, three: 3, only: 'one' },
          foo: { a: 1, b: 2, bar: { a: 'a', b: 'b', top: 'secret' } },
        }, 'custom-serializers');

        return { dummy: true };
      },
    });

  return routes;
};
