module.exports = (options, {
  logger,
  errors: { noSuchEntityError },
  routes = require('@prun/express/router')(),
}) => {
  const log = logger.of('ctrl.built');
  const getTarget = ({ query: { to } }) => ({ target: to || 'world' });
  const controller = { getTarget };
  const { Readable } = require('stream');
  const stream = str => {
    const s = new Readable();
    s._read = () => {} //eslint-disable-line
    s.push(str);
    s.push(null);
    return s;
  };


  log.info('initialized');

  return routes
    .defaults({
      views: { ok: 'json' },
    })
    .get('/built-paths/html', {
      views: { ok: 'page' },
    }, getTarget)
    .get('/built-paths/with-validation', {
      views: { ok: 'json' },
      controller,
      validate: {
        query: {
          additionalProperties: false,
          properties: {
            to: { type: 'string', maxLength: 10, minLength: 2 },
          },
        },
      },
      handler: 'getTarget',
    })
    .get(
      '/built-paths/ctrl-and-method-ref',
      {
        views: { ok: 'json' },
        controller,
      },
      controller.getTarget,
    )
    .get('/built-paths/stream', {}, () => {
      log.debug('returning a stream');
      return stream(JSON.stringify({ message: 'that came from a stream' }));
    })
    .get('/built-paths/no-such-id', () => {
      throw noSuchEntityError({ id: 'no-such-id' });
    })
    .get('/built-paths', getTarget);
};
