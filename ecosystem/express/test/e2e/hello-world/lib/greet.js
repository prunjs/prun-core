module.exports = (options, { logger, errors: { PrunError, noSuchEntity } }) => {
  const log = logger.of('ctrl.greet');
  const getTarget = ({ query: { to } }) => ({ target: to || 'world' });
  const controller = { getTarget };

  log.info('initialized');

  return [{
    path: '/hello/html',
    views: { ok: 'page' },
    handler: getTarget,
  }, {
    path: '/hello/md',
    views: { ok: 'md' },
    handler: getTarget,
  // }, {
  //   path: '/hello/inspect',
  //   views: { ok: 'inspect' },
  //   handler: getTarget,
  }, {
    path: '/hello/no-such-view',
    views: { ok: 'json' },
    handler: async (req, res) => {
      res.view = 'testing-no-such-view';
      await Promise.resolve(); //stupid lint.
      return { oups: 'did not throw' };
    },
  }, {
    path: '/hello/no-such-entity',
    views: { ok: 'json' },
    handler: () => {
      return noSuchEntity({ id: 'foo' });
    },
  }, {
    path: '/hello/reject',
    views: { ok: 'json' },
    handler: () => Promise.reject(new PrunError('error-view', {
      with: 'some',
      context: 'data',
    })),
  }, {
    path: '/hello/sync-throw',
    views: { ok: 'json' },
    handler: () => {
      throw new PrunError('error-view', {
        with: 'some',
        context: 'data',
      });
    },
  }, {
    path: '/hello/in-chain-async-throw',
    views: { ok: 'json' },
    handler: ({ query: { ms = 200 } }) => new Promise(() => {
      setTimeout(() => {
        throw new PrunError('in-chain-async-error-view', {
          with: 'some',
          context: 'data',
        });
      }, ms);
    }),
  }, {
    path: '/hello/off-chain-error',
    views: { ok: 'json' },
    handler: ({ query: { ms = 200 } }) => {
      setTimeout(() => {
        throw new PrunError('off-chain-error-view', {
          with: 'some',
          context: 'data',
        });
      }, ms);
    },
  }, {
    method: 'get',
    path: '/hello/brute-mw-error',
    views: { ok: 'json' },
    mw: [
      (req, res, next) => next(new Error('unexpected low-level error')),
    ],
    handler: () => {
      //the error in this route should be brute error thrown from the mw
      throw new Error('This is NOT The error you are looking for');
    },
  }, {
    path: '/hello/named-ctrl-method',
    views: { ok: 'json' },
    controller,
    handler: 'getTarget',
  }, {
    path: '/hello/ctrl-and-method-ref',
    views: { ok: 'json' },
    controller,
    handler: controller.getTarget,
  }, {
    path: '/hello',
    views: { ok: 'json' },
    handler: getTarget,
  }];
};
