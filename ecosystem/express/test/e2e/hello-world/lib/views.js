module.exports = (options, {
  defineView = require('@prun/express/view'),
  //TBD: maybe even:
  //defineView = require('@prun/web').defineView,???
}) => ({
  page: defineView(
    {
      type: 'text',
      headers: {
        'content-type': 'text/html; charset=utf8',
      },
    },
    ({ target }) => `
      <html>
        <body>
          <h1>hello ${target}</h1>
          personalize greeting by adding QS param '?to=name'
        <body>
      </html>
    `.replace(/\n {6}/g, '\n').trim(),
  ),
  md: defineView(
    {
      type: 'text',
      headers: {
        'content-type': 'text/markdown',
        'x-by': 'a bored markdown poet',
      },
    },
    ({ target }) => `
      # hello ${target}
      personalize greeting by adding QS param '?to=name'
    `.replace(/\n {6}/g, '\n').trim(),
  ),
  json: defineView(
    ({ target }) => ({
      hello: target,
      hint: 'personalize greeting by adding QS param \'?to=name\'',
    }),
    // -> defaults to { type: 'json' }
  ),
});
