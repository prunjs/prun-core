const { promisify } = require('node:util');
const fs = require('node:fs/promises');
const Should = require('should');
const { svrSetup } = require('./util');

describe('controlling log-levels of endpoints', () => {
  const setup = svrSetup({
    fx: 'log-control',
    args: [
      '--logger.disablePritty', 'true',
      '--logger.levels.default', 'warn',
      '--logger.levels.request-log', 'info',
    ],
  });
  const svrStartPromise = mochaTest => promisify(next => setup.call(mochaTest, next))();
  const svrStopPromise = mochaTest => promisify(next => setup.teardown.call(mochaTest, next))();

  describe('when provided a mutedPaths collection', () => {
    describe('and hit on various endpoints', () => {
      const ctx = {};

      /*
            routes:
            .get('/not-muted/path', dummyHanlder)
            .get('/mute/path/starts-with/something', dummyHanlder)
            .use('/mute-cfg/with/methods/starts-with', dummyHanlder)
            .use('/mute-cfg/with/method-arr/starts-with', dummyHanlder)
            .use('/mute-x/with/a/regexp/anymethod', dummyHanlder)
            .use('/mute-x/with/a/RegExp/with/CASE-Flag', dummyHanlder)
            .use('/another-not-muted/path', dummyHanlder);

            mutedPaths:
            - /mute/path/starts-with
            - search: /mute-cfg/with/methods
              method: get|options
            - search: /mute-cfg/with/method-arr
              method: [get, options]
            - type: match
              search: ^/mute-x/.*\/regexp/anymethod$
            - type: match
              search: ^/mute-x/.*\/regexp/with/case-flag$ # test route ends with .*\/RegExp/with/CASE-Flag
              flags: i
              method: get
            */

      const cases = [
        { method: 'get', url: '/not-muted-path', shouldMute: false },
        { method: 'get', url: '/mute/path/starts-with/something', shouldMute: true },

        { method: 'get', url: '/mute-cfg/with/methods/starts-with', shouldMute: true },
        { method: 'options', url: '/mute-cfg/with/methods/starts-with', shouldMute: true },
        { method: 'post', url: '/mute-cfg/with/methods/starts-with', shouldMute: false },
        { method: 'put', url: '/mute-cfg/with/methods/starts-with', shouldMute: false },

        { method: 'options', url: '/mute-cfg/with/method-arr/starts-with', shouldMute: true },
        { method: 'get', url: '/mute-cfg/with/method-arr/starts-with', shouldMute: true },
        { method: 'put', url: '/mute-cfg/with/method-arr/starts-with', shouldMute: false },
        { method: 'post', url: '/mute-cfg/with/method-arr/starts-with', shouldMute: false },
        { method: 'delete', url: '/mute-cfg/with/method-arr/starts-with', shouldMute: false },

        { method: 'options', url: '/mute-x/with/a/regexp/anymethod', shouldMute: true },
        { method: 'get', url: '/mute-x/with/a/regexp/anymethod', shouldMute: true },
        { method: 'put', url: '/mute-x/with/a/regexp/anymethod', shouldMute: true },
        { method: 'post', url: '/mute-x/with/a/regexp/anymethod', shouldMute: true },
        { method: 'delete', url: '/mute-x/with/a/regexp/anymethod', shouldMute: true },

        { method: 'get', url: '/mute-x/with/a/RegExp/with/CASE-Flag', shouldMute: true },
        { method: 'post', url: '/mute-x/with/a/RegExp/with/CASE-Flag', shouldMute: true },

        { method: 'get', url: '/another-not-muted/path', shouldMute: false },

        { method: 'get', url: '/custom-serializers' },
        { method: 'get', url: '/custom-redaction' },
        { method: 'get', url: '/route-log-level' },
      ];

      before(async function beforeAll() {
        const self = this; //eslint-disable-line consistent-this,no-invalid-this

        await svrStartPromise(self);

        for (const req of cases) {
          const { url, shouldMute, ...options } = req;
          options.method = options.method.toUpperCase();
          await fetch(`http://localhost:8081${url}`, options); //eslint-disable-line no-await-in-loop
        }

        await svrStopPromise(self);

        const log = await fs.readFile(setup.logPath);

        ctx.log = log.toString()
          .trim()
          .split('\n')
          .map(s => JSON.parse(s));

        ctx.reqLog = ctx.log
          .filter(s => 'request served' === s.msg);
      });

      describe('muted paths', () => {
        it('none of the muted endpoints should emit a "request served" entry', () => {
          const failedCases = cases.filter(oCase => oCase.shouldMute)
            .filter(oCase => {
              oCase.logged = ctx.reqLog.find(
                ({ req }) => req.url === oCase.url && req.method.toLowerCase() === oCase.method,
              );
              return Boolean(oCase.logged);
            });
          Should(failedCases).eql([]);
        });

        it('all of the non-muted endpoints should emit a "request served" entry', () => {
          const failedCases = cases.filter(oCase => false === oCase.shouldMute)
            .filter(oCase => {
              oCase.logged = ctx.reqLog.find(
                ({ req }) => req.url === oCase.url && req.method.toLowerCase() === oCase.method,
              );
              return !oCase.logged;
            });
          Should(failedCases).eql([]);
        });
      });

      describe('when a route is configured with log serializers', () => {
        it('should apply route serialisers to logged data', () => {
          const entry = ctx.log.find(entry => 'custom-serializers' === entry.msg);

          Should(entry).have.properties({
            endpoint: { method: 'GET', path: '/custom-serializers' },
            view: { reply: { view: 'ok' } },
            headers: '[Redacted]',
            one: { only: 'one' },
            foo: { a: 1, b: 2, bar: { a: 'a', b: 'b', top: '[Redacted]' } },
          });
        });
      });

      describe('when a route is configured with log.redact', () => {
        it('should apply the redacters to logged data', () => {
          const entry = ctx.log.find(entry => 'custom-redaction' === entry.msg);
          Should(entry).have.properties({
            endpoint: { method: 'GET', path: '/custom-redaction' },
            foo: { a: 1, b: 2, bar: { a: 'a', b: 'b', top: '[Redacted]' } },
            list: [
              { name: 'one', secret: '[Redacted]' },
              { name: 'two', secret: '[Redacted]' },
              { name: 'six', secret: '[Redacted]' },
            ],
          });
        });
      });

      describe('when a route is configured with a log.level', () => {
        it('should apply the log level to the req.log', () => {
          const entry = ctx.log.find(entry => 'debugged!' === entry.msg);
          Should(entry).have.properties({ level: 20 }); //pino levels.debug -> 20
        });
      });
    });
  });
});
