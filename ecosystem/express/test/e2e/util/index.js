module.exports = {
  request: require('./request'),
  svrSetup: require('./svr-setup'),
  expectedStdHeaders: {
    'content-type': 'application/json; charset=utf-8',
    'content-length': /^\d+$/,
  },
};
