const SUT = process.env.SUT || 'http://localhost:8081'; //eslint-disable-line no-process-env
const fire = require('mocha-ui-exports-request');
module.exports = opts => fire({
  json: true,
  ...opts,
  url: `${SUT}${opts.url}`,
});
