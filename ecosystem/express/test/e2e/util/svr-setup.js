const helperFcty = require('e2e-helper');

module.exports = ({
  fx,
  logId = fx.replace(/\//g, '_'),
  logPath = `./e2e.${logId}.log`,
  args = [],
}) => Object.assign(
  helperFcty({
    svc: '../../node_modules/.bin/prun',
    args: ['--config', `./test/e2e/${fx}/.prunrc`].concat(args),
    readyNotice: 'web interface started',
    logPath,
  }),
  { logPath },
);
