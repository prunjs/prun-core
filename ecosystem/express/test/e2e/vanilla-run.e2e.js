/* eslint  no-constant-binary-expression: off */
const Should = require('should');
const { request, svrSetup } = require('./util');
const expectedHeaders =  {
  'content-type': 'application/json; charset=utf-8',
  'content-length': /^\d+$/,
};

describe('a hello-world vanilla project', () => {
  const setup = svrSetup({ fx: 'hello-world' });
  before(setup);
  after(setup.teardown);

  describe('when hit on', () => {
    describe('a built-in route', () => {
      describe('the health-check route (/health-check)', () => {
        request({ url: '/health-check' })
          .responds({
            status: 200,
            headers: expectedHeaders,
            body: {
              'should contain name of the project package': body => {
                Should(body).have.property('name', '@prun/express.hello-world');
              },
              'should contain version of the project package': body => {
                Should(body).have.property('version', '1.7.1');
              },
            },
          });
      });

      describe('a route that does not exist', () => {
        const rndPathElement = Buffer.from(Math.random().toString()).toString('base64');
        const url = `/no/such/${rndPathElement}/endpoint`;
        request({ url })
          .responds({
            status: 404,
            headers: expectedHeaders,
            body: {
              'should be a no-such-endpoint view': {
                'error: no such endpoint': body => {
                  Should(body).property('error', 'No Such Endpoint');
                },
                'endpoint.method: the attempted url': body => {
                  Should(body).have.property('endpoint')
                    .have.property('method', 'GET');
                },
                'endpoint.url: the attempted url': body => {
                  Should(body).have.property('endpoint')
                    .have.property('url', url);
                },
              },
            },
          });
      });
    });

    describe('a project route', () => {
      const helloWorldResponse = {
        status: 200,
        headers: expectedHeaders,
        body: {
          'should be the view from the expected handler': body => Should(body).eql({
            hello: 'world',
            hint: 'personalize greeting by adding QS param \'?to=name\'',
          }),
        },
      };

      describe('that is defined by providing only handler', () => {
        describe('and processing ends successfully', () => {
          request({
            url: '/hello',
          })
            .responds(helloWorldResponse);
        });
      });

      describe('that is defined by providing a controller and a handler reference', () => {
        describe('and processing ends successfully', () => {
          request({
            url: '/hello/ctrl-and-method-ref',
          })
            .responds(helloWorldResponse);
        });
      });

      describe('that is defined by providing a controller and a handler name', () => {
        describe('and processing ends successfully', () => {
          request({
            url: '/hello/named-ctrl-method',
          })
            .responds(helloWorldResponse);
        });
      });

      describe('that is defined with a schema validation', () => {
        describe('and request schema validation fails', () => {
          request({
            url: '/built-paths/with-validation',
            qs: { to: 'm' },
          })
            .responds({
              status: 400,
              body: {
                'should be a bad-request view for the request failures': {
                  'error: Invalid Request': body => {
                    Should(body).have.property('error', 'Invalid Request');
                  },
                  'rejects: Array of rejects': body => {
                    Should(body)
                      .have.property('rejects')
                      .have.property('query')
                      .be.an.Array()
                      .property('length')
                      .greaterThan(0);
                  },
                },
              },
            });
        });

        describe('and request schema validation passes', () => {
          request({
            url: '/built-paths/with-validation',
            qs: { to: 'me' },
          })
            .responds({
              ...helloWorldResponse,
              body: {
                'should use passed params in view': body => {
                  Should(body)
                    .have.property('hello', 'me');
                },
              },
            });
        });
      });

      describe('and endpoint throws NoSuchEntity error', () => {
        request({
          url: '/built-paths/no-such-id',
        })
          .responds({
            status: 404,
            headers: expectedHeaders,
            body: {
              'should pass endpoint properties': body => {
                Should(body)
                  .have.property('endpoint')
                  .have.properties({
                    method: 'GET',
                    path: '/built-paths/no-such-id',
                  });
              },
              'should pass the entity descriptor': body => {
                Should(body)
                  .have.property('entity')
                  .have.property('id', 'no-such-id');
              },
            },
          });
      });

      describe('and processing ends successfully with the default JSON view', () => {
        request({
          url: '/hello',
        })
          .responds(helloWorldResponse);
      });

      describe('and processing ends successfully using a stream', () => {
        request({
          url: '/built-paths/stream',
        })
          .responds({
            status: 200,
            heades: expectedHeaders,
            body: {
              'should be the streamed body': body => {
                Should(body).have.property('message', 'that came from a stream');
              },
            },
          });
      });

      describe('and processing ends successfully with a custom html view', () => {
        request({
          json: false,
          url: '/hello/html',
        })
          .responds({
            status: 200,
            headers: {
              ...expectedHeaders,
              'content-type': 'text/html; charset=utf-8',
            },
            body: {
              'should not be stringified to json': body => {
                Should(body.trim())
                  .match(/^</) //start with html tag
                  .match(/>$/); //end with html tag
              },
              'should contain the expected view in the correct format': body => {
                Should(body).match(/<h1>hello world<.h1>/);
              },
            },
          });
      });

      describe('and processing ends successfully with a custom view', () => {
        request({
          json: false,
          url: '/hello/md',
        })
          .responds({
            status: 200,
            headers: {
              ...expectedHeaders,
              'content-type': 'text/markdown; charset=utf-8',
              'x-by': 'a bored markdown poet',
            },
            body: {
              'should not be stringified to json': body => {
                Should(body.trim())
                  .match(/^[^"]/) //not start with a string quote
                  .match(/[^"]$/); //not end with a string quote
              },
              'should contain the expected view in the correct format': body => {
                Should(body).match(/# hello world/i);
              },
            },
          });
      });

      describe('and controller handler rejects with a baked-in PrunError', () => {
        request({
          url: '/hello/reject',
        })
          .responds({
            status: 500,
            headers: expectedHeaders,
            body: {
              'should be the view from the expected handler': body => Should(body)
                .have.properties({
                  error: 'PrunError',
                  message: 'error-view',
                })
                .property('err')
                .be.an.Object()
                .have.properties({
                  with: 'some',
                  context: 'data',
                }),
            },
          });
      });

      describe('and controller handler tries to setView to a non-existing view,'
              + ' or from within a promise chain link', () => {
        request({
          url: '/hello/no-such-view',
        })
          .responds({
            status: 501,
            headers: expectedHeaders,
            body: {
              'should have error:NoSuchWebViewError': body => {
                Should(body)
                  .have.property('error', 'NoSuchWebViewError');
              },
              'should have message:/view.*not a part of configuration/': body => {
                Should(body)
                  .have.property('message')
                  .match(/view.*not a part of configuration/i);
              },
              'should include route details': body => {
                Should(body)
                  .have.property('endpoint')
                  .be.an.Object()
                  .have.properties({
                    method: '[*]',
                    path: '/hello/no-such-view',
                  });
              },
              'should include the name of the missing view': body => {
                Should(body)
                  .have.properties(['foundViews'])
                  .have.properties({
                    missingView: 'testing-no-such-view',
                  });
              },
            },
          });
      });

      describe('and controller handler throws a baked-in PrunError synchronously,'
              + ' or from within a promise chain link', () => {
        request({
          url: '/hello/sync-throw',
        })
          .responds({
            status: 500,
            headers: expectedHeaders,
            body: {
              'should be the view from the error handler': body => Should(body)
                .have.properties({
                  error: 'PrunError',
                  message: 'error-view',
                })
                .property('err')
                .be.an.Object()
                .have.properties({
                  with: 'some',
                  context: 'data',
                }),
            },
          });
      });

      //TBD - need to associate errors from timeouts to req/res pair
      describe.skip(
        'and controller handler throws a baked-in PrunError asynchronously in a timeout, but in the promise chain',
        () => {
          request({
            url: '/hello/in-chain-async-throw',
          })
            .responds({
              status: 500,
              headers: expectedHeaders,
              body: {
                'should be the view from the error handler': body => Should(body)
                  .have.properties({
                    error: 'PrunError',
                    message: 'in-chain-async-error-view',
                  })
                  .property('err')
                  .be.an.Object()
                  .have.properties({
                    with: 'some',
                    context: 'data',
                  }),
              },
            });
        },
      );

      //TBD - need to associate errors from timeouts to req/res pair
      describe.skip(
        'and controller handler throws a baked-in PrunError asynchronously, in a timeout outside the promise chain',
        () => {
          request({
            url: '/hello/off-chain-error',
          })
            .responds({
              status: 500,
              headers: expectedHeaders,
              body: {
                'should be the view from the error handler': body => Should(body)
                  .have.properties({
                    error: 'PrunError',
                    message: 'off-chain-async-error-view',
                  })
                  .property('err')
                  .be.an.Object()
                  .have.properties({
                    with: 'some',
                    context: 'data',
                  }),
              },
            });
        },
      );

      describe('and the path processing results with an unexpected low-level error', () => {
        request({
          url: '/hello/brute-mw-error',
        })
          .responds({
            status: 500,
            headers: expectedHeaders,
            body: {
              'should be the view from the error handler': body => Should(body)
                .have.properties({
                  error: 'there has been an error during this request',
                  code: 'ERR_UNEXPECTED_LOW_LEVEL_ERROR',
                  endpoint: { method: 'GET', path: '/hello/brute-mw-error' },
                })
                .property('cause')
                .be.an.Object()
                .have.properties({
                  error: 'Error',
                  message: 'unexpected low-level error',
                }),
            },
          });
      });
    });
  });
});
