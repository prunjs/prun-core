const Should = require('should');
const SUT = require('../../lib/mw/req-log-mappers');

describe('lib/mw/req-log-mappers', () => {
  it('should be a static singleton object', () => Should(SUT).be.an.Object());
  describe('.reqView', () => {
    it('should be a function that names 1 arg - req', () => {
      Should(SUT).property('reqView')
        .be.a.Function()
        .have.property('length', 1);
    });

    describe('when used with a request object without correlationId or originalUrl', () => {
      const ctx = {};
      before(() => {
        try {
          ctx.view = SUT.reqView({
            httpVersion: '1.1',
            method: 'OPTIONS',
            url: '/mock/url',
            headers: { 'x-mock': 'header' },
          });
        } catch (e) {
          ctx.err = e;
        }
      });

      shouldNotFail(ctx);
      shouldMapAllRequestProperties(ctx);

      it('should populate correlationId with `n/a`', () => {
        Should(ctx.view).have.property('correlationId', 'n/a');
      });
      it('should populate url based on req.url', () => {
        Should(ctx.view).have.property('url', '/mock/url');
      });
    });

    describe('when used with an request object with correlationId method and originalUrl', () => {
      const ctx = {};
      before(() => {
        try {
          ctx.view = SUT.reqView({
            httpVersion: '1.1',
            method: 'OPTIONS',
            url: '/mock/url',
            originalUrl: '/original/mock/url',
            correlationId() { return 'mock-correlation-id'; },
            headers: { 'x-mock': 'header' },
          });
        } catch (e) {
          ctx.err = e;
        }
      });

      shouldNotFail(ctx);
      shouldMapAllRequestProperties(ctx);

      it('should populate correlationId with value pulled from correlationId()', () => {
        Should(ctx.view).have.property('correlationId', 'mock-correlation-id');
      });
      it('should populate url based on req.url', () => {
        Should(ctx.view).have.property('url', '/original/mock/url');
      });
    });
  });

  describe('.resView', () => {
    it('should be a function that names 1 arg - res', () => {
      Should(SUT).property('resView')
        .be.a.Function()
        .have.property('length', 1);
    });
    describe('when used with a not finished response object with no _header or body', () => {
      const ctx = {};
      before(() => {
        try {
          ctx.view = SUT.resView({
            finished: false,
            statusCode: 999,
            statusMessage: 'WTF',
            // _header: <---
            //view: 'myView',
          });
        } catch (e) {
          ctx.err = e;
        }
      });

      shouldNotFail(ctx);

      it('should map to { finished: false }', () => {
        Should(ctx.view).eql({ finished: false });
      });
    });

    describe('when used with an finished response object with _header and body', () => {
      const ctx = {};
      before(() => {
        try {
          ctx.view = SUT.resView({
            finished: true,
            statusCode: 999,
            statusMessage: 'WTF',
            _header: [
              'HTTP/1.1 999 WTF',
              'x-mock: header1',
              'y-mock: header2',
            ].join('\r\n'),
            view: 'myView',
          });
        } catch (e) {
          ctx.err = e;
        }
      });

      shouldNotFail(ctx);
      shouldMapBaseResponseProperties(ctx);

      it('should find computed properties correctly', () => {
        Should(ctx.view).have.properties({
          replyLine: 'HTTP/1.1 999 WTF',
          protocol: 'HTTP/1.1',
          view: 'myView',
        });
      });
    });
  });
});

function shouldNotFail(ctx) {
  it('should not fail', () => {
    Should.not.exist(ctx.err);
  });
}

function shouldMapAllRequestProperties(ctx) {
  it('should still map all properties, ', () => {
    Should(ctx.view).have.properties([
      'httpVersion',
      'method',
      'url',
      'forwardedSecure',
      'correlationId',
      'headers',
      'body',
      'complete',
    ]);
  });
}

function shouldMapBaseResponseProperties(ctx) {
  it('should still map all properties, ', () => {
    Should(ctx.view).have.properties([
      'replyLine',
      'protocol',
      'statusCode',
      'statusMessage',
      'headers',
      'view',
    ]);
  });
}
