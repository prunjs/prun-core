/* eslint no-empty-function: off */
const Should = require('should');
const SUT = require('../../lib/routing/index.js');
const logger = {
  of: () => logger,
  trace: () => {},
  debug: () => {},
  info: () => {},
  warn: () => {},
  error: () => {},
  fatal: () => {},
  levels: {
    values: { trace: 10, debug: 20, info: 30, warn: 40, error: 50, fatal: 60 },
  },
};
const errors = {
  configError: (msg, meta) => Object.assign(new Error(msg), meta),
};

describe('lib/routing/index', () => {
  describe('when called with a default error-view that is not found', () => {
    const ctx = setupErrorCase({
      options: {
        defaultErrorView: 'no-such-view',
      },
      ioc: {
        views: {
          view1: () => {},
          view2: () => {},
          view42: () => {},
        },
      },
    });

    describe('the thrown error', () => {
      it('should state the passed default error view as `defaultErrorView`', () => {
        Should(ctx.err).have.property('defaultErrorView', 'no-such-view');
      });
      it('should list the existing views as `foundViews`', () => {
        Should(ctx.err).have.property('foundViews', ['view1', 'view2', 'view42']);
      });
    });
  });

  describe('when called with in invalid route config', () => {
    describe('that provides route.controller as a string that names a path not found on the IoC', () => {
      const ctx = setupErrorCase({
        options: {},
        ioc: {
          controller1: {
            method1: () => {},
          },
        },
        routeCfg: {
          method: 'get',
          path: '/some/path',
          controller: 'no-such-ioc',
        },
      });

      describe('the thrown error', () => {
        it('should state the endpoint info as `err.endpoint`', () => {
          Should(ctx.err).have.property('endpoint', {
            method: 'GET',
            path: '/some/path',
          });
        });
        it('should state the passed controller name as `err.controllerName`', () => {
          Should(ctx.err).have.property('controllerName', 'no-such-ioc');
        });
      });
    });

    describe('that provides a route.handler as a method name without providing a controller', () => {
      const ctx = setupErrorCase({
        options: {},
        ioc: {
          ctrl1: {
            method1: () => {},
          },
        },
        routeCfg: {
          method: 'get',
          path: '/some/path',
          //controller: 'ctrl1', <--- missing on purpose!
          handler: 'method1',
        },
      });

      describe('the thrown error', () => {
        it('should state the endpoint info as `err.endpoint`', () => {
          Should(ctx.err).have.property('endpoint', {
            method: 'GET',
            path: '/some/path',
          });
        });
        it('should state the passed method name as `err.methodName`', () => {
          Should(ctx.err).have.property('methodName', 'method1');
        });
      });
    });

    describe('that provides a route.handler as a method name that is not found on the controller', () => {
      const ctx = setupErrorCase({
        options: {},
        ioc: {
          ctrl1: {
            method1: () => {},
          },
        },
        routeCfg: {
          method: 'get',
          path: '/some/path',
          controller: 'ctrl1',
          handler: 'no-such-method', //<--- missing on ctrl1
        },
      });

      describe('the thrown error', () => {
        it('should state the endpoint info as `err.endpoint`', () => {
          Should(ctx.err).have.property('endpoint', {
            method: 'GET',
            path: '/some/path',
          });
        });
        it('should state the passed method name as `err.methodName`', () => {
          Should(ctx.err).have.property('methodName', 'no-such-method');
        });
      });
    });

    describe('that provides a route.handler as a method name that is not found on the controller', () => {
      const ctx = setupErrorCase({
        options: {},
        ioc: {
          ctrl1: {
            method1: () => {},
          },
        },
        routeCfg: {
          method: 'get',
          path: '/some/path',
          controller: 'ctrl1',
          //handler <--- missing at all
        },
      });

      describe('the thrown error', () => {
        it('should state the endpoint info as `err.endpoint`', () => {
          Should(ctx.err).have.property('endpoint', {
            method: 'GET',
            path: '/some/path',
          });
        });
      });
    });
    //TBD: broken formatters

    //TBD: broken route views

    describe('that provides a route.log.level that is not a valid logger level', () => {
      const ctx = setupErrorCase({
        options: {},
        ioc: {
          ctrl1: {
            method1: () => {},
          },
        },
        routeCfg: {
          method: 'get',
          path: '/some/path',
          controller: 'ctrl1',
          handler: 'method1',
          log: { level: 'no-such-level' },
        },
      });

      describe('the thrown error', () => {
        it('should state the endpoint info as `err.endpoint`', () => {
          Should(ctx.err).have.property('endpoint', {
            method: 'GET',
            path: '/some/path',
          });
        });
        it('should state the wrong level as `err.logLevel`', () => {
          Should(ctx.err).have.property('logLevel', 'no-such-level');
        });
        it('should state the logger levels supported in the project as `err.supportedLevels`', () => {
          Should(ctx.err).have.property('supportedLevels', Object.keys(logger.levels.values));
        });
      });
    });

    describe('that provides a route.log.level that is not a valid logger level', () => {
      const ctx = setupErrorCase({
        options: {},
        ioc: {
          ctrl1: {
            method1: () => {},
          },
        },
        routeCfg: {
          method: 'get',
          path: '/some/path',
          controller: 'ctrl1',
          handler: 'method1',
          log: {
            serializers: {
              prop1: () => {},
              prop2: 'not-a-function',
              prop3: null,
            },
          },
        },
      });

      describe('the thrown error', () => {
        it('should state the endpoint info as `err.endpoint`', () => {
          Should(ctx.err).have.property('endpoint', {
            method: 'GET',
            path: '/some/path',
          });
        });

        it('should state the invalid serializers as `err.invalidSerializers`', () => {
          Should(ctx.err).have.property('invalidSerializers', ['prop2', 'prop3']);
        });
      });
    });
  });
});


function setupErrorCase({ options, ioc, routeCfg }) {
  const ctx = {};
  before(() => {
    try {
      SUT({
        ...options,
      }, {
        logger,
        errors,
        config: {
          logger: { levels: { default: 'warn' } },
        },
        views: {
          genericError: () => {},
        },
        formatters: {
          json: () => {},
        },
        //TRICKY: mocking the routes gathered from other IoC entries
        modulesFor: () => [[routeCfg]],
        //TRICKYEnd
        ...ioc,
      });
    } catch (err) {
      ctx.err = err;
    }
  });

  it('should throw a PrunConfigError', () => {
    Should(ctx.err).be.an.Error();
  });

  return ctx;
}
