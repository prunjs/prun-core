const Should = require('should');
const SUT = require('../../lib/views');

describe('lib/views', () => {
  it(
    'should be a factory function that accepts ioc and options',
    () => Should(SUT).be.a.Function().property('length', 2),
  );

  it(
    'should expose a static method .defineView(..) that expects at least 1 argument',
    () => Should(SUT)
      .have.a.property('defineView')
      .be.a.Function().property('length', 1),
  );

  describe('.defineView(options, [handler])', () => {
    const { defineView } = SUT;
    const handlers = {
      current: null,
      next() {
        handlers.current = m => m;
        return handlers.current;
      },
    };
    const headers = {};

    describe('when called with a mapper function that has wrong number of arguments', () => {
      let result;
      before(() => {
        try {
          result = defineView(() => {}); //eslint-disable-line no-empty-function
        } catch (e) {
          result = e;
        }
      });

      it(
        'should throw a friendly error with propper message',
        () => Should(result)
          .be.an.Error()
          .have.property('message').match(/number of named args for handler is wrong/i),
      );
    });
    [{
      title: 'when called without arguments',
      args: [],
      expect: { /* use only defaults */ },
    }, {
      title: 'when called with form defineView(bodyMapper)',
      args: [handlers.next()],
      expect: {
        handler: handlers.current,
      },
    }, {
      title: 'when called with form defineView({code})',
      args: [{ code: 201 }],
      expect: {
        code: 201,
      },
    }, {
      title: 'when called with form defineView({headers})',
      args: [{ headers }],
      expect: {
        headers,
      },
    }, {
      title: 'when called with form defineView({code, headers}, bodyMapper)',
      args: [{ code: 123, headers }, handlers.next()],
      expect: {
        code: 123,
        headers,
        handler: handlers.current,
      },
    }, {
      title: 'when called with form defineView(bodyMapper, {code, headers})',
      args: [handlers.next(), { code: 123, headers }],
      expect: {
        code: 123,
        headers,
        handler: handlers.current,
      },
    }, {
      title: 'when called with form defineView({bodyMapper, code, headers})',
      args: [{ bodyMapper: handlers.next(), code: 123, headers }],
      expect: {
        code: 123,
        headers,
        handler: handlers.current,
      },
    }].forEach(({ title, args, expect: { code, headers, handler } }) => {
      describe(title, () => {
        let result;
        before(() => { result = defineView(...args); });

        it('should always return a mapper function', () => Should(result).be.a.Function());

        handler
          ? it('should use the passed handler', () => Should(result).equal(handler))
          : it('should use the `asIs` default handler', () => Should(result).have.property('name', 'asIs'));

        code
          ? it('should use options.code', () => Should(result).have.property('code', code))
          : it('should use default code: 200', () => Should(result).have.property('code', 200));

        headers
          ? it('should use options.headers', () => Should(result).have.property('headers').equal(headers))
          : it('should not add any headers', () => Should(result).have.property('headers').eql({}));
      });
    });
  });
});
