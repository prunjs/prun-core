import { join, sep } from 'node:path';
import { readFileSync } from 'node:fs';

const { version: httpAxiosVersion } = JSON.parse(
  readFileSync(join(import.meta.dirname, '../package.json')),
);

export default async function factory(
  { axiosConfig, internalDomains, baseStaticHeaders, contextHeaders, axiosClient, axiosInfo },
  {
    logger,
    context,
    process,
    config: {
      pkg: { name, version },
    },
  },
) {
  const { default: axios } = await import(axiosClient);
  const log = logger.of('http-axios');
  log.info('initializing');

  if (axiosInfo === 'skip') axiosInfo = '';
  else axiosInfo = `@prun/http-axios/v${httpAxiosVersion}; ${axiosInfo.name}/v${axiosInfo.version};`;

  const userAgent = `${name}/v${version}; nodejs/${process.version}; ${axiosInfo}`;

  log.debug({ userAgent }, 'composed user agent');


  const client = axios.create(axiosConfig);
  client.interceptors.request.use(config => {
    const { hostname } = new URL(config.baseURL ?? config.url);
    if (baseStaticHeaders) {
      Object.assign(config.headers, baseStaticHeaders);
    }

    const isInternalApiCall = internalDomains.find(internalDomain => hostname.endsWith(internalDomain));
    if (contextHeaders && isInternalApiCall) {
      Object.assign(config.headers, {
        ...context.populateValuesFromContextKeys(contextHeaders),
        ...config.headers,
      });
    }

    config.headers['User-Agent'] = userAgent;
    log.trace({ headers: config.headers }, 'request interceptor - headers augmented');
    return config;
  });
  return client;
}

//eslint-disable-next-line complexity
export function validate(
  {
    axiosConfig = {},
    axiosClient = 'axios',
    axiosInfo,
    internalDomains = ['.local'],
    baseStaticHeaders = {},
    baseContextHeaders = {
      'x-correlation-id': 'correlationId',
      'x-flow-name': 'flowName',
      'x-flow-initiator': 'flowInitiator',
    },
    customContextHeaders = {},
    contextHeaders = { ...baseContextHeaders, ...customContextHeaders },
  },
  { errors: { configError } },
) {
  //normalize
  try {
    axiosClient = import.meta.resolve(axiosClient);
  } catch (innerError) {
    throw configError('could not resolve axios client module', {
      description: [
        '@prun/http-axios is expected to be a peer dependency of the `axios` client',
        'variant you provide in `options.axiosClient (default: axios)`, however,',
        'its place on disk cannot be resolved.',
        ' - make sure the named module is installed in your project and is',
        '   available on the import path',
        ' - if you provided a custom variant - check the name is correct',
      ],
      providedAxiosClient: axiosClient,
      innerError,
    });
  }

  if (!axiosInfo) axiosInfo = loadAxiosInfo(axiosClient, configError);
  if ('string' == typeof internalDomains) internalDomains = [internalDomains];

  //validate
  const isNotAValidHeadersMapper = (name, obj) =>
    ('object' != typeof obj
      || Array.isArray(obj)
      || Object.values(obj).find(v => 'string' != typeof v))
    && `options.${name} should be a mapping of a string key on the context to a string http-header name`;

  const reject =
    !(Array.isArray(internalDomains) && !internalDomains.find(s => 'string' != typeof s))
    && 'options.internalDomains is not an array of internal domain strings'
    || typeof axiosConfig !== 'undefined' && typeof axiosConfig !== 'object'
      && 'options.axiosConfig must be a valid Axios client config object or undefined'
    || isNotAValidHeadersMapper('baseContextHeaders', baseContextHeaders)
    || isNotAValidHeadersMapper('customContextHeaders', customContextHeaders)
    || isNotAValidHeadersMapper('contextHeaders', contextHeaders)
    || !(
      'skip' === axiosInfo
      || 'string' === typeof axiosInfo.name && 'string' === typeof axiosInfo.version
    )
      && 'when provided, options.axiosInfo may be either the string "skip",'
        + ' or an object with `name` and `version` string properties';
  if (reject) {
    throw configError(reject, {
      description: [
        'invalid options for @prun/http-axios',
        'supported options:',
        '- defaultTimeout: optional number,  timeout in miliseconds, default to 2000',
        '- internalDomains: optional string[], list of all the internal domains that',
        '    calling them should apply correlation headers, default to ["internal"].',
        '- baseContextHeaders: optional object, mapping of keys on cls context to names',
        '    of http headers. Both keys and values should be strings. This comes with',
        '    a few useful defaults you can cascade by the next option',
        '- customContextHeaders: optional object, mapping of keys on cls context to',
        '    names of http headers. Both keys and values should be strings.',
        '- contextHeaders: it is a merge of the previous two where the later cascades',
        '    however, you can override it completely if you so wish.',
        '- axiosClient: the axios client variant to use, as it is expected to be resolved',
        '    by the import mechanism',
        '- axiosInfo: either the string `"skip"`, ',
        '    or an object with string props: name, version',
        'Note:',
        ' * when contextHeaders is provided, then both baseContextHeaders and',
        '    customContextHeaders are ignored.',
      ],
    });
  }

  return {
    axiosConfig,
    internalDomains,
    baseStaticHeaders,
    contextHeaders,
    axiosClient,
    axiosInfo,
  };
}

function loadAxiosInfo(axiosClient, configError) {
  const axiosPkgPath = axiosClient
    .slice(6)
    .replaceAll(sep, '/') //
    .replace(/\/node_modules\/([^/]+)\/.*$/, (_, n) => `/node_modules/${n}/package.json`);

  try {
    const { name, version } = JSON.parse(readFileSync(axiosPkgPath));
    return { name, version };
  } catch (innerError) {
    throw configError('could not detect name and version for your axios client', {
      description: [
        'The package.json of the axios-client cannot be read.',
        'the provided name is resolved successfully, however',
        ' - its package.json could be read or parsed',
        'you can override this mechanism by providing:',
        '  options:',
        '    axiosInfo:',
        '      name: <the name to appear in user-agent header>',
        '      version: <the version to appear in user-agent header>',
        'you can disable this behavior by providing:',
        '  options:',
        '    axiosInfo: skip',
      ],
      axiosClient,
      axiosPkgPath,
      innerError,
    });
  }
}

export const env = {
  internalDomains: {
    name: 'PRUN_HTTP_INTERNAL_DOMAIN',
    parser: v => v.split(',').map(v => v.trim()),
  },
  defaultTimeout: {
    name: 'PRUN_HTTP_DEFAULT_TIMEOUT',
    parse: true,
  },
  baseHeaders: {
    name: 'PRUN_HTTP_BASE_HEADERS',
    parse: true,
  },
};
