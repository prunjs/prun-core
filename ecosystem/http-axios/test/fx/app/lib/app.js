export default (_blank, { swapi }) => {
  return async query => {
    const resp = await swapi.get(`${query}`);
    return resp.data;
  };
};
