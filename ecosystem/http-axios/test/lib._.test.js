import { claim } from '@prun/bdd-helper';

import * as SUT from '../lib/index.js';

describe('http-axios', () => {
  claim(SUT).isPrunFactory({
    hasValidator: true,
    mapsEnv: [
      'internalDomains',
      'defaultTimeout',
      'baseHeaders',
    ],
  });
});
