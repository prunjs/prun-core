import { loadCustomEnv } from '@prun/prun/lib/usr-app/loader.js';

import { env as SUT } from '../lib/index.js';

describe('http-axios.env', () => {
  describe('.internalDomains', () => {
    describe('when acted on an environment with PRUN_HTTP_INTERNAL_DOMAIN populated with a csv list', () => {
      const config = {
        envVars: {
          PRUN_HTTP_INTERNAL_DOMAIN: '.local,.internal,.docker.local',
        },
      };

      before(() => {
        loadCustomEnv('testComponent', SUT, { config });
      });

      it('should map the internal domain into an Array on the component options', () => {
        Should(config).have.property('testComponent')
          .have.property('internalDomains').eql([
            '.local',
            '.internal',
            '.docker.local',
          ]);
      });
    });
  });
});
