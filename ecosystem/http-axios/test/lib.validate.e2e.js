//import { describe, it, before, after } from 'node:test';
import { claim, iocFactory } from '@prun/bdd-helper';

import { validate as SUT } from '../lib/index.js';
import { describe } from 'mocha';

//Object.assign(global, { describe, it, before, after })

describe('http-axios.validate(options, ioc)', () => {
  it('should be a factory function that expects an options object and ioc bag', () => {
    claim(SUT).isFunction(2);
  });

  const ioc = iocFactory();

  describe('when called with invalid options', () => {
    describe('with options.axiosClient that canot be loaded', () => {
      claim(SUT).behavior({
        options: {
          axiosClient: 'no-such-module',
        },
        ioc,
        expect: {
          reject: /PrunConfigError: could not resolve axios client module/,
        },
      });
    });

    describe('with options.axiosClient whose version canot be resolved', () => {
      claim(SUT).behavior({
        options: {
          axiosClient: '../test/fx/client-without-pkg-json/index.js',
        },
        ioc,
        expect: {
          reject: /PrunConfigError: could not detect name and version for your axios client/,
        },
      });
    });

    describe('with options.customContextHeaders that is not a Record<string, string>', () => {
      claim(SUT).behavior({
        options: {
          customContextHeaders: {
            foo: 'x-foo',
            oups: {},
          },
        },
        ioc,
        expect: {
          reject:
            /.customContextHeaders should be a mapping of a string key on the context to a string http-header/,
        },
      });
    });

    describe('with options.internalDomains that is not a string nor an Array of strings', () => {
      claim(SUT).behavior({
        options: {
          internalDomains: [12],
        },
        ioc,
        expect: {
          reject: /options.internalDomains is not an array of internal domain strings/,
        },
      });
    });

    describe('with options.axiosConfig that is not an object or undefined', () => {
      claim(SUT).behavior({
        options: {
          axiosConfig: -1,
        },
        ioc,
        expect: {
          reject: /PrunConfigError: options.axiosConfig/,
        },
      });
    });

    describe('with options.axiosInfo that is not `skip` nor an object with `name` and `version`', () => {
      claim(SUT).behavior({
        options: {
          axiosInfo: { name: 'my-custom-axios' }, //no .version
        },
        ioc,
        expect: {
          reject: /PrunConfigError: when provided, options.axiosInfo may be either the string "skip", or an object with `name` and `version`/, //eslint-disable-line max-len
        },
      });
    });
  });

  describe('when called with a valid options', () => {
    describe('a valid minialist options and ioc bag', () => {
      claim(SUT).behavior({
        //options: nothing!
        ioc,
        expect: {
          props: {
            axiosConfig: Object,
            internalDomains: Array,
            contextHeaders: {
              'x-correlation-id': String,
              'x-flow-name': String,
              'x-flow-initiator': String,
            },
            axiosClient: String,
            axiosInfo: { name: String, version: String },
          },
        },
      });
    });

    describe('an options with a an undefined `options.axiosConfig`', () => {
      claim(SUT).behavior({
        options: {},
        ioc,
        expect: {
          props: { axiosConfig: Object },
        },
      });
    });

    describe('an options with a string `options.internalDomains`', () => {
      claim(SUT).behavior({
        options: { internalDomains: '.internal' },
        ioc,
        expect: {
          props: {
            internalDomains: Array,
          },
        },
      });
    });
  });
});
