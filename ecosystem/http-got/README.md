# @prun/http-got

A `prunjs` component for a `got` httm-client. 

# Concerns
The concerns of this component are:

1. Augment http request headers with any tracking headers to all requests to internal services.
2. Augment http request headers with a verbose `User-Agent` header, listing versions of the project, node runtime and the `got` http client
3. Set a default request timeout for every request that does not specify an explicit one.

## Composing `User-Agent` header

The user agent should include name and version for the following components:
1. the project (taken from the package.json)
2. the node runtime (taken from `process.version`)
3. the `got` http client library (taken from the `package.json` of the `got` http-client)

## Tracking heaers

Many APM systems use a combination of network packet analytics and instrumentation of low-level hooks to analyse what internal requests are the result of a user-request.
However, because of this nature, they come with a significant performance penalty.

Tracking http headers is a **no-voodoo**, **low-footprint** way to analyse how requests trickle down in a service mesh.
The http-requests they employ differ from organization to organization, tipically one of `x-correlation-id`, `x-transaction-id`, `x-trx-id` - a unique identifier of the user-request.
Additional headers may be `x-flow-name`, `x-flow-initiator`, `x-request-id`.

This component imnplements the http-client part of the following two parts:
1. the interface layer should add the base values on local context (in IoC name `context`). Tyipically, it's done in the `http` interface, however, any interface should be able to provide them.
2. http client picks up these values, and mounts them as http headers.

Then, given a log-centralizaion that holds the logs from all the services in the mesh, you can filter by these headers to get a complete narrative.

# Configurations
* `defaultTimeout` : `number`, default: `2000`
  * The timeout in in milliseconds
* `internalDomains` : `string[]`, default: `['.local']`
  * a request will be augmented with tracking headers if it is made to any of the provided domains or their subdomains.
  * a single string will be understood as an array of a single element
* `contextToCustomHeaders`: `Record<contextKeyString, httpHeaderNameString>`, default: `{}`
  * Mapping of any custom context keys to http-headers in addition to the default correlation headers.
* `baseContextValues`: `Record<contextKeyString, httpHeaderNameString>`.
  * default: 
    ```
    {
      correlationId: 'x-correlation-id',
      flowName: 'x-flow-name',
      flowInitiator: 'x-flow-initiator',      
    },
    ```
* `contextToHeaders`: `Record<contextKeyString, httpHeaderNameString>`.
  * this computed internally as `{ ...baseContextValues, ...contextToCustomHeaders }`.
  * you may provide an empy object or an explicit `null` if you wish to override this behavior alltogether, letting the component set for you only the default timeout and user-agent header.
