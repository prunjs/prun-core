import { join, sep } from 'node:path';
import { readFileSync } from 'node:fs';

const { version: httpGotVersion } = JSON.parse(readFileSync(join(import.meta.dirname, '../package.json')));

export default async function factory(
  {
    defaultTimeout,
    internalDomains,
    baseStaticHeaders,
    contextHeaders,
    gotClient,
    gotInfo,
  },
  {
    logger,
    context,
    process,
    config: { pkg: { name, version } },
  },
) {
  const { default: got } = await import(gotClient);
  const log = logger.of('http-got');
  log.info('initializing');

  if (gotInfo === 'skip') gotInfo = '';
  else gotInfo = `@prun/http-got/v${httpGotVersion}; ${gotInfo.name}/v${gotInfo.version};`;

  const userAgent = `${name}/v${version}; nodejs/${process.version}; ${gotInfo}`;

  log.debug({ userAgent }, 'composed user agent');

  return got.extend({
    timeout: defaultTimeout,
    hooks: {
      beforeRequest: [
        options => {
          const { hostname } = options.url;

          if (baseStaticHeaders) Object.assign(options.headers, baseStaticHeaders);

          if (
            contextHeaders
            && internalDomains.find(internalDomain => hostname.endsWith(internalDomain))
          ) {
            Object.assign(options.headers, {
              ...context.populateValuesFromContextKeys(contextHeaders),
              ...options.headers,
            });
          }

          options.headers['user-agent'] = userAgent;

          log.trace({ headers: options.headers }, 'beforeRequest hook - headers augmented');
        },
      ],
    },
  });
};

//eslint-disable-next-line complexity
export function validate({
  defaultTimeout = 2000, // 2 seconds
  gotClient = 'got',
  gotInfo,
  internalDomains = ['.local'],

  baseStaticHeaders = {},
  baseContextHeaders = {
    'x-correlation-id': 'correlationId',
    'x-flow-name': 'flowName',
    'x-flow-initiator': 'flowInitiator',
  },
  customContextHeaders = {},
  contextHeaders = { ...baseContextHeaders, ...customContextHeaders },
}, {
  errors: { configError },
}) {
  //normalize
  try {
    gotClient = import.meta.resolve(gotClient);
  } catch (innerError) {
    throw configError('could not resolve got client module', {
      description: [
        '@prun/http-got is expected to be a peer dependency of the `got` client',
        'variant you provide in `options.gotClient (default: got)`, however,',
        'its place on disk cannot be resolved.',
        ' - make sure the named module is installed in your project and is',
        '   available on the import path',
        ' - if you provided a custom variant - check the name is correct',
      ],
      providedGotClient: gotClient,
      innerError,
    });
  }

  if (!gotInfo) gotInfo = loadGotInfo(gotClient, configError);
  if ('string' == typeof internalDomains) internalDomains = [internalDomains];
  if ('number' == typeof defaultTimeout) defaultTimeout = { request: defaultTimeout };

  //validate
  const isNotAValidHeadersMapper = (name, obj) =>
    (
      'object' != typeof obj
      || Array.isArray(obj)
      || Object.values(obj).find(v => 'string' != typeof v)
    ) && `options.${name} should be a mapping of a string key on the context to a string http-header name`;

  const reject =
    (
      'object' != typeof defaultTimeout
      || 'number' == typeof defaultTimeout.request && defaultTimeout.request < 0
    ) && 'options.defaultTimeout should be either a number as ms for request timeout, or a got timeout options'
    || !(Array.isArray(internalDomains)
        && !internalDomains.find(s => 'string' != typeof s)
    ) && 'options.internalDomains is not an array of internal domain strings'
    || isNotAValidHeadersMapper('baseContextHeaders', baseContextHeaders)
    || isNotAValidHeadersMapper('customContextHeaders', customContextHeaders)
    || isNotAValidHeadersMapper('contextHeaders', contextHeaders)
    || !('skip' === gotInfo
          || 'string' === typeof gotInfo.name
            && 'string' === typeof gotInfo.version

    ) && 'when provided, options.gotInfo may be either the string "skip",'
         + ' or an object with `name` and `version` string properties';
  if (reject) {
    throw configError(reject, {
      description: [
        'invalid options for @prun/http-got',
        'supported options:',
        '- defaultTimeout: optional number,  timeout in miliseconds, default to 2000',
        '- internalDomains: optional string[], list of all the internal domains that',
        '    calling them should apply correlation headers, default to ["internal"].',
        '- baseContextHeaders: optional object, mapping of keys on cls context to names',
        '    of http headers. Both keys and values should be strings. This comes with',
        '    a few useful defaults you can cascade by the next option',
        '- customContextHeaders: optional object, mapping of keys on cls context to',
        '    names of http headers. Both keys and values should be strings.',
        '- contextHeaders: it is a merge of the previous two where the later cascades',
        '    however, you can override it completely if you so wish.',
        '- gotClient: the got client variant to use, as it is expected to be resolved',
        '    by the import mechanism',
        '- gotInfo: either the string `"skip"`, ',
        '    or an object with string props: name, version',
        'Note:',
        ' * when contextHeaders is provided, then both baseContextHeaders and',
        '    customContextHeaders are ignored.',
      ],
    });
  }

  return {
    defaultTimeout,
    internalDomains,
    baseStaticHeaders,
    contextHeaders,
    gotClient,
    gotInfo,
  };
};

function loadGotInfo(gotClient, configError) {
  const gotPkgPath = gotClient.slice(6).replaceAll(sep, '/')
    .replace(/\/node_modules\/([^/]+)\/.*$/, (_, n) => `/node_modules/${n}/package.json`);

  try {
    const { name, version } = JSON.parse(readFileSync(gotPkgPath));
    return { name, version };
  } catch (innerError) {
    throw configError('could not detect name and version for your got client', {
      description: [
        'The package.json of the got-client cannot be read.',
        'the provided name is resolved successfully, however',
        ' - its package.json could be read or parsed',
        'you can override this mechanism by providing:',
        '  options:',
        '    gotInfo:',
        '      name: <the name to appear in user-agent header>',
        '      version: <the version to appear in user-agent header>',
        'you can disable this behavior by providing:',
        '  options:',
        '    gotInfo: skip',
      ],
      gotClient,
      gotPkgPath,
      innerError,
    });
  }
}

export const env = {
  internalDomains: {
    name: 'PRUN_HTTP_INTERNAL_DOMAIN',
    parser: v => v.split(',').map(v => v.trim()),
  },
  defaultTimeout: {
    name: 'PRUN_HTTP_DEFAULT_TIMEOUT',
    parse: true,
  },
  baseHeaders: {
    name: 'PRUN_HTTP_BASE_HEADERS',
    parse: true,
  },
};
