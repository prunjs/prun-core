import { promisify } from 'node:util';

import { claim, iocFactory } from '@prun/bdd-helper';
import got from 'got';
import mockWebServer from 'mock-web-server';

import SUT, { validate as normalizeOptions } from '../lib/index.js';

//TRICKY: back to mocha because of how node:test displays rejections :(
//import { describe, before, it, after, afterEach } from 'node:test';
//Object.assign(global, { describe, it, before, after });
//TRICKYEnd

describe('http-got.default', () => {
  const externalSvr = mockWebServer({
    headers: { 'content-type': 'application/json' },
    body: { external: true },
  });
  externalSvr.url = 'http://127.0.0.1:3000/foo';

  const internalSvr = mockWebServer({
    headers: { 'content-type': 'application/json' },
    body: { internal: true },
  });
  internalSvr.url = 'http://localhost:4000/bar';

  before(() => Promise.all([
    promisify(externalSvr.listen)(3000),
    promisify(internalSvr.listen)(4000),
  ]));

  after(() => Promise.all([
    promisify(externalSvr.close)(),
    promisify(internalSvr.close)(),
  ]));

  const ioc = iocFactory({
    config: {
      pkg: { name: 'foo', version: '1.0.0' },
    },
  });

  describe('when called with', () => {
    const expectedGotApi = {
      result: {
        'should be a got client': result => {
          claim(result).isFunction(2);
          Should(result).have.properties(Object.keys(got));
        },
      },
    };
    describe('options that is the bare minimum (an empty object), and a valid IoC bag', () => {
      const ctx = claim(SUT).behavior({
        options: normalizeOptions({}, ioc),
        ioc,
        expect: expectedGotApi,
      });

      describe('when using the initiated client to perform a request', () => {
        before(async () => {
          externalSvr.reset();
          internalSvr.reset();
          const got = ctx.result;

          ctx.response = await got(externalSvr.url);
        });

        it('should not fail', () => Should(ctx).have.property('response'));

        describe('the request user-agent header', () => {
          it('should include the project name and version', () => {
            Should(externalSvr.accepted[0].headers).have.property('user-agent').match(/foo\/v1.0.0/);
          });
          it('should include node-runtime version', () => {
            Should(externalSvr.accepted[0].headers).have.property('user-agent').match(/nodejs\/v\d+\.\d+\.\d+/);
          });
          it('should include @prun/http-got version info', () => {
            Should(externalSvr.accepted[0].headers)
              .have.property('user-agent').match(/@prun\/http-got\/v\d+\.\d+\.\d+/);
          });
          it('should include the got library version info', () => {
            Should(externalSvr.accepted[0].headers).have.property('user-agent').match(/got\/v\d+\.\d+\.\d+/);
          });
        });
      });
    });

    describe('options with `gotInfo: skip`, and a valid IoC bag', () => {
      const ctx = claim(SUT).behavior({
        options: normalizeOptions({ gotInfo: 'skip' }, ioc),
        ioc,
        expect: expectedGotApi,
      });

      describe('when using the initiated client to perform a request', () => {
        before(async () => {
          ctx.response = null;
          internalSvr.reset();

          const got = ctx.result;

          ctx.response = await got(internalSvr.url);
        });

        describe('the request user-agent header', () => {
          it('should include the project name and version', () => {
            Should(externalSvr.accepted[0].headers).have.property('user-agent').match(/foo\/v1.0.0/);
          });
          it('should include node-runtime version', () => {
            Should(externalSvr.accepted[0].headers).have.property('user-agent').match(/nodejs\/v\d+\.\d+\.\d+/);
          });
          it('should NOT include @prun/http-got version info', () => {
            Should(externalSvr.accepted[0].headers).have.property('user-agent').is.String();

            const userAgent = internalSvr.accepted[0].headers['user-agent'].split('; ');
            Should.not.exist(userAgent.find(s => s.startsWith('@prun/http-got')));
          });
          it('should NOT include the got clietn info', () => {
            Should(internalSvr.accepted[0].headers).have.property('user-agent').is.String();

            const userAgent = internalSvr.accepted[0].headers['user-agent'].split('; ');
            Should.not.exist(userAgent.find(s => s.startsWith('got')));
          });
        });
      });
    });

    describe('options with an `internalDomains: [...domain strings]`', () => {
      const ctx = claim(SUT).behavior({
        options: normalizeOptions({
          internalDomains: ['localhost'],
        }, ioc),
        ioc,
        expect: expectedGotApi,
      });

      describe('when using the initiated client to perform a request', () => {
        describe('on an internal service', () => {
          before(async () => {
            externalSvr.reset();
            internalSvr.reset();

            const got = ctx.result;

            ctx.response = null;

            ctx.ioc.context.correlationId = `some-random-id${Math.random()}`;
            ctx.ioc.context.flowName = 'testing';
            ctx.ioc.context.flowInitiator = 'mocha';

            ctx.response = await got(internalSvr.url);
          });

          it('should not fail', () => Should(ctx).have.property('response'));

          describe('the request headers', () => {
            const headerAccepted = (name, value) => {
              Should(internalSvr.accepted[0].headers)
                .have.property(name)[value instanceof RegExp ? 'match' : 'eql'](value);
            };

            it('should include x-correlation-id with the value of ioc.context.correlationId', () => {
              headerAccepted('x-correlation-id', ioc.context.correlationId);
            });

            it('should include x-flow-name with the value of ioc.context.flowName', () => {
              headerAccepted('x-flow-name', ioc.context.flowName);
            });

            it('should include x-flow-initiator with the value of ioc.context.flowInitiator', () => {
              headerAccepted('x-flow-initiator', ioc.context.flowInitiator);
            });
          });
        });

        describe('on an external service', () => {
          before(async () => {
            externalSvr.reset();
            internalSvr.reset();

            const got = ctx.result;

            ctx.response = null;

            ctx.ioc.context.correlationId = `some-random-id${Math.random()}`;
            ctx.ioc.context.flowName = 'testing';
            ctx.ioc.context.flowInitiator = 'mocha';

            ctx.response = await got(externalSvr.url);
          });

          it('should not fail', () => Should(ctx).have.property('response'));

          describe('the request headers', () => {
            const headerNotAccepted = name => {
              Should(externalSvr.accepted[0].headers).not.have.property(name);
            };

            it('should NOT include x-correlation-id', () => {
              headerNotAccepted('x-correlation-id');
            });

            it('should NOT include x-flow-name', () => {
              headerNotAccepted('x-flow-name');
            });

            it('should NOT include x-flow-initiator', () => {
              headerNotAccepted('x-flow-initiator');
            });
          });
        });
      });
    });
  });
});
