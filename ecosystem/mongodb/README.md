## prun-mongodb

An prun component for managing MongoDB connection

The initiated client and DB connections should be a `MongoClient` and `Db`, after validating session.

### Options

- **uri** - required - string -  A MongoDB connection URI
- **dbName** - required - string - the database being used
- **connectTimeout** - optional - number - default=1000 - timeout for connection checking, must be a positive number
- **dbOptions** - optional - object - Database Options
- **dbOptions** - optional - object - Client Options

### Resolves:

The following object, `Db` and `MongoClient` are taken from the mongodb dependency
`{ db: Db, client: MongoClient }`
