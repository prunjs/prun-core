export const errors = ({ errors: { PrunError } }) => {
  return [
    class PrunMongoConnectError extends PrunError {},
  ];
};
