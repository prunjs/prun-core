import globalMongoDb from 'mongodb';
export { errors } from './errors.js';

/**
 * @typedef {import('mongodb').MongoClient} MongoClient
 */

/** @returns {Promise<{ client: MongoClient }>} */
export default async function factory({
  uri,
  clientOptions,
  connectTimeout,
}, {
  errors: { configError },
  bus,
  logger,
  mongodb: { MongoClient } = globalMongoDb,
}) {
  const log = logger.of('mongodb');
  log.info('initializing');
  const client = new MongoClient(uri, { serverSelectionTimeoutMS: connectTimeout, ...clientOptions });

  log.info('connecting');
  try {
    await client.connect();
  } catch (err) {
    throw configError('could not connect to database', {
      description: [
        'This error is thrown while establishing a MongoDB connection.',
        'It is thrown when:',
        ' - the DB rejects the configured credentials',
      ],
      uri: maskCredentials(uri),
      connectTimeout,
      cause: err,
    });
  }

  log.info('Connected successfully');
  bus.on('shutdown', async () => {
    log.info('shutdown - closing connection');
    await client.close();
  });

  return { client };
}

export function validate(options, {
  errors: { configError },
}) {
  const { uri, clientOptions = {} } = options;
  const connectTimeout = typeof options.connectTimeout === 'undefined'
    ? 1000
    : Number(options.connectTimeout);

  const isObject = val => typeof val === 'object' && !Array.isArray(val);
  const isValidNumber = val => typeof val == 'number' && !Number.isNaN(val) && val > 0;

  const reject =
    uriRejectMessage(uri)
    || !isObject(clientOptions) && 'options.clientOptions must be an object'
    || (!isValidNumber(connectTimeout) || connectTimeout <= 0) && 'options.connectionTimeout must be a positive number';

  if (reject) {
    throw configError('Invalid MongoDB Options', {
      description: [
        'this is a configuration error.',
        'this error is thrown when invalid options are provided to the `@prun/mongodb` module',
        'valid options are:',
        ' - url - a mongodb connection string',
        ' - clientOptions - optional - options for client connection',
        ' - connectionTimeout - optional - default 1000 - timeout for database connection',
      ],
      reject,
      options: { ...options, uri: maskCredentials(options.uri) },
    });
  }
  return {
    uri,
    clientOptions,
    connectTimeout,
  };
}

function uriRejectMessage(uri) {
  if (typeof uri !== 'string' || uri.length === 0) {
    return 'options.uri must be a connection string';
  }
  if (!uri.startsWith('mongodb:') && !uri.startsWith('mongodb+srv/')) {
    return 'options.uri must start with Mongo protocol (mongodb | mongodb+srv)';
  }
  return null;
}

function maskCredentials(uri) {
  if (typeof uri !== 'string' || !uri.includes('@')) {
    return uri;
  }
  const [credentials, ...remaining] = uri.split('@');
  const [protocol] = credentials.split(':');
  const masked = `${protocol}://*****`;

  return [masked, ...remaining].join('@');
}

export const env = {
  uri: ['MONGODB_URI'],
};
