/**
 * @typedef {import('mongodb').MongoClient} MongoClient
 */

/**
 * @param {{ collection: string }} options
 * @param {{ logger: any, mongodb: { client: MongoClient }}}
 * @returns
 */
export default function factory({ collection, name }, { bus, logger, mongodb: { client } }) {
  const log = logger.of('cmd');
  const db = client.db('database');
  return {
    run:async () => {
      log.info({ name }, 'inserting to mongodb');
      await db.collection(collection).insertOne({ name });

      const collectionData = await db.collection(collection).find();
      const myCollection = await collectionData.toArray();
      log.info({ myCollection }, 'Retrieved my collection');

      const nameDocument = myCollection.find(document => document.name === name);
      if (nameDocument) {
        log.info({ nameDocument }, 'found name in collection');
      }
      bus.emit('shutdown');
    },
  };
}
