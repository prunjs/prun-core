/**
 * @typedef {import('mongodb').Db} Db
 * @typedef {import('mongodb').MongoClient} MongoClient
 */

/**
 * @param {{ collection: string }} options
 * @param {{ logger: any, mongodb: { db: Db, client: MongoClient }}}
 * @returns
 */
export default function factory(_opt, { logger }) {
  const log = logger.of('cmd');
  return {
    run() {
      log.info('should not reach here');
    },
  };
}
