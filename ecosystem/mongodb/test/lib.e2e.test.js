import { promisify } from 'node:util';
import { exec as syncExec } from 'node:child_process';

import { GenericContainer } from 'testcontainers';
import { MongoDBContainer } from '@testcontainers/mongodb';
import { randomBytes } from 'node:crypto';

const exec = promisify(syncExec);

describe('end to end', () => {
  const dbUsername = 'user';
  const dbPassword = 'password';
  const MONGO_PORT = 27017;
  let testContainer;
  let unavailableTestContainer;

  before(async function beforeSetup() {
    // Tricky: This is what it takes in the builder
    this.timeout('30s'); // eslint-disable-line no-invalid-this

    [testContainer, unavailableTestContainer] = await Promise.all([
      await new GenericContainer('mongo:4.0.1')
        .withExposedPorts(27017)
        .withEnvironment({ MONGO_INITDB_ROOT_USERNAME:dbUsername })
        .withEnvironment({ MONGO_INITDB_ROOT_PASSWORD:dbPassword })
        .start(),
      new MongoDBContainer().start(), // Tricky: Would not connect without directConnect
    ]);
  });
  describe('when running a valid application', () => {
    const ctx = {};
    let name;
    before(async function before() {
      this.timeout('10s'); // eslint-disable-line no-invalid-this
      try {
        name = randomBytes(5).toString('hex');
        const { stdout, stderr } = await exec(
          `npx prun --logger.module debug \
            --mongodb.uri=${getConnectionString()} \
            --cmd.name=${name} \
            --config test/fx/app/.prunrc`,
        );
        Object.assign(ctx, { stdout, stderr });
      } catch (err) {
        Object.assign(ctx, { err });
      }
    });

    it(
      'should not end with an error',
      () => Should.not.exist(ctx.err),
    );

    describe('logged output', () => {
      it(
        'should log mongo connected',
        () => Should(ctx.stdout).match(/Connected successfully/),
      );

      it(
        'should insert data',
        () => Should(ctx.stdout).match(/inserting to mongodb/),
      );

      describe('the data inserted', () => {
        it(
          'should have name inserted',
          () => Should(ctx.stdout).match(new RegExp(name)),
        );

        it(
          'should find name in collection',
          () => Should(ctx.stdout).match(/found name in collection/),
        );

        describe('when quitting gracefully', () => {
          it(
            'should close the connection with Mongodb',
            () => Should(ctx.stdout).match(/- closing connection/),
          );
        });
      });
    });
  });

  describe('when trying to invalid server', () => {
    const ctx = {};
    let name;
    let startTime;
    let endTime;

    before(async function before() {
      this.timeout('20s'); // eslint-disable-line no-invalid-this
      try {
        name = randomBytes(5).toString('hex');
        startTime = Date.now();
        const { stdout, stderr } = await exec(
          `npx prun --logger.module debug \
                    --mongodb.uri=${unavailableTestContainer.getConnectionString()} \
                    --cmd.name=${name} \
                    --mongodb.connectTimeout=50 \
                    --config test/fx/failing-app/.prunrc`,
        );
        endTime = Date.now();
        Object.assign(ctx, { stdout, stderr });
      } catch (err) {
        endTime = Date.now();
        Object.assign(ctx, { err });
      }
    });

    it('should fail', () => Should.exist(ctx.err));
    it('should fast fail without hanging', () => {
      const executionTime = endTime - startTime;
      console.log(`Connection attempt took ${executionTime}ms`);
      Should(executionTime).be.lessThan(2500); // Tricky: Takes longer in CI
    });
    describe('logged output', () => {
      it(
        'should specify it connection issue',
        () => Should(ctx.err.stdout).match(/could not connect/),
      );
    });
  });

  describe('when trying with invalid credentials', () => {
    const ctx = {};
    let name;

    before(async function before() {
      this.timeout('10s'); // eslint-disable-line no-invalid-this
      try {
        name = randomBytes(5).toString('hex');
        const { stdout, stderr } = await exec(
          `npx prun --logger.module debug \
                    --mongodb.uri=${getConnectionString('badPassword')} \
                    --cmd.name=${name} \
                    --config test/fx/app/.prunrc`,
        );
        Object.assign(ctx, { stdout, stderr });
      } catch (err) {
        Object.assign(ctx, { err });
      }
    });

    it('should fail', () => Should.exist(ctx.err));
    describe('logged output', () => {
      it(
        'should specify the cause',
        () => Should(ctx.err.stdout).match(/could not connect/),
      );

      it(
        'should print authentication failed at the message',
        () => Should(ctx.err.stdout).match(/Authentication failed/),
      );
    });
  });

  after(() =>
    Promise.all([
      testContainer.stop(),
      unavailableTestContainer.stop(),
    ]));

  function getConnectionString(password = dbPassword) {
    return `mongodb://${dbUsername}:${password}@${testContainer.getHost()}:${testContainer.getMappedPort(MONGO_PORT)}`;
  }
});
