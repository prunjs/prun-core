import { claim, iocFactory } from '@prun/bdd-helper';
import { MongoClient } from 'mongodb';
import sinon from 'sinon';

import * as SUT from '../lib/index.js';

describe('@prun/mongodb.factory', () => {
  describe('exported factory', () => {
    claim(SUT).isPrunFactory({
      hasValidator: true,
      mapsEnv: ['uri'],
    });

    describe('when called', () => {
      const ioc = iocFactory();

      describe('and initiation goes well', () => {
        let sandbox;
        claim(SUT, 'default').behavior({
          ioc,
          options: {
            uri: 'mongodb://localhost:1234',
            dbName: 'DbName',
          },
          before: () => {
            sandbox = sinon.createSandbox();
            sandbox.stub(MongoClient.prototype, 'connect').resolves();
            sandbox.stub(MongoClient.prototype, 'close').resolves();
          },
          after: () => sandbox.restore(),
          expect: {
            result: {
              'property .client should be an instance of MongoClient':
                result => Should(result.client).be.instanceOf(MongoClient),
            },
          },
        });

        describe('and shutdown signal is sent', () => {
          before(() => {
            ioc.bus.emit('shutdown');
          });

          it('should quit gracefully', () => Should(MongoClient.prototype.close.called).eql(true));
        });
      });

      describe('and there is an error in the connection', () => {
        let sandbox;
        claim(SUT, 'default').behavior({
          ioc,
          options: {
            uri: 'mongodb://localhost:1234',
            dbName: 'DbName',
          },
          before: () => {
            sandbox = sinon.createSandbox();
            sandbox.stub(MongoClient.prototype, 'connect').rejects(new Error('Oops'));
            sandbox.stub(MongoClient.prototype, 'close').resolves();
          },
          after: () => sandbox.restore(),
          expect: { reject: /could not connect/,
            describe: ctx => {
              describe('thrown error', () => {
                it(
                  'should have property cause with the underlying error',
                  () => Should(ctx.err).have.property('cause').have.property('message').match(/Oops/),
                );
              });
            } },
        });
      });
    });
  });
});
