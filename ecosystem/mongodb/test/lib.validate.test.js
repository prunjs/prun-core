
import { claim, iocFactory } from '@prun/bdd-helper';


import { validate as SUT } from '../lib/index.js';

describe('@prun/mongodb.validate', () => {
  it('should be a factory function that expects an options object and ioc bag', () => {
    claim(SUT).isFunction(2);
  });
  const validUri = 'mongodb://username:password@dummy.com';
  const validDbName = 'DbName';
  const ioc = iocFactory();

  describe('when calling', () => {
    describe('with valid', () => {
      describe('options.uri', () => {
        claim(SUT).behavior({
          options: {
            uri: validUri,
            dbName: validDbName,
          },
          ioc,
          expect: {
            result: {
              'should not modify the URI': result => Should(result.uri).be.eql(validUri),
            },
          },
        });

        describe('options.connectTimeout', () => {
          describe('when provided with a number in a string', () => {
            claim(SUT).behavior({
              options: {
                uri: validUri,
                dbName: validDbName,
                connectTimeout: '444',
              },
              ioc,
              expect: {
                result: {
                  'should convert type to number ': result => Should(typeof result.connectTimeout).be.eql('number'),
                  'should not change the value': result => Should(result.connectTimeout).be.eql(444),
                },
              },
            });
          });
        });
      });
    });
    describe('with missing', () => {
      describe('options.uri', () => {
        claim(SUT).behavior({
          options: {
            dbName: validDbName,
          },
          ioc,
          expect: {
            reject: /PrunConfigError: Invalid MongoDB Options/,
          },
        });
      });

      describe('options.connectTimeout', () => {
        claim(SUT).behavior({
          options: {
            uri: validUri,
            dbName: validDbName,
          },
          ioc,
          expect: {
            result: {
              'should return positive number': result => Should(result.connectTimeout).be.greaterThan(0),
            },
          },
        });
      });

      describe('options.clientOptions', () => {
        claim(SUT).behavior({
          options: {
            uri: validUri,
            dbName: validDbName,
          },
          ioc,
          expect: {
            result: {
              'should return an object':
                  result => Should(typeof result.clientOptions).be.eql('object'),
              'should be empty':
                  result => Should(result.clientOptions).be.empty(),
            },
          },
        });
      });
    });

    describe('with invalid', () => {
      describe('options.uri', () => {
        describe('that does not use a mongo protocol', () => {
          claim(SUT).behavior({
            options: {
              uri: 'https://username:passwrod@bad-example.com',
              dbName: validDbName,
            },
            ioc,
            expect: {
              reject: /PrunConfigError: Invalid MongoDB Options/,
              describe: ctx => {
                describe('the output', () => {
                  it(
                    'should mask the credentials',
                    () => Should(ctx.err).have.property('options').have.property('uri').not.match(/password/),
                  );
                });
              },
            },
          });

          describe('that is not a string', () => {
            claim(SUT).behavior({
              options: {
                uri: 134,
                dbName: validDbName,
              },
              ioc,
              expect: {
                reject: /PrunConfigError: Invalid MongoDB Options/,
              },
            });
          });
        });
      });

      describe('options.connectTimeout', () => {
        describe('that is not a number', () => {
          claim(SUT).behavior({
            options: {
              uri: validUri,
              dbName: validDbName,
              connectTimeout: 'Not a number',
            },
            ioc,
            expect: {
              reject: /PrunConfigError: Invalid MongoDB Options/,
              describe: ctx => {
                describe('the output', () => {
                  it(
                    'should mask the credentials',
                    () => Should(ctx.err).have.property('options').have.property('uri').not.match(/password/),
                  );
                });
              },
            },
          });
        });

        describe('that is a NaN', () => {
          claim(SUT).behavior({
            options: {
              uri: validUri,
              dbName: validDbName,
              connectTimeout: NaN,
            },
            ioc,
            expect: {
              reject: /PrunConfigError: Invalid MongoDB Options/,
              describe: ctx => {
                describe('the output', () => {
                  it(
                    'should mask the credentials',
                    () => Should(ctx.err).have.property('options').have.property('uri').not.match(/password/),
                  );
                });
              },
            },
          });
        });

        describe('that is not a positive', () => {
          claim(SUT).behavior({
            options: {
              uri: validUri,
              dbName: validDbName,
              connectTimeout: 0,
            },
            ioc,
            expect: {
              reject: /PrunConfigError: Invalid MongoDB Options/,
              describe: ctx => {
                describe('the output', () => {
                  it(
                    'should mask the credentials',
                    () => Should(ctx.err).have.property('options').have.property('uri').not.match(/password/),
                  );
                });
              },
            },
          });
        });
      });

      describe('options.clientOptions', () => {
        describe('that is not an object', () => {
          claim(SUT).behavior({
            options: {
              uri: validUri,
              dbName: validDbName,
              clientOptions: [],
            },
            ioc,
            expect: {
              reject: /PrunConfigError: Invalid MongoDB Options/,
              describe: ctx => {
                describe('the output', () => {
                  it(
                    'should mask the credentials',
                    () => Should(ctx.err).have.property('options').have.property('uri').not.match(/password/),
                  );
                });
              },
            },
          });
        });
      });
    });
  });
});
