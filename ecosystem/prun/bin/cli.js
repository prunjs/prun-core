#!/usr/bin/env node
import { inspect } from 'node:util';

import core from '../lib/core/index.js';
import usrApp from '../lib/usr-app/index.js';

(async () => {
  // launch core;
  const ioc = { process, logger: console };
  try {
    await core(ioc);
  } catch (e) {
    const { log } = console;
    log('FATAL:', e.toJSON());
    hardExit(ioc, 'core');
    return;
  }

  ioc.logger.debug('core ready');

  if (ioc.config.debugConfig) {
    ioc.bus.emit('shutdown');
    const sep = '---------'.split('').join('--------');
    setTimeout(() => console.log(
      'DEBUG CONFIG - start\n%s\n%s\n%s\nDEBUG CONFIG - End',
      sep,
      inspect(ioc.config, { colors: true, depth: 99, sorted: true, compact: 1 }),
      sep,
    ), 100);
    return;
  }

  // load and run user layers in a cls context
  ioc.context.watch(async () => {
    process.on('uncaughtException', fatalAt('Error'));
    process.on('unhandledRejection', fatalAt('Rejection'));

    const sig = fatalAt('SIGNAL');
    process.on('SIGINT', sig);
    process.on('SIGTERM', sig);

    try {
      await usrApp(ioc);
    } catch (e) {
      fatalAt('usrApp')(e);
    }
  });

  // "privates"

  function fatalAt(at) {
    return cause => {
    /* istanbul ignore else */
      if (cause.toJSON) cause = cause.toJSON();

      ioc.logger.fatal({ at, cause }, 'shutting down');
      ioc.bus.emit('shutdown');

      hardExit(ioc, at);
    };
  }

  function hardExit({ process, hrtime, logger, config = {} }, at) {
    const ms = hrtime();
    const ackExit = () => logger.info({ at, inMs: ms() }, 'exited gracefully');
    const { shutdownGraceTimeout: graceTimeout = 3000 } = config;
    process.on('exit', ackExit);
    process.exitCode = 'SIGNAL' === at ? 0 : 1;

    setTimeout(
      () => {
        process.removeListener('exit', ackExit);
        logger.warn({ at, graceTimeout }, 'abrupt exit: graceTimeout expired');
        process.exit(2); //eslint-disable-line no-process-exit
      },
      graceTimeout,
    )
      .unref();
  }
})();
