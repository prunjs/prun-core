import { EventEmitter } from 'node:events';
import assert from 'node:assert';

export default () => {
  //TRICKY: we're not really using OOP `this` and object state.
  // However - we do want to communicate that it's not a vanilla EventEmitter
  //   in the debugger-inspector and in call stack trace.
  const asyncHooks = {};
  class PrunAsyncEventHandler extends EventEmitter {
    asyncHook = asyncHook;
    dropAsyncHook = dropAsyncHook;
    fireAsyncHook = fireAsyncHook;
  };
  const bus = new PrunAsyncEventHandler();

  return bus;

  function asyncHook(type, handler) {
    assert.equal(typeof type, 'string', '.asyncHook(type) must be a string');
    assert.equal(typeof handler, 'function', '.asyncHook(..,handler) must be a function');

    asyncHooks[type]
      ? asyncHooks[type].push(handler)
      : asyncHooks[type] = [handler];
  }

  function dropAsyncHook(type, handler) {
    assert.equal(typeof type, 'string', '.dropAsyncHook(type) must be a string');
    assert.equal(typeof handler, 'function', '.dropAsyncHook(..,handler) must be a function');

    const hooks = asyncHooks[type];
    if (!hooks) return false;

    const ix = hooks.indexOf(handler);
    hooks.splice(ix, 1);
    if (!hooks.length) Reflect.deleteProperty(asyncHooks, type);
    return true;
  }

  async function fireAsyncHook(type, ...args) {
    assert.equal(typeof type, 'string', '.fireHook(type) must be a string');

    const handlers = asyncHooks[type] || [];

    const results = await Promise.all(handlers.map(h => h(...args)));

    bus.emit(`${type}-resolved`, { args, handlers, results });

    return handlers.length;
  }
};
