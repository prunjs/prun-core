export default {
  envType: 'local',
  lib: './lib',
  //hooks: 'hooks',
  logger: {
    module: 'pino',
    levels: {
      main: 'info',
      default: 'warn',
    },
  },
  shutdownGraceTimeout: 5000,
  env: {
    env: { name: 'NODE_ENV', default: 'development' },
    envType: { name: ['ENVIRONMENT_TYPE', 'ENV_TYPE'], default: 'local' },
    'logger.levels.default': ['LOG_LEVEL', 'LOGGER_LEVEL'],
    'logger.module': ['LOG_MODULE', 'LOGGER_MODULE'],
    shutdownGraceTimeout: { name: 'SHUTDOWN_GRACE_TIMEOUT', parse: true },
  },
};
