import nodeFs from 'node:fs';
import path from 'node:path';

import globalDotenv from 'dotenv';
import globalJsYaml from 'js-yaml';
import globalMinimist from 'minimist';
import globalRc from 'rc';
import lodash from 'lodash';

import bakedInDefaults from './config.defaults.js';

const { set: setVal } = lodash;

coreConfigFactory.bakedInDefaults = bakedInDefaults;

export default coreConfigFactory;

function coreConfigFactory({
  process: { env, argv, cwd: pwd, chdir, stdout = {} },
  //injectables
  rc        = globalRc,
  minimist  = globalMinimist,
  yaml      = globalJsYaml,
  defaults  = bakedInDefaults,
}) {
  const cliOptions = minimist(argv.slice(2), { boolean: ['logger.disablePritty'] });
  cliOptions.cliArgs = cliOptions._;
  const config = rc('prun', defaults, cliOptions, yaml.load);
  config.cwd = resolveCwd(pwd(), config);
  chdir(config.cwd);
  config.pkg = pkgInfo(config.cwd);
  loadDotEnv(env);
  envToConfig(env, config, config.env);
  config.isTTY = stdout.isTTY;
  Reflect.defineProperty(config, 'envVars', {
    configurable: true,
    enumerable: false,
    writable: false,
    value: env,
  });
  return config;
}

export const resolveCwd = (pwd, { config, cwd }) => {
  return config // - i.e  the cli arg --config, or `config` key in .prunrc
    ? path.resolve(path.dirname(config))
    : cwd || pwd;
};

export const pkgInfo = (cwd, { fs = nodeFs } = {}) => {
  const pkgText = fs.readFileSync(path.join(cwd, 'package.json'));// eslint-disable-line no-sync
  const { name, version } = JSON.parse(pkgText);
  return { name, version };
};

export const loadDotEnv = (env, { fs = nodeFs, dotenv = globalDotenv } = {}) => {
  let text;
  try {
    text = fs.readFileSync('./.env');// eslint-disable-line no-sync
  } catch (err) {
    //istanbul ignore else
    if ('ENOENT' === err.code) return;
    //istanbul ignore next
    throw err; //can still fall on perms...
  }

  Object.assign(env, dotenv.parse(text));
};

export const envToConfig = (env, config, envDirectives) => {
  Object.entries(envDirectives).forEach(([path, options]) => {
    if ('string' == typeof options || Array.isArray(options)) options = { name: options, parse: false };
    const { parse, parser: appParser, name } = options;
    if (appParser && 'function' != typeof appParser) {
      //TBD: use PrunComponentError
      throw Object.assign(
        new Error('export env.<option>.parser is not a function'),
        {
          description: [
            'when provided, env.<option>.parser should be a parser function',
            'that accepts the string from the env and parses it into a value',
          ],
          configPath: path,
          options,
        },
      );
    }

    const variable =
            Array.isArray(name)
              ? name.find(variable => env[variable])
              : name;

    let v = env[variable];
    if (!v) return;

    if (parse || appParser) {
      const parser = appParser || JSON.parse;
      try {
        v = parser(v);
      } catch (e) {
        //TBD: use PrunConfigError
        throw Object.assign(
          new Error('Could not parse envitonment variable'),
          {
            description: [
              'Environment variables are marked for parsing when an .env key is defined as:',
              '  env:',
              '    path.to.config.key: { name: YOUR_ENV_VAR_NAME, parse:true }',
              'in this case, the variable will be parsed as JSON - which also works to',
              'convert plain numbers and booleans to JS values.',
            ],
            configPath: path,
            options,
            variable,
            value: v,
            reason: e.message,
          },
        );
      }
    }

    setVal(config, path, v);
  });
};
