import cls from 'cls-hooked';
import { v4 } from 'uuid';

const undef = undefined; //eslint-disable-line no-undefined

export default ({
  config: { clsNsId = v4() } = {},
  cls: { createNamespace } = cls,
}, {
  logMeta = {
    correlationId: true,
    flowName: true,
    flowInitiator: true,
  },
} = {}) => {
  const ns = createNamespace(clsNsId);
  const watch = f => ns.run(() => {
    ns.set('logMeta', {});
    f();
  });

  const mapKeys = contextToHeaders => Object
    .entries(contextToHeaders)
    .reduce((map, [mappedName, contextKey]) => {
      const value = contextGet(contextKey);
      if (value !== undef && value !== null) map[mappedName] = value;
      return map;
    }, {});

  const contextGet = k => ns.active ? ns.get(k) : undef;

  return new Proxy({}, {
    get: (t, k) => {
      switch (k) {
      case 'watch': return watch;
      case 'populateValuesFromContextKeys': return mapKeys;
      case 'current': return ns.active ? ns : undef;
      default: return contextGet(k);
      }
    },
    set: (t, k, v) => {
      if (!ns.active) return undef;
      gatherRequiredMeta(logMeta[k], k, v);
      ns.set(k, v);
      return v;
    },
  });

  function gatherRequiredMeta(attr, k, v) {
    if (!attr) return;
    ns.get('logMeta')[attr === true ? k : attr] = v;
  }
};
