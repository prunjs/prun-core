const { assign } = Object;

export class PrunError extends Error {
  constructor(message, meta) {
    if (
      message
            && 'object' == typeof message
            && !Array.isArray(message)
    ) [message = '<no message provided>', meta] = [message.message, message];
    if (Array.isArray(message)) message = message.join('\n');
    super(message);
    this.name = this.constructor.name;
    assign(this, meta);
  }
  reject() {
    return Promise.reject(this); //eslint-disable-line prefer-promise-reject-errors
  }
}

export class PrunConfigError extends PrunError {}
export class PrunComponentError extends PrunError {}
export class PrunArgumentError extends PrunError {}
export class NotImplementedError extends PrunError {}

const errors = {
  PrunError,
  PrunConfigError,
  PrunArgumentError,
  PrunComponentError,
  NotImplementedError,
};

export default errors;
