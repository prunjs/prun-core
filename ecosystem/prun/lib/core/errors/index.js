import errorStringify from 'error-stringify';

import errorClasses from './error-classes.js';

const { assign } = Object;
const { PrunError, PrunConfigError } = errorClasses;

errorStringify({ splitStackTrace: true, allowOverrideToJSON: true });

export default assign(errorsFactory, errorClasses);

function errorsFactory() {
  const errors = { register };

  //load built-ins
  register(Object.values(errorClasses));

  return errors;

  function register(ErrorClass, { override, name = ErrorClass.name } = {}) {
    if (Array.isArray(ErrorClass)) {
      ErrorClass.forEach(ErrorClass => register(ErrorClass, { override }));
      return;
    }

    decendFromPrunError(ErrorClass);
    validClassName(name);
    unregisteredOrExplicitOverride(name, override);

    const factoryProperty = name.replace(
      /^(Prun)?(.)(.*)$/,
            (m, prefix, first, rest) => `${first.toLowerCase()}${rest}`, //eslint-disable-line
    );
    const rejectorProperty = factoryProperty.replace(/(Error)?$/, 'Reject');

    errors[name] = ErrorClass;
    errors[factoryProperty] = getNamedFactory(ErrorClass);
    errors[rejectorProperty] = getNamedRejector(ErrorClass);
  }

  // -- privates to the closure

  function getNamedFactory(ErrorClass) {
    //TRICKY: force the .name of the function
    const name = `factory<${ErrorClass.name}>`;
    const o = { [name]: (...args) => new ErrorClass(...args) };
    return o[name];
  }

  function getNamedRejector(ErrorClass) {
    //TRICKY: force the .name of the function
    const name = `rejector<${ErrorClass.name}>`;
    const o = { [name]: (...args) => new ErrorClass(...args).reject() };
    return o[name];
  }

  function decendFromPrunError(ErrorClass) {
    const isSubclass = ErrorClass === PrunError || ErrorClass.prototype instanceof PrunError;
    if (!isSubclass) {
      throw new PrunConfigError(
        '.register(ErrorClass) is expected to be provided with PrunError or PrunError subclass',
      );
    }
  }

  function validClassName(name) {
    if (!name.match(/^[A-Z]/) //starts with capital
           || !name.endsWith('Error')
    ) {
      throw new PrunConfigError(
        'name of registered error class must begin with a capital English letter.',
        { type: name },
      );
    }
  }

  function unregisteredOrExplicitOverride(name, override) {
    if (errors[name] && !override) {
      throw new PrunConfigError(
        [
          'error type is already declared.',
          'To explicitly override - provide { override: true } as 2nd argument',
        ],
        { type: name },
      );
    }
  }
}
