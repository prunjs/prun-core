export default ({ process: { hrtime } }) => {
  return () => {
    const start = hrtime.bigint();
    return () => Number(hrtime.bigint() - start) / 1e6;
  };
};
