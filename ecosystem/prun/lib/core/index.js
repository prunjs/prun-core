import busFactctory from './bus.js';
import configFactory from './config.js';
import contextFactory from './context.js';
import errorsFactory from './errors/index.js';
import hrtimeFactory from './hrtime.js';
import loggerFactory from './logger/index.js';

export default async ioc => {
  ioc.errors  = errorsFactory();
  ioc.hrtime  = hrtimeFactory(ioc);
  ioc.bus     = busFactctory(ioc);
  ioc.config  = configFactory(ioc);
  ioc.context = contextFactory(ioc);
  ioc.logger  = await loggerFactory(ioc);
  return ioc;
};
