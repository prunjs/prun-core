import { inspect } from 'node:util';

import globalDebug from 'debug';

export default ({
  levels,
}, {
  debug = globalDebug,
  config: { pkg, isTTY },
}) => {
  const {
    default: defaultLevel,
    main: mainLevel = 'info',
  } = levels;
  const lvls = 'fatal,error,warn,info,debug,trace'.split(',');
  pkg = { ...pkg, from: pkg.name };
  Reflect.deleteProperty(pkg, 'name');
  return logger(pkg, mainLevel);

  function logger(ctx, lvl) {
    const { from } = ctx;
    Reflect.deleteProperty(ctx, 'from');
    const emit = debug(`${from}`);
    emit.enabled = true;
    emit.log = (...a) => console.log(...a);
    const level = lvl || levels[from] || defaultLevel;
    const iLevel = lvls.indexOf(level);
    const noop = () => {}; //eslint-disable-line no-empty-function
    const inspectOpts = { colors: isTTY, depth: 10 };

    return lvls.reduce((api, level, ix) => Object.assign(api, {
      [level] : iLevel < ix
        ? noop
        : (...a) => {
          let meta = { ...ctx };
          while (a[0] && 'object' == typeof a[0]) meta = { ...meta, ...a.shift() };
          emit(level.toUpperCase(), ...a, inspect(meta, inspectOpts).replace(/\n/g, '\n  '));
        },
    }), {
      of: (from, meta) => child({ ...meta, from }),
      isLevelEnabled(level) { return this[level] !== noop; },
      child,
    });

    function child(meta) {
      return logger({ ...ctx, from, ...meta });
    }
  }
};
