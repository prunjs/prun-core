import { dirname, resolve } from 'node:path';
import { fileURLToPath } from 'node:url';
import { inspect } from 'node:util';

export default async ioc => {
  const { config: {
    pkg: { name },
    logger: logCfg,
  } } = ioc;

  logCfg.levels[name] = logCfg.levels.main;

  const loggerModule = resolveModule(logCfg.module);
  const rootLoggerFactory = await import(loggerModule);
  const rootLogger = await rootLoggerFactory.default(logCfg, ioc);

  rootLogger.info('starting up');
  rootLogger.debug(
    'loaded config',
    inspect({ config: ioc.config }, { depth: 99, colors: true }),
  );

  return rootLogger;

  function resolveModule(loggerModule) {
    //a backed in default logger
    const bakedInLoggers = ['debug', 'pino'];
    if (bakedInLoggers.includes(loggerModule)) {
      const fullPath = fileURLToPath(import.meta.url);
      const dir = dirname(fullPath);
      return `file://${dir}/${loggerModule}.js`;
    }

    //a project file determined by .prunrc relative to cwd
    if ('.' === loggerModule.charAt(0)) return `file://${resolve(ioc.config.cwd, ioc.config.lib, loggerModule)}`;

    //a package from node_modules
    return loggerModule;
  }
};
