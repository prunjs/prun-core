import globalPino from 'pino';

export default async ({
  levels,
  disablePritty,
  devTransport = 'pino-prettier',
  prodTransport = null, //TRICKY: defaults to simple JSON
}, {
  config: {
    pkg: { name, version },
  },
  pino = globalPino,
}) => {
  const rootLogger = pino({
    name,
    level: levels.main,
    transport: await getTransport(),
  }).child({ version });

  Object.assign(Reflect.getPrototypeOf(rootLogger), {
    of(channel, meta) {
      const level = levels[channel] || levels.default;
      const child = this.child({ caller: channel, ...meta });
      child.level = level;
      return child;
    },
  });

  return rootLogger;

  async function getTransport() {
    if (disablePritty) return null;

    try {
      await import(devTransport);
      return { target: devTransport };
    } catch (e) {
      //istanbul ignore next
      if (!['MODULE_NOT_FOUND', 'ERR_MODULE_NOT_FOUND'].includes(e.code)) throw e;

      return prodTransport ? { target: prodTransport } : null;
    }
  }
};
