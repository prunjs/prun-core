/* eslint sort-imports: 'off' */
import { appLoader, isLayersStructureValid } from './loader.js';
import appValidator from './validator.js';
import appInit from './initializer.js';
import { appRunner, isRunConfigSectionValid } from './runner.js';

export default async ioc => {
  const {
    errors,
    config,
    bus,
    logger,

    loader = appLoader,
    validator = appValidator,
    init   = appInit,
    runner = appRunner,
  } = ioc;

  const log = logger.of('core');

  log.debug('validating core config');
  isLayersStructureValid(config, errors, bus);
  isRunConfigSectionValid(config, errors, bus); //valid run config serction

  log.debug('loading factories');
  await loader(config.layers, ioc);

  log.debug('validating project config');
  validator(config.layers, ioc); //modules & project custom validations

  log.debug('initializing iocs');
  await init(config.layers, ioc); //fires async-hook: 'initialized'

  log.info('running');
  return runner(config.run, ioc);
};
