export default async (layers, ioc) => {
  const { logger, hrtime, bus } = ioc;
  const log = logger.of('initializer');

  const layerNames = Object.keys(layers).reverse();

  for (const layerName of layerNames) {
    await initLayer(layerName, layers[layerName]); //eslint-disable-line no-await-in-loop
  }

  const hooksCount = await bus.fireAsyncHook('initialized', ioc);
  log.debug({ keys: Object.keys(ioc), hooksCount }, 'ioc initiated');

  return ioc;

  async function initLayer(layerName, modules) {
    const ms = hrtime();
    const layerCtx = { layerName };
    const layerLog = log.child(layerCtx);
    layerLog.debug('initiating layer');

    await Promise.all(
      Object.entries(modules)
        .map(async ([iocName, modCfg], ix) => {
          const {
            module: { default: factory },
            modulePath,
            options,
          } = modCfg;

          const modCtx = { layerName, modulePath, iocName, moduleIndex: ix };
          const modLog = layerLog.child(modCtx);

          modLog.debug({ options, factory }, 'initiating module');
          const ms = hrtime();
          const initiated = await factory(options, { ...ioc });
          modLog.debug({ inMs: ms(), addToIoc: Boolean(initiated) }, 'module initiated');

          if (initiated) ioc[iocName] = modCtx.module = initiated;

          bus.emit('module-initiated', modCtx);
          bus.emit(`module-initiated:${iocName}`, modCtx);
        }),
    );

    layerLog.debug({ inMs: ms() }, 'layer initiated');
    bus.emit('layer-initiated', layerCtx);
    bus.emit(`layer-initiated:${layerName}`, layerCtx);
  }
};
