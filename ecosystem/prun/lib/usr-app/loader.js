import { existsSync } from 'node:fs';
import path from 'node:path';

import lodash from 'lodash';

import { envToConfig } from '../core/config.js';

export const appLoader = async (layers, ioc) => {
  const { defaultsDeep } = lodash;
  const { logger, config, bus, errors: { configError } } = ioc;
  const log = logger.of('loader');

  const layersEntries = Object.entries(layers);
  const layersCount = layersEntries.length;

  // sync version with require was:
  // Object.entries(layers)
  //   .reverse()
  //   .forEach(loadLayer);

  while (layersEntries.length) {
    const index = layersCount - layersEntries.length;
    //TRICKY: sacrifice some load-time performance for a clearer log
    await loadLayer(layersEntries.pop(), index); //eslint-disable-line no-await-in-loop
    //TRICKYEnd
  }

  log.debug({ keys: Object.keys(ioc) }, 'loaded ioc');
  bus.emit('loaded', ioc);

  return ioc;

  async function loadLayer([layerName, modules], ix) {
    const layerCtx = { layerName, layerIndex: ix };
    log.debug(layerCtx, 'loading layer ');

    await Promise.all(Object.entries(modules).map(async ([iocName, modCfg], ix) => {
      const modulePath = modCfg.modulePath = modCfg.module;
      const modCtx = { ...layerCtx, iocName, moduleIndex: ix, modulePath };
      const modLog = log.child(modCtx);

      modLog.debug(modCtx, 'loading module');

      const module = await loadModule(modulePath, { config, log }, modCtx);
      if (!module) {
        throw configError(
          'Cannot find module',
          {
            code: 'MODULE_NOT_FOUND',
            ...modCtx,
            cwd: config.cwd,
            providedPath: modulePath,
          },
        );
      }

      modCtx.module = modCfg.module = module;

      const customEnv = module.env || module.default.env;
      const customErrorsFactory = module.errors || module.default.errors;

      if (customErrorsFactory) {
        loadCustomErrors(modCtx, customErrorsFactory, ioc);
        modLog.debug({ errors: Object.keys(ioc.errors) }, 'custom errors factory found');
      }

      if (customEnv) {
        const { pullEnvOptions = true } = modCfg;
        if (pullEnvOptions) {
          modLog.debug({ customEnv }, 'custom env directives found');
          loadCustomEnv(iocName, customEnv, ioc);
        } else {
          modLog.debug({ customEnv, pullEnvOptions }, 'custom env directives will be skipped for this instance');
        }
      }

      modCfg.options = defaultsDeep({ iocName }, config[iocName], modCfg.options);

      log.debug({ modCtx, modCfg }, 'module loaded');
      bus.emit('module-loaded', modCtx);
      bus.emit(`module-loaded:${iocName}`, modCtx);
    }));

    log.debug(layerCtx, 'layer loaded');
    bus.emit('layer-loaded', layerCtx);
    bus.emit(`layer-loaded:${layerName}`, layerCtx);
  }
};

export const isLayersStructureValid = (config, { PrunConfigError }) => {
  const isObj = v => 'object' == typeof v;
  const isStr = v => 'string' == typeof v;
  const { layers } = config;
  let layer, iocName;

  if (!isObj(layers)) throw new PrunConfigError('config.layers must be an object');

  //$layer/$svc/options - must be mergable
  //$layer/$svc/module - must be string (requireable?)
  Object.entries(layers).forEach(([name, cfg]) => validLayer(name, cfg));

  return config;

  function validLayer(name, cfgLayer) {
    layer = name;
    Object.entries(cfgLayer).forEach(
      ([iocName, iocCfg]) => { cfgLayer[iocName] = validModule(iocName, iocCfg); },
    );

    return cfgLayer;
  }

  function validModule(name, iocCfg) {
    iocName = name;

    if (isStr(iocCfg)) iocCfg = { module: iocCfg, options: {} };

    if (!isStr(iocCfg.module)) {
      throw new PrunConfigError('iocName without a module.', {
        description: [
          'values of entries in a layer should be either a path string,',
          'or - an object with:',
          ' - options - object',
          ' - module - path string',
          'when the value is a string - it is understood as the module path,',
          'with an empty options object.',
        ],
        layer,
        iocName,
      });
    }

    const { [iocName] : rootOptions } = config;
    if (rootOptions && !isObj(rootOptions)) {
      throw new PrunConfigError('Cannot merge options from config root for iocName.', {
        description: [
          'When config root contains a key of same name as an iocName - it is expected to',
          'be rootOptions - cascading options object for that iocName',
        ],
        layer,
        iocName,
        rootOptions,
      });
    }

    iocCfg.options = iocCfg.options
      ? Object.assign(iocCfg.options, rootOptions)
      : rootOptions;

    return iocCfg;
  }
};

export const loadCustomEnv = (iocName, customEnv, { config }) => {
  const envMapping = Object.entries(customEnv).reduce((envMapping, [k, v]) => {
    envMapping[`${iocName}.${k}`] = v;
    return envMapping;
  }, {});

  envToConfig(config.envVars, config, envMapping);
};

export const loadCustomErrors = (modCtx, customErrorsFactory, ioc) => {
  const { errors: { register, PrunError, configError } } = ioc;

  if ('function' != typeof customErrorsFactory) {
    throw configError('Invalid Custom Errors Factory', {
      description: [
        'this is a developer error',
        'this error is thrown in load time when an prun-component defines',
        'an `.errors` property which is not a function.',
      ],
      ...modCtx,
      foundAttributeType: typeof customErrorsFactory,
    });
  }

  const customErrors = customErrorsFactory(ioc);
  if (!customErrors) return;

  if (!Array.isArray(customErrors) || customErrors.some(e => !(e && e.prototype instanceof PrunError))) {
    throw configError('Invalid Custom Errors returned', {
      description: [
        'this is a developer error',
        'this error is thrown in load time when an prun-component defines',
        'an errors factory that returns a value which is not an Array of PrunError decendents.',
        'all values in the array must be a subclass of PrunError.',
      ],
      ...modCtx,
      returned: customErrors,
    });
  }

  register(customErrors);
};

export const loadModule = (providedPath, { config, log }, modCtx) => {
  const resolvedAs = normalizePath(providedPath, config);
  if (!resolvedAs) return null;

  log.trace({ providedPath, resolvedAs }, 'resolved module path');

  return import(resolvedAs)
    .catch(err => Promise.reject(Object.assign(err, { ...modCtx, cwd: config.cwd, providedPath, resolvedAs })));
};

function normalizePath(providedPath, { lib, cwd }) {
  if (providedPath.startsWith('.')) {
    //project modules - relative to `lib`
    const found = [
      `${lib}/${providedPath}.js`,
      `${lib}/${providedPath}.mjs`,
      `${lib}/${providedPath}`,
    ].find(actualPath => existsSync(path.resolve(cwd, actualPath)));

    if (found) return `file://${path.resolve(cwd, found)}`;
  } else {
    //packages - resolve from FS
    const relative = ['node_modules'];
    let dirCount = cwd.slice(0).split(path.sep).length;

    while (dirCount--) {
      const dir = relative.join('/');
      const found = [
        `${dir}/${providedPath}.js`,
        `${dir}/${providedPath}.mjs`,
        `${dir}/${providedPath}`,
      ].find(actualPath => existsSync(path.resolve(cwd, actualPath)));

      if (found) return providedPath;

      relative.unshift('..');
    }
  }

  return null;
}
