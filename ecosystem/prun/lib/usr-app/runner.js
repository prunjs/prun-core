export const appRunner = (run, ioc) => {
  const log = ioc.logger.of('runner');
  const runnerNames = Object.keys(run);

  return Promise.all(runnerNames.map(async runner => {
    const { module: iocName, method, params = [] } = run[runner];
    log.debug({ iocName, method, params }, 'invoking');
    const result = await (
      method
        ? ioc[iocName][method](...params)
        : ioc[iocName](...params)
    );

    log.info({ iocName, method, params, result }, 'running result');
    return result;
  }));
};

export const isRunConfigSectionValid = (config, { configError }, bus) => {
  const { run, layers } = config;

  Object.entries(run).forEach(([iocName, options]) => {
    //normalize
    if ('string' == typeof options) {
      run[iocName] = options = { module: options, method: 'run' };
    }

    const { module, method, params } = options;

    //assert
    const isIocFound = () => Object
      .values(layers)
      .some(modules => modules[module]);
    const reject =
            'string' != typeof module && 'Invalid runner: ioc name in property `module` is not a string'
            || !isIocFound() && 'Invalid runner: module is not found in ioc';

    if (reject) {
      throw configError(reject, {
        description: [
          'this is a developer error.',
          'this error is thrown when the ioc name for a runner is not a string',
          'or is not found in any of the layers.',
          'note: when a runner is configured just with an ioc name, it is understood as:',
          ' { module: iocName, method: "run" }',
        ],
        iocName,
        options,
      });
    }

    if (method) {
      bus.on(`module-initiated:${module}`, ({ layer, module, modulePath }) => {
        if ('function' == typeof module[method]) return;

        throw configError('Invalid runner: method not found on module', {
          description: [
            'this is a developer error.',
            'this error is thrown when the method configured for a runner is not found',
            'on the ioc it expects.',
            'note: when a runner is configured just with an ioc name, it is understood as:',
            ' { module: iocName, method: "run" }',
          ],
          iocName,
          method,
          layer,
          modulePath,
        });
      });
    }

    if (params && !Array.isArray(params)) {
      throw configError('Invalid runner: params is not an array', {
        description: [
          'this is a developer error.',
          'this error is thrown when the params for the runner method is not an array',
        ],
        iocName,
        params,
      });
    }
  });

  return config;
};
