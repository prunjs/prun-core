export default (layers, ioc) => {
  const { logger, bus } = ioc;
  const log = logger.of('validator');
  const validatedModules = { ...ioc };

  const layerNames = Object.keys(layers).reverse();

  for (const layerName of layerNames) {
    Object.assign(validatedModules, validateLayer(layerName, layers[layerName], validatedModules));
  }

  log.debug({ layerNames }, 'layers validated');
  return ioc;

  function validateLayer(layerName, modules, validatedModules) {
    const layerCtx = { layerName };
    const layerLog = log.child(layerCtx);
    const layerValidatedModules = {};
    layerLog.debug('validating layer modules');

    Object.entries(modules)
      .forEach(([iocName, modCfg], moduleIndex) => {
        const {
          module: { default: { validate: defaultValidate }, validate = defaultValidate },
          options,
        } = modCfg;

        const modLog = layerLog.child({ iocName, moduleIndex });

        if ('function' == typeof validate) {
          modLog.debug('running validation hook');
          try {
            const normalized = validate(options, validatedModules);
            if (normalized) modCfg.options = normalized;
          } catch (err) {
            Object.assign(err, { layerName, iocName });
            throw err;
          }
        } else {
          modLog.debug('no validation hook');
        }

        layerValidatedModules[iocName] = true;
        modLog.info('module validated');
      });

    layerLog.debug('layer validated');
    bus.emit('layer-validated', layerCtx);
    bus.emit(`layer-validated:${layerName}`, layerCtx);
    return layerValidatedModules;
  }
};
