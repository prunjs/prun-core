/* eslint max-lines: 'off' */
import { inspect, promisify } from 'node:util';
import { exec as execRaw } from 'node:child_process';

import e2eLogFactory from '../util/e2e-log.js';

const exec = promisify(execRaw);

//HACK
const mode = 'run';
//-no-hack  //TBD - that's from mocha. what's vitests?
// const mode = process.argv[2] === '--reporter' ? 'cover' : 'run';
//HACKEnd

const e2eLog = e2eLogFactory(`./cli-e2e.${mode}.log`);

const expectedHelloBanner = `
-----------------------------
      hello   world !
-----------------------------`;

describe('prun e2e', () => {
  before(e2eLog.setup);
  after(e2eLog.teardown);

  describe('when ran with a cli project', () => {
    describe('and provided a bad config file path that does not point to a project', () => {
      testCase({
        fx: 'no-such-fixture',
        expect: {
          'should exit with code 1': ctx => shouldExitWith(1, ctx),
          'should exit gracefully': ctx => Should(ctx.stdout).match(/exited gracefully/),
        },
      });
    });

    describe('and one of project module paths is misconfigured', () => {
      testCase({
        fx: 'thrown-errors',
        args: [
          '--layers.interface.hang ./no/such/module',
        ],
        expect: {
          'should exit with code 1': ctx => {
            shouldExitWith(1, ctx);
          },
          'should emit error with message:Cannot find module': ctx => {
            Should(ctx.stdout).match(/Cannot find module/);
          },
          'should augment error with context': ctx => {
            Should(ctx.stdout)
              .match(/layerName:(\s+|\n)'interface'/)
              .match(/iocName:(\s+|\n)'hang'/)
              .match(/cwd:/)
              .match(/providedPath:(\s+|\n)'.\/no\/such\/module'/);
          },
          'should exit gracefully': ctx => {
            Should(ctx.stdout).match(/exited gracefully/);
          },
        },
      });
    });

    describe('and root key `layers` is not an object ', () => {
      testCase({
        fx: 'thrown-errors',
        args: [
          '--layers not-an-object',
        ],
        expect: {
          'should exit with code 1': ctx => {
            shouldExitWith(1, ctx);
          },
          'should emit error with message:config.layers must be an object': ctx => {
            Should(ctx.stdout).match(/config.layers must be an object/);
          },
          'should exit gracefully': ctx => {
            Should(ctx.stdout).match(/exited gracefully/);
          },
        },
      });
    });

    describe('and root options for an ioc-name is not an object ', () => {
      testCase({
        fx: 'thrown-errors',
        args: [
          '--hang not-an-object',
        ],
        expect: {
          'should exit with code 1': ctx => {
            shouldExitWith(1, ctx);
          },
          'should emit error with message: PrunConfigError: Cannot merge options ... for iocName': ctx => {
            Should(ctx.stdout).match(/PrunConfigError: Cannot merge options from config root for iocName/);
          },
          'should exit gracefully': ctx => {
            Should(ctx.stdout).match(/exited gracefully/);
          },
        },
      });
    });

    describe('and one of project modules does not have module path', () => {
      testCase({
        fx: 'thrown-errors',
        args: [
          //TRICKY: this will result with a module with .options, but without .module
          '--layers.pathless.module.options.yes yes',
        ],
        expect: {
          'should exit with code 1': ctx => {
            shouldExitWith(1, ctx);
          },
          'should emit error with message: PrunConfigError: iocName without a module': ctx => {
            Should(ctx.stdout).match(/PrunConfigError: iocName without a module/);
          },
          'should exit gracefully': ctx => {
            Should(ctx.stdout).match(/exited gracefully/);
          },
        },
      });
    });

    describe('and provided --debugConfig flag', () => {
      testCase({
        fx: 'thrown-errors',
        args: ['--debugConfig'],
        expect: {
          'should not fail': shouldNotFail,
          'should print the configs as it understands them': ctx => {
            Should(ctx.stdout).match(/DEBUG CONFIG - start\n[^]+DEBUG CONFIG - End/m);
          },
        },
      });
    });

    describe('and a parsable env-var does not parse well', () => {
      testCase({
        fx: 'calc-cli',
        env: { SHUTDOWN_GRACE_TIMEOUT: '5se00' },
        expect: {
          'should exit with code 1': ctx => shouldExitWith(1, ctx),
          'should emit a firendly error': ctx => {
            Should(ctx.stdout).match(/Could not parse envitonment variable/i);
          },
        },
      });
    });

    describe('and run throws an error during initiation without hanging', () => {
      testCase({
        fx: 'thrown-errors',
        expect: {
          'should exit with code 1': ctx => shouldExitWith(1, ctx),
          'the thrown error should be evident on stdout': ctx => {
            Should(ctx.stdout)
              .match(/status.+bad-future \(thrown manually\)/i)
              .match(/during.+initialization/);
          },
          'should exit gracefully': ctx => {
            Should(ctx.stdout).match(/INFO.+exited gracefully/);
          },
        },
      });
    });

    describe('and run throws an error during initiation and hangs', () => {
      testCase({
        fx: 'thrown-errors',
        args: [
          '--hang.at init',
        ],
        expect: {
          'should exit with code 2': ctx => shouldExitWith(2, ctx),
          'should warn about ungraceful shutdown': ctx => {
            Should(ctx.stdout).match(/warn.+graceTimeout expired/i);
          },
        },
      });
    });

    describe('and run throws an error after successful initiation (and process hangs)', () => {
      testCase({
        fx: 'thrown-errors',
        args: [
          '--hang.at cmd',
          '--hang.delayThrow 500',
        ],
        expect: {
          'should exit with code 2': ctx => shouldExitWith(2, ctx),
          'the thrown error should be evident on stdout': ctx => {
            Should(ctx.stdout)
              .match(/status.+bad-future \(factory-thrown\)/i)
              .match(/during.+timer/);
          },
          'should warn about ungraceful shutdown': ctx => {
            Should(ctx.stdout).match(/warn.+graceTimeout expired/i);
          },
        },
      });
    });

    describe('and run rejects a promise after successful initiation (and process hangs)', () => {
      testCase({
        fx: 'thrown-errors',
        args: [
          '--hang.at cmd',
          '--hang.reject',
          '--hang.delayThrow 500',
        ],
        expect: {
          'should exit with code 2': ctx => shouldExitWith(2, ctx),
          'the rejected error should be evident on stdout': ctx => {
            Should(ctx.stdout)
              .match(/status.+bad-future \(rejected\)/i)
              .match(/during.+timer/);
          },
          'should warn about ungraceful shutdown': ctx => {
            Should(ctx.stdout).match(/warn.+graceTimeout expired/i);
          },
        },
      });
    });

    describe('and run throws an error during module validation', () => {
      testCase({
        fx: 'throwing-validation',
        expect: {
          'should exit with code 1': ctx => shouldExitWith(1, ctx),
          'should throw error about missing modules': ctx => {
            ctx.stdout.should.match(/Error: Missing modules/);
          },
          'should warn about higher layers required modules': ctx => {
            Should(ctx.stdout).match(/calcA/);
          },
          'should not throw error about lower layer existing modules': ctx => {
            Should(ctx.stdout).not.match(/missingModules.+calcB/);
          },
        },
      });
    });

    describe('and runner iocName is not found', () => {
      testCase({
        fx: 'runners',
        args: ['--run.implied', 'noSuchIoc'],
        expect: {
          'should exit with code 1': ctx => shouldExitWith(1, ctx),
          'should emit a firendly error': ctx => {
            Should(ctx.stdout).match(/Invalid runner: module is not found in ioc/i);
          },
        },
      });
    });

    describe('and runner iocName is not a string', () => {
      testCase({
        fx: 'runners',
        args: ['--run.explicit.module.attr', 'notAString'], //module will be: { attr: 'notAString'}
        expect: {
          'should exit with code 1': ctx => shouldExitWith(1, ctx),
          'should emit a firendly error': ctx => {
            Should(ctx.stdout).match(/Invalid runner: ioc name.*is not a string/i);
          },
        },
      });
    });

    describe('and runner method is not found on the ioc module', () => {
      testCase({
        fx: 'runners',
        args: ['--run.explicit.method', 'noSuchMethod'],
        expect: {
          'should exit with code 1': ctx => shouldExitWith(1, ctx),
          'should emit a firendly error': ctx => {
            Should(ctx.stdout).match(/Invalid runner: method not found on module/i);
          },
        },
      });
    });

    describe('that provides more than one runner', () => {
      testCase({
        fx: 'runners',
        expect: {
          'should not fail': ctx => shouldNotFail(ctx),
          'should execute runners provided explicitly': ctx => {
            Should(ctx.stdout).containEql('track: \'home-yard\'');
          },
          'should execute runners provided implicitly': ctx => {
            Should(ctx.stdout).containEql('track: \'coast-to-coast\'');
          },
        },
      });
    });

    describe('that uses an .env file', () => {
      testCase({
        fx: 'uses-.env',
        expect: {
          'should not fail': ctx => shouldNotFail(ctx),
          'should load the vars into the config module': ctx => {
            Should(ctx.stdout).containEql('prunrc: \'ok\'');
            Should(ctx.stdout).containEql('inModule: \'found\'');
          },
        },
      });
    });

    describe('that uses a module with custom errors', () => {
      describe('and the project uses error-factory API', () => {
        testCase({
          fx: 'custom-errors',
          args: ['--mikelJordan.throw', 'true', '--coach.defend', 'SelfScoredError'],
          expect: {
            'should not fail': ctx => shouldNotFail(ctx),
            'should be able to catch using constructor name': ctx => {
              Should(ctx.stdout).containEql('This is the error I\'m expecting');
            },
          },
        });
      });
    });

    describe('that uses only project code - e2e/fixtures/hello-world-services-cli', () => {
      describe('a vanilla run', () => {
        testCase({
          fx: 'hello-world-services-cli',
          expect: {
            'should not fail': ctx => shouldNotFail(ctx),
            'should print the `hello world` banner': ctx => {
              Should(ctx.stdout).containEql(expectedHelloBanner);
            },
          },
        });
      });
      describe('passing --logger.levgels.default debug', () => {
        testCase({
          fx: 'hello-world-services-cli',
          args: ['--logger.levels.default debug'],
          expect: {
            'should not fail': ctx => shouldNotFail(ctx),
            'should print the `hello world` banner': ctx => {
              Should(ctx.stdout).containEql(expectedHelloBanner);
            },
            'should include lots of debug comments': ctx => {
              Should(ctx.stdout).containEql('DEBUG');
            },
          },
        });
      });
      describe('passing env var LOG_LEVEL=debug', () => {
        testCase({
          fx: 'hello-world-services-cli',
          env: { LOG_LEVEL: 'debug' },
          expect: {
            'should not fail': ctx => shouldNotFail(ctx),
            'should print the `hello world` banner': ctx => {
              Should(ctx.stdout).containEql(expectedHelloBanner);
            },
            'should include lots of debug comments': ctx => {
              Should(ctx.stdout).containEql('DEBUG');
            },
          },
        });
      });
    });

    describe('that uses an npm-package as a service - e2e/fixtures/calc-cli', () => {
      describe('and package is not found', () => {
        testCase({
          fx: 'calc-cli',
          args: ['--layers.BL.calc.module @no-such/package'],
          expect: {
            'should exit with code 1': ctx => shouldExitWith(1, ctx),
            'should emit a friendly error': ctx => {
              Should(ctx.stdout.trim()).match(/PrunConfigError[^]+cannot find module/mi);
            },
          },
        });
      });

      describe('and run succeeds', () => {
        testCase({
          fx: 'calc-cli',
          expect: {
            'should not fail': ctx => shouldNotFail(ctx),
            'should print the calculation results': ctx => {
              Should(ctx.stdout.trim()).containEql('calc results');
            },
          },
        });
      });
    });

    describe('that uses a logger as', () => {
      describe('the baked-in pino adapter - e2e/fixtures/calc-cli', () => {
        describe('and run succeeds', () => {
          testCase({
            fx: 'calc-cli',
            useLoggerDebug: false,
            args: [
              '--logger.module pino',
            ],
            expect: {
              'should not fail': ctx => shouldNotFail(ctx),
              'should print the calculation results': ctx => {
                Should(ctx.stdout.trim()).containEql('calc results');
              },
            },
          });
        });
      });

      describe('the baked-in pino adapter, raw-json - e2e/fixtures/calc-cli', () => {
        describe('and run succeeds', () => {
          testCase({
            fx: 'calc-cli',
            useLoggerDebug: false,
            args: [
              '--logger.module pino',
              '--logger.disablePritty true',
            ],
            expect: {
              'should not fail': ctx => shouldNotFail(ctx),
              'should print the calculation results': ctx => {
                Should(ctx.stdout.trim()).containEql('calc results');
              },
            },
          });
        });
      });

      describe('a custom module as logger - e2e/fixtures/calc-cli', () => {
        describe('and run succeeds', () => {
          testCase({
            fx: 'calc-cli',
            useLoggerDebug: false,
            args: [
              '--logger.module ./custom-logger.js',
            ],
            expect: {
              'should not fail': ctx => shouldNotFail(ctx),
              'should print the calculation results': ctx => {
                Should(ctx.stdout.trim()).containEql('calc results');
              },
            },
          });
        });
      });

      describe('the baked-in debug logger - e2e/fixtures/calc-cli', () => {
        describe('and run succeeds', () => {
          testCase({
            fx: 'calc-cli',
            useLoggerDebug: false,
            args: [
              '--logger.module debug',
            ],
            expect: {
              'should not fail': ctx => shouldNotFail(ctx),
              'should print the calculation results': ctx => {
                Should(ctx.stdout.trim()).containEql('calc results');
              },
            },
          });
        });
      });

      describe('passing LOGGER_MODULE env param', () => {
        describe('and run succeeds', () => {
          testCase({
            fx: 'calc-cli',
            useLoggerDebug: false,
            env: {
              LOGGER_MODULE: 'debug',
            },
            expect: {
              'should not fail': ctx => shouldNotFail(ctx),
              'should print the calculation results': ctx => {
                Should(ctx.stdout.trim()).containEql('calc results');
              },
            },
          });
        });
      });
    });

    describe('that depends on async hooks - e2e/fixtures/hookie', () => {
      describe('and run succeeds', () => {
        testCase({
          fx: 'hookie',
          useLoggerDebug: false,
          expect: {
            'should not fail': ctx => shouldNotFail(ctx),
            'should print the last status': ctx => {
              Should(ctx.stdout.trim()).containEql('last status');
            },
            'should include reports from all sailors': ctx => {
              ['Jack', 'Tom', 'Liz'].forEach(sailor => {
                Should(ctx.stdout.trim()).containEql(`from - ${sailor}`);
              });
            },
          },
        });
      });
    });
  });
});

function testCase({ skip, fx, env = {}, args = [], expect, useLoggerDebug = true }) {
  if (useLoggerDebug) args.push('--logger.module', 'debug');
  const cmd = `node bin/cli --config ./test/e2e/fixtures/${fx}/.prunrc ${args.join(' ')}`.trim();
  const ctx = {};

  const localIt = skip
    ? (t, fn) => it.skip(t, fn)
    : it;

  before(async () => {
    try {
      const { stdout, stderr } = await exec(cmd, {
        env: { ...process.env, ...env }, //eslint-disable-line no-process-env
      });
      Object.assign(ctx, { stderr, stdout });
    } catch (err) {
      ctx.stderr = err.stderr;
      ctx.stdout = err.stdout;
      ctx.err = err;
    }
  });

  after(function () { // eslint-disable-line func-names
    if (ctx.stdout) ctx.stdout = ctx.stdout.split('\n');
    if (ctx.stderr) ctx.stderr = ctx.stderr.split('\n');
    if (ctx.err) {
      ctx.err = {
        name: ctx.err.name,
        message: ctx.err.message,
        ...ctx.err,
        stack: ctx.err.stack?.split('\n'),
      };
    }

    const caseTitles = this.test.fullTitle().trim() // eslint-disable-line no-invalid-this
      .split(' > ')
      .slice(2, -1); //first 2 are same for all, last is the `it` part

    return e2eLog.write(
      caseTitles.join(' > '),
      {
        case: caseTitles,
        fx,
        env,
        cmd,
        'cli-args': args,
        expected: Object.keys(expect),
        found: ctx,
      },
    );
  });

  expect.stderr
    ? localIt(`should emit to stderr - ${expect.stderr}`)
    : localIt('should not emit to stderr', () => Should(ctx.stderr).eql(''));

  Object.entries(expect)
    .filter(([, test]) => 'function' == typeof test)
    .forEach(([title, test]) => {
      localIt(title, () => test(ctx));
    });
}

function shouldNotFail({ err }) {
  err && Should.fail(
    inspect(Object.assign(err.stack.split('\n'), err)).slice(1, -1),
    ' <expected no error> ',
    'command should not fail',
  );
}

function shouldExitWith(code, { err }) {
  Should(Object.assign(err.stack.split('\n'), err)).have.property('code', code);
}
