export default (options, { logger, calc }) => {
  const log = logger.of('cmd');
  log.isLevelEnabled('debug') && log.debug({ options }, 'loaded with options');

  return {
    compute,
  };

  function compute() {
    log.info('running command: compute');

    const args = [1, 2, 3, 4];

    log.info({
      args,
      result: calc.add(...args),
    }, 'calc results');
  }
};
