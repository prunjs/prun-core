export default ({ defend }, { mikelJordan, errors, logger }) => {
  return {
    run: defend
      ? () => {
        try {
          mikelJordan.play();
        } catch (err) {
          if (!(err instanceof errors[defend])) throw err;

          logger.info('This is the error I\'m expecting');
        }
        logger.info('allrighy then');
      }
      : () => {
        logger.info('im careless');
        mikelJordan.play();
      },
  };
};
