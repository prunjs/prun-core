export default ({ throw: throwErr, reject }, { errors: { noEneergyReject, selfScoredError } }) => {
  return {
    play: () => {
      if (reject) return noEneergyReject('Im tierd', { sorry: true });

      if (throwErr) throw selfScoredError('points', { points: 3 });

      return null;
    },
  };
};

export const errors = ({ errors: { PrunError } }) => [
  class NoEnergyError extends PrunError {
    constructor(...a) {
      super(...a);
      this.stack = 'energy 0%';
    }
  },
  class SelfScoredError extends PrunError {
    constructor(...a) {
      super(...a);
      this.stack = 'Scored at your own basket';
    }
  },
];
