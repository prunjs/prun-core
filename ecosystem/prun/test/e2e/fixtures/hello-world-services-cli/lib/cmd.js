export default (options, { logger, banner }) => {
  const log = logger.of('cmd');
  log.debug({ options }, 'loaded with options');

  //TRICKY: to be used with/without method:run - testing the runner
  greet.run = greet;
  return greet;

  function greet() {
    log.info('running command: greet');

    const greet = banner.generate();
    log.info(greet);
  }
};
