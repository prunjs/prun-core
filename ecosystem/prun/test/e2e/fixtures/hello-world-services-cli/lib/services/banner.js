import { format } from 'node:util';
export default (
  { template, greet: greetIocName = 'greet' }, //options.greeter - names the ioc for greeter
  { logger, [greetIocName]: greet, target },
) => {
  const log = logger.of('svc/banner');
  log.debug('initiated', { name: 'banner' });

  return {
    generate: () => {
      const greeting = greet.greet();
      const who = target.who();
      log.debug({ greeting, who }, 'formatting banner');
      return format(template, greeting, who);
    },
  };
};
