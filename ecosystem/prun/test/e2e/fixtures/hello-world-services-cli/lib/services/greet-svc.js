export default (options, { logger }) => {
  const log = logger.of('svc/greet-svc');
  log.debug('initiated', { options });

  return {
    greet(greeting = 'hello') {
      log.debug({ greeting }, 'sending greeting');
      return greeting;
    },
  };
};
