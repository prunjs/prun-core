export default (options, { logger }) => {
  const log = logger.of('svc/location');
  log.debug('initiated', { options });

  return {
    who(target = 'world') {
      log.debug({ target }, 'providing target');
      return target;
    },
  };
};
