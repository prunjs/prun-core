// Changing one of th ioc modules to make sure it's only being changed locally and not propagate to other modules.
export default (options, ioc) => {
  const log = ioc.logger.of('changing-buss');
  log.debug('loaded');
  ioc.bus = 'not-working-bus';

  log.debug({ bus: ioc.bus }, 'changed bus (I think so)');
};
