export default (
  { captainIoc = 'captain' },
  { logger, bus, [captainIoc]: captain },
) => {
  const log = logger.of('crowsNest');

  bus.asyncHook('initialized', () => log.info('horizon is clear'));
  bus.on('initialized-resolved', r => log.info({ resolved: Object.keys(r) }, 'resolved names'));

  return {
    run: () => {
      log.info({ status: captain.status() }, 'last status');
    },
  };
};
