export default (options, { bus }) => {
  const staff = [];
  let lastStatus = ['not init'];

  bus.asyncHook('initialized', headCount);

  return {
    ack: (name, sailor) => staff.push({ name, sailor }),
    status: () => lastStatus,
  };

  function headCount() {
    return Promise.all(
      staff.map(async ({ name, sailor }) => {
        const report = await sailor.report();
        return `I hear "${report}" from - ${name}`;
      }),
    ).then(reports => { lastStatus = reports; });
  }
};
