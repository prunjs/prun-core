export default ({ uplinkIoc = 'captain', name, deck }, { [uplinkIoc]: uplink, logger }) => {
  const log = logger.of('sailor');

  log.info('initialized');

  const sailor = {
    report: () => {
      return Promise.resolve().then(() => {
        log.info('reporting');
        return `Aye, at ${deck}`;
      });
    },
  };

  uplink.ack(name, sailor);

  return sailor;
};
