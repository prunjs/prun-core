export default ({ track }, { logger }) => {
  const log = logger.of(`forrest-gump:${track}`);
  log.info(':)');

  return {
    run: () => {
      logger.info({ track }, 'vooom :)');
    },
  };
};
