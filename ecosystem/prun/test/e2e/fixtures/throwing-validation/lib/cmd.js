export default (options, { logger }) => {
  const log = logger.of('cmd');
  log.isLevelEnabled('debug') && log.debug('loaded with options');

  return {
    shouldNotRun,
  };

  function shouldNotRun() {
    throw new Error('Should have already fail on validate');
  }
};
