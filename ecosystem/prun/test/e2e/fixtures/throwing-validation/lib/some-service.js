// eslint-disable-next-line no-empty-function
export default () => {};

const iocDeps = ['logger', 'calcA', 'calcB'];
export const validate = (options, ioc) => {
  const { errors } = ioc;

  const missingModules = iocDeps.reduce((accModules, iocDepName) => {
    if (!ioc[iocDepName]) {
      accModules.push(iocDepName);
    }

    return accModules;
  }, []);

  if (missingModules.length) {
    throw errors.configError('Missing modules', { missingModules });
  }
};
