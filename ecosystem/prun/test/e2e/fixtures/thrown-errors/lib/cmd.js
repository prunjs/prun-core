export default (options, {
  logger,
  //errors are defined in previously loaded modules (including core modules).
  //project can destruct from errors registry Error class, error factory or error rejector.
  // this gives basically 3 variants on how to use them
  errors: { PrunFutureError, futureError, futureReject },
}) => {
  const log = logger.of('throwing-init-cmd');
  if (log.isLevelEnabled('debug')) log.debug({ options }, 'loaded with options');

  if ('init' === options.at) hang();

  if (!options.delayThrow) {
    //Way 1: project can use Error constructor directly
    throw new PrunFutureError({ status: 'bad-future (thrown manually)', during: 'initialization' });
  }

  log.warn({ options }, 'delayed throw');
  setTimeout(
    () => {
      //Way 2: project can throw error using the factory helper
      //   saves you to use `new`, and has a shorter name
      if (!options.reject) throw futureError({ status: 'bad-future (factory-thrown)', during: 'timer' });

      return futureReject({ status:'bad-future (rejected)', during: 'timer' });
      //Way 3: project can use rejector method
    },
    options.delayThrow,
  );

  return {
    hang: () => {
      if ('cmd' === options.at) hang();
    },
  };

  function hang() {
    let tik = false;
    setInterval(() => {
      tik = !tik;
      log.info(tik ? 'tik' : 'tack');
    }, 1000);
  }
};
