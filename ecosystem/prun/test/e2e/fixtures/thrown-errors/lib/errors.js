export default (options, { errors: { register, PrunError } }) => {
  register(class PrunFutureError extends PrunError {
    constructor(meta) {
      super('This error came from the future', meta);
    }
  });
};
