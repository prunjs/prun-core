export default ({ prunrc, inModule }, { logger }) => {
  return {
    run: () => {
      logger.info({ prunrc, inModule });
    },
  };
};

export const env = {
  inModule:  ['ENV_PARAM'],
};
