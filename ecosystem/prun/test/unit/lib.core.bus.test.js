import { EventEmitter } from 'events';

import SUT from '../../lib/core/bus.js';

describe('lib/core/bus', () => {
  it('should be a factory function', () => {
    Should(SUT).be.a.Function();
  });
  describe('when called with ioc', () => {
    it('should return an extension of EventEmitter', () => {
      Should(SUT()).be.instanceOf(EventEmitter);
    });
    describe('the initiated bus', () => {
      describe('when firing an async hook with no registered listeners', () => {
        const ctx = {};
        before(() => {
          const bus = SUT();
          try {
            bus.fireAsyncHook('no-such-handlers', 123);
          } catch (e) {
            ctx.err = e;
          }
        });
        it('should not fail', () => {
          Should.not.exist(ctx.err);
        });
      });
      describe('when registering an asyncHook and the hook is fired', () => {
        const ctx = { sum: 0 };

        before(async () => {
          const bus = SUT();
          const addAsync = n => {
            ctx.sum += n;
            return Promise.resolve();
          };

          bus.asyncHook('foo', addAsync);

          try {
            ctx.fired1 = await bus.fireAsyncHook('foo', 123);
            ctx.snap1 = ctx.sum;

            ctx.fired2 = await bus.fireAsyncHook('foo', 123);
            ctx.snap2 = ctx.sum;
          } catch (e) {
            ctx.err = e;
          }
        });
        it('should not fail', () => {
          Should.not.exist(ctx.err);
        });
        it('should fire for all listeners', () => {
          Should(ctx.fired1)
            .eql(ctx.fired2)
            .eql(1);
        });
        it('should await the hook', () => {
          Should(ctx.snap1).eql(123);
          Should(ctx.snap2)
            .eql(ctx.sum)
            .eql(246);
        });
      });
      describe('when registering an asyncHook and dropping it later and the hook is fired', () => {
        const ctx = { sum: 0 };
        before(async () => {
          const bus = SUT();
          const addAsync = n => {
            ctx.sum += n;
            return Promise.resolve();
          };

          bus.asyncHook('foo', addAsync);

          try {
            ctx.fired1 = await bus.fireAsyncHook('foo', 123);
            ctx.snap1 = ctx.sum;

            bus.dropAsyncHook('foo', addAsync);

            ctx.fired2 = await bus.fireAsyncHook('foo', 123);
            ctx.snap2 = ctx.sum;
          } catch (e) {
            ctx.err = e;
          }
        });
        it('should not fail', () => Should.not.exist(ctx.err));
        it('should fire only for listeners that has not been dropped', () => {
          Should(ctx.fired1).eql(1);
          Should(ctx.fired2).eql(0);
        });
        it('should await only registered hooks', () => {
          Should(ctx.snap1)
            .eql(ctx.snap2)
            .eql(ctx.sum)
            .eql(123);
        });
      });
      describe('when dropping hook listeners', () => {
        const ctx = {};
        const f1 = async () => {}; //eslint-disable-line no-empty-function
        const f2 = async () => {}; //eslint-disable-line no-empty-function
        before(() => {
          const bus = SUT();
          bus.asyncHook('bar', f1);
          bus.asyncHook('bar', f2);

          ctx.f1rm1 = bus.dropAsyncHook('bar', f1);
          ctx.f2rm1 = bus.dropAsyncHook('bar', f2);

          ctx.f1rm2 = bus.dropAsyncHook('bar', f1);
          ctx.f2rm2 = bus.dropAsyncHook('bar', f2);
        });
        it('removing existing hook should return indicate handler removed by returning true', () => {
          Should(ctx.f1rm1).be.True();
          Should(ctx.f2rm1).be.True();
          Should(ctx.f1rm2).be.False();
          Should(ctx.f2rm2).be.False();
        });
      });
    });
  });
});
