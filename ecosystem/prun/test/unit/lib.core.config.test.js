import { createRequire } from 'node:module';
import { join } from 'node:path';

import SUT from '../../lib/core/config.js';
import defaults from '../../lib/core/config.defaults.js';


const require = createRequire(import.meta.url);
const { env, cwd } = process;
const chdir = path => { chdir.path = path; };

describe('lib/config', () => {
  it('should be a factory function that expects iocSeed', () => {
    Should(SUT).be.a.Function().property('length', 1);
  });

  useCaseTestFactory({
    title: 'when used with an ioc seed with a valid process with vanilla args',
    seedIoc: { process },
    expect: {
      cwd: process.cwd(),
      found: {
        'should contain the defaults':
                    found => Should(found).containEql(defaults),
        'should have`name` and `version` based on package.json of current work dir':
                    found => Should(found)
                      .have.property('pkg')
                      .eql({ name: '@prun/prun', version:  require('../../package').version }),
        'should have no layers?': found => Should(found).not.have.property('layers'),
      },
    },
  });
  useCaseTestFactory({
    title: 'when used with an ioc seed with a valid process with --config switch',
    seedIoc: { process: {
      env,
      cwd,
      chdir,
      argv: ['node', 'bin/cli', '--config', './test/e2e/fixtures/hello-world-services-cli/.prunrc'],
    } },
    expect: {
      cwd: `${process.cwd()}/test/e2e/fixtures/hello-world-services-cli`,
      found: {
        'should have`name` and `version` based on package.json of directory of config file':
            found => Should(found).have.property('pkg').eql({
              name: 'hello-world-services-cli', version: '1.0.0',
            }),
        'should include info from provided .prunrc': found => Should(found).have.property('layers'),
        'should change directory to the directory where .prunrc is found':
            () => Should(chdir.path).eql(join(process.cwd(), 'test/e2e/fixtures/hello-world-services-cli')),
      },
    },
  });
});

function useCaseTestFactory({
  title,
  seedIoc,
  expect: {
    cwd,
    found = {},
  },
}) {
  const ctx = {};
  describe(title, () => {
    before(() => {
      try {
        ctx.found = SUT(seedIoc);
      } catch (e) {
        ctx.err = e;
      }
    });

    it('should not fail', () => {
      ctx.err && Should.fail(`thrown: ${ctx.err.message}`, '<should not fail>', ctx.err.stack);
    });

    it('it should return a config object', () => {
      Should(ctx.found).be.an.Object();
    });

    describe('the resulting config.pkg', () => {
      it('should have `cwd` pointing to current work dir', () => {
        Should(ctx)
          .property('found')
          .property('cwd')
          .eql(join(cwd));
      });

      Object.keys(found).forEach(title => {
        const f = found[title];
        it(title, () => f(ctx.found));
      });
    });
  });
}
