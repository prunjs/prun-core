import { createWriteStream } from 'node:fs';
import jsYaml from 'js-yaml';

export default logPath => {
  const out = createWriteStream(logPath, { flags: 'w' });

  out.promise = method => chunk => new Promise((acc, rej) => out[method](chunk, err => err ? rej(err) : acc()));
  const writePromise = out.promise('write');

  const format = (
    (jsYaml, dumpOpts) =>
      (title, ctx) =>
        jsYaml.dump({ [title]: ctx }, dumpOpts)
  )(jsYaml, { lineWidth: 999 });

  let start;

  return {
    setup: () => {
      start = new Date();
      return writePromise(`start-time: ${start.toUTCString()}\n`);
    },
    write: (title, ctx) => {
      out.write('\n');
      return writePromise(format(title, ctx));
    },
    teardown: () => {
      const end = new Date();
      return out.promise('end')(`\nend-time: ${end.toUTCString()}\ndur: ${end.getTime() - start.getTime()}ms\n\n`);
    },
  };
};
