# prun-redis

An prun component for managing redis connection

The initiated redis-client is a `redis-async` client, where all APIs except `multi` do not expect a callback and return a promise.

# Usage:


```
run:
  main: cmd

layers:
  interface:
    cmd: ./cmd

  dal:
    redis: @prun/redis

redis:
    host:             redis-13252.c84.us-east-1-2.ec2.cloud.redislabs.com
    port:             13252
    password:         ...from.env
```
