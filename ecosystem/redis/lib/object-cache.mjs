import msLibrary from 'ms';

export default function factory({
  ttl,
  entity,
  keyFor = id => `${entity}:id:${id}`.toLowerCase(),
  storeIoc = 'redis',
}, {
  [storeIoc]: redisStore,
  logger,
}) {
  const log = logger.of(`cache:${entity}`, { entity, ttl });
  log.info('initiated');

  return {
    get: async id => {
      const value = await redisStore
        .get(keyFor(id))
        .then(value => value && JSON.parse(value));
      log.info({ id, value }, 'retrieved from cache');
      return value;
    },
    set: (id, value) => {
      log.info({ id, value }, 'storing in cache');
      return ttl
        ? redisStore.set(keyFor(id), JSON.stringify(value), { EX: ttl })
        : redisStore.set(keyFor(id), JSON.stringify(value));
    },
    remove: id => {
      log.info({ id }, 'removing from cache');
      return redisStore.del(keyFor(id));
    },
  };
};

export function validate(options, ioc) {
  const { entity, storeIoc = 'redis' } = options;
  const { errors: { configError }, ms = msLibrary, [storeIoc]: redisStore } = ioc;

  if ('string' == typeof options.ttl) options.ttl = ms(options.ttl) / 1000;
  const { ttl } = options;

  const reject = !(entity && 'string' == typeof entity) && 'options.entity is not a entity-name string'
        || !(!ttl || 'number' == typeof ttl && ttl > 0) && 'options.ttl is not a positive number (of seconds)';

  if (reject) {
    throw configError('Invalid Options for @prun/redis/object-cache', {
      from: '@prun/redis/object-cache',
      reject,
      description: [
        'This is a developer configuration error',
        'This error is thrown when a required option is missing, or any option is provided',
        'with an unsupported value.',
        '@prun/redis/object-cache supports the following options:',
        ' - entity - mandatory string, name of the entities managed by this component',
        ' - ttl - optional number - number of seconds to keep cache results',
        ' - storeIoc - optional iocName, defaults to `redis`',
      ],
      found: { ttl, entity },
      expected: 'valid options',
      actual: reject,
    });
  }

  if (!redisStore) {
    throw configError('Missing dependency', {
      from: '@prun/redis/object-cache',
      description: [
        'This is a developer configuration error',
        'This error appears when the required redis store is not found on the ioc.',
        'This happpens when there is no redis store on the ioc under the name provided',
        'in `options.storeIoc`, which defaults to `redis`',
      ],
      reject: 'cannot find store on ioc',
      missing: storeIoc,
      found: Object.keys(ioc),
    });
  }
};

export const env = {
  entity: ['REDIS_OBJECT_ENTITY'],
  storeIoc: ['REDIS_OBJECT_STORE_IOC'],
};
