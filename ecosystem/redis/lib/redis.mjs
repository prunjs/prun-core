import { createClient as redisCreateClient } from 'redis';

export default async function redisFactory(conf, {
  bus,
  logger,
  redis: { createClient } = { createClient: redisCreateClient },
}) {
  const log = logger.of('redis');
  log.info('initializing');

  log.info('connecting');
  const client = createClient(conf).on('error', err => {
    if (err.message.match(/ERR AUTH.*called without.*password configured/)) return;

    bus.emit('error', err);
  });
  await client.connect();

  await client.ping();
  log.info('connected');

  bus.on('shutdown', async () => {
    log.info('shutting down');
    //istanbul ignore else
    if (client.connected) client.unref();

    await client.disconnect();
    log.info('closed connection');
  });
  return client;
}

export function validate(options, {
  errors: { configError },
}) {
  const { url } = options;
  const isStr = v => v && 'string' == typeof v;
  const urlOptionPath = 'options.url';

  const reject = !isStr(url) && `${urlOptionPath} is not a string`
    || !url.startsWith('redis') && `${urlOptionPath} must be a redis connection string`;

  if (reject) {
    throw configError('Invalid Redis Options', {
      description: [
        'this is a configuration error.',
        'this error is thrown when invalid options are provided to the `@prun/redis` module',
        'valid options are:',
        ' - url - a redis connection string',
      ],
      reject,
      options, //TBD: mask password?
    });
  }
  return options;
}

export const env = {
  url: ['REDIS_URL'],
};
