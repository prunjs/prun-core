import { RedisContainer } from '@testcontainers/redis';
import Should from 'should';
import { exec } from 'node:child_process';
import { promisify } from 'node:util';

const execProm = promisify(exec);

describe('end to end', () => {
  describe('when ran with missing .env file', () => {
    const ctx = {};
    before(done => {
      exec(
        'npx prun --logger.module debug --config test/fx/app-crash/.prunrc',
        (err, stdout, stderr) => {
          Object.assign(ctx, { err, stdout, stderr });
          done();
        },
      );
    });

    it('should exit with error, and without hanging', () => {
      Should(ctx.err).be.an.Error()
        .have.property('code', 1);
    });

    it('should propagate the invalid url error', () => {
      Should(ctx.stdout).match(/url must be a redis connection string/);
    });
  });

  describe('when ran with valid redis url a program that pings Redis, stores a key and exits', () => {
    const ctx = {};
    const testKey = 'testKey';
    const testValue = Math.random().toString(36)
      .substr(2, 5);

    let container;
    before(async function before() {
      // Tricky: This is what it takes in the builder
      this.timeout('20s'); // eslint-disable-line no-invalid-this
      container = await new RedisContainer().start();
      try {
        const { stdout, stderr } = await execProm(
          `npx prun --logger.module debug \
          --cmd.key=${testKey} --cmd.value=${testValue} \
          --redis.url=${container.getConnectionUrl()} \
          --objectCache.ttl=5000 \
          --config test/fx/app/.prunrc`,
        );
        Object.assign(ctx, { stdout, stderr });
      } catch (err) {
        Object.assign(ctx, { err });
      }
    });

    it('should not resolve with an error', () => Should.not.exist(ctx.err));

    describe('logged output', () => {
      it(
        'should contain the PONG notice',
        () => Should(ctx.stdout).match(/Pinging redis result: PONG/),
      );


      it(
        'should contain the key=value',
        () => Should(ctx.stdout).match(new RegExp(`${testKey}=${testValue}`)),
      );

      it(
        'should note the correct ttl',
        () => Should(ctx.stdout).match(/ttl: 5000/),
      );

      it(
        'should exit gracefully',
        () => Should(ctx.stdout).match(/INFO running result/),
      );
    });

    after(() => container.stop());
  });
});
