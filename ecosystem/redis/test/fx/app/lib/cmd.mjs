export default ({ key, value }, { logger, objectCache, redis, bus }) => {
  return {
    run: async () => {
      logger.info('Pinging redis...');
      const pong = await redis.ping();
      logger.info(`Pinging redis result: ${pong}`);

      await objectCache.set(key, value);
      const storedValue = await objectCache.get(key);
      logger.info(`${key}=${storedValue}`);

      bus.emit('shutdown');
    },
  };
};
