import { claim, iocFactory } from '@prun/bdd-helper';
import sinon from 'sinon';

import * as SUT from '../lib/object-cache.mjs';

const redis = mockRedis();

describe('@prun/redis/object-cache', () => {
  describe('the exported factory', () => {
    claim(SUT).isPrunFactory({ hasValidator: true });

    const ioc = iocFactory({ redis });

    describe('when called with', () => {
      describe('minimal valid options and ioc', () => {
        claim(SUT, 'default').isPrunFactory();
        claim(SUT, 'default').behavior({
          options: {
            entity: 'candy',
          },
          ioc,
          expect: {
            api: {
              get: 1,
              set: 2,
              remove: 1,
            },
          },
        });
      });
    });
  });

  describe('instance api', () => {
    const ioc = iocFactory({ redis });
    const ctx = {};
    before(() => {
      ctx.SUT = SUT.default({ entity: 'pets' }, ioc);
    });

    describe('.get', () => {
      describe('when called with a key', () => {
        describe('that IS found in the store', () => {
          const pet = { name: 'mooki', specie: 'dog' };
          const petStr = JSON.stringify(pet);
          claim(ctx, 'SUT.get').behavior({
            before: () => {
              redis.reset();
              redis.get.resolves(petStr);
            },
            args: ['mooki'],
            expect: {
              result: {
                'should be the returned entity, parsed from JSON': result => {
                  Should(result).eql(pet);
                },
              },
            },
          });

          it('should call get with the key formatted into redis key', () => {
            Should(redis.get.firstCall.args).eql(['pets:id:mooki']);
          });
        });

        describe('that is NOT found in the store', () => {
          claim(ctx, 'SUT.get').behavior({
            before: () => {
              redis.reset();
              redis.get.resolves(null);
            },
            args: ['bobi'],
            expect: {
              result: {
                'should be null': result => {
                  Should(result).be.Null();
                },
              },
            },
          });

          it('should call get with the key formatted into redis key', () => {
            Should(redis.get.firstCall.args).eql(['pets:id:bobi']);
          });
        });

        describe('and redis client responds with an error', () => {
          const err = new Error('Mock Redis Error');
          claim(ctx, 'SUT.get').behavior({
            before: () => {
              redis.reset();
              redis.get.rejects(err);
            },
            args: ['mooki'],
            expect: propagatedError(err),
          });
        });
      });
    });

    describe('.set', () => {
      describe('when called with a key and value', () => {
        describe('and initiated without ttl', () => {
          describe('and all goes ell', () => {
            const pet = { name: 'mooki', specie: 'dog' };

            claim(ctx, 'SUT.set').behavior({
              before: () => {
                ioc.logger.reset();
                redis.reset();
                redis.set.resolves('OK');
              },
              args: [pet.name, pet],
            });

            it('should emit an info log entry with the set details', () => {
              const entry = ioc.logger.entries
                .of('cache:pets')
                .level('info')
                .msg('storing in cache')
                .pop();
              Should(entry).be.ok();
            });

            it('should set with the key formatted into redis key', () => {
              Should(redis.set.firstCall.args[0]).eql(`pets:id:${pet.name}`);
            });

            it('should set value formatted into JSON', () => {
              Should(redis.set.firstCall.args[1]).eql(JSON.stringify(pet));
            });
          });
        });

        describe('and initiated WITH ttl', () => {
          const ctx = {};
          const ttl = 100 + Math.floor(1000 * Math.random());
          before(() => {
            ctx.SUT = SUT.default({ entity: 'pets', ttl }, ioc);
          });
          describe('and all goes ell', () => {
            const pet = { name: 'mooki', specie: 'dog' };

            claim(ctx, 'SUT.set').behavior({
              before: () => {
                ioc.logger.reset();
                redis.reset();
                redis.set.resolves('OK');
              },
              args: [pet.name, pet],
            });

            it('should emit an info log entry with the set details', () => {
              const entry = ioc.logger.entries
                .of('cache:pets')
                .level('info')
                .msg('storing in cache')
                .pop();
              Should(entry).be.ok();
            });

            it('should set with the key formatted into redis key', () => {
              Should(redis.set.firstCall.args[0]).eql(`pets:id:${pet.name}`);
            });

            it('should set value formatted into JSON', () => {
              Should(redis.set.firstCall.args[1]).eql(JSON.stringify(pet));
            });

            it('should pass TTL to the set command', () => {
              Should(redis.set.firstCall.args.slice(2)).eql([{ EX: ttl }]);
            });
          });
        });

        describe('and redis client responds with an error', () => {
          const err = new Error('Mock Redis Error');
          claim(ctx, 'SUT.set').behavior({
            before: () => {
              redis.reset();
              redis.set.rejects(err);
            },
            args: ['mooki'],
            expect: propagatedError(err),
          });
        });
      });
    });

    describe('.remove', () => {
      describe('when called with a key and value', () => {
        describe('and all goes ell', () => {
          claim(ctx, 'SUT.remove').behavior({
            before: () => {
              ioc.logger.reset();
              redis.reset();
              redis.set.resolves('OK');
            },
            args: ['shooki'],
          });

          it('should emit an info log entry with the cache clear', () => {
            const entry = ioc.logger.entries
              .of('cache:pets')
              .level('info')
              .msg('removing from cache')
              .pop();
            Should(entry).be.ok();
          });

          it('should remove the id formatted into redis key', () => {
            Should(redis.del.firstCall.args).eql(['pets:id:shooki']);
          });
        });

        describe('and redis client responds with an error', () => {
          const err = new Error('Mock Redis Error');
          claim(ctx, 'SUT.remove').behavior({
            before: () => {
              redis.reset();
              redis.del.rejects(err);
            },
            args: ['loki'],
            expect: propagatedError(err),
          });
        });
      });
    });

    function propagatedError(err) {
      return {
        reject: true,
        result: {
          'should propagate the error': result => {
            Should(result).equal(err);
          },
        },
      };
    }
  });

  describe('static api', () => {
    const ioc = iocFactory({ redis });

    describe('.validate(options, ioc)', () => {
      describe('when called with valid options', () => {
        describe('and options are minimal', () => {
          claim(SUT, 'validate').behavior({
            options: {
              entity: 'candy',
            },
            ioc,
          });
        });

        describe('and all options proivded explicitly with custom values', () => {
          claim(SUT, 'validate').behavior({
            options: {
              entity: 'candy',
              ttl: 3600,
              keyFor: id => `candies:${id}`,
              storeIoc: 'redis-candies',
            },
            ioc: iocFactory({
              'redis-candies': redis,
            }),
          });
        });

        describe('with options.ttl expressed in human form', () => {
          const options = {
            entity: 'unplesant-memories',
            ttl: '1h', //<-- its expected to be translated to a number
          };
          claim(SUT, 'validate').behavior({
            options,
            ioc,
          });

          it('should mute options to numeric value of seconds', () => {
            Should(options)
              .have.property('ttl', 3600);
          });
        });
      });

      describe('when called with invalid options or ioc', () => {
        describe('with options.entity not a string', () => {
          claim(SUT, 'validate').behavior({
            options: {
              //entity: missing!
            },
            ioc,
            expect: configReject(/options.entity is not a entity-name string/),
          });
        });

        describe('with invalid options.ttl', () => {
          claim(SUT, 'validate').behavior({
            options: {
              entity: 'unplesant-memories',
              ttl: -99999, //<-- its negative! I don't want to remember them
            },
            ioc,
            expect: configReject(/options.ttl is not a positive number \(of seconds\)/),
          });
        });

        describe('with invalid ioc - without redis', () => {
          claim(SUT, 'validate').behavior({
            options: {
              entity: 'unplesant-memories',
              ttl: -99999, //<-- its negative! I don't want to remember them
            },
            ioc: iocFactory(),
            expect: configReject(/options.ttl is not a positive number \(of seconds\)/),
          });
        });

        describe('with ioc and options that do not agree on the storeIoc', () => {
          claim(SUT, 'validate').behavior({
            options: {
              entity: 'heros',
            },
            ioc: iocFactory({}),
            expect: dependencyReject(/cannot find store on ioc/),
          });
        });

        function configReject(reject) {
          return expectReject(/Invalid Options/, reject, ['ttl', 'entity']);
        }

        function dependencyReject(reject) {
          return expectReject(/Missing dependency/, reject, Object);
        }

        function expectReject(errMsg, reject, found) {
          return {
            reject: errMsg,
            props: {
              from: /@prun\/redis.object-cache/,
              reject,
              description: Array,
              found,
            },
          };
        }
      });
    });
  });
});


function mockRedis() {
  const redis = {
    get: sinon.stub().resolves(null),
    set: sinon.stub().resolves(null),
    del: sinon.stub().resolves(null),
    ping: sinon.stub().resolves('PONG'),
    reset: () => {
      redis.get.reset();
      redis.set.reset();
      redis.del.reset();
    },
  };
  return redis;
}
