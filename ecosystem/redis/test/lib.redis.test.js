import { claim, iocFactory } from '@prun/bdd-helper';
import { EventEmitter } from 'node:events';
import sinon from 'sinon';

import * as SUT from '../lib/redis.mjs';

describe('@prun/redis', () => {
  describe('exported factory', () => {
    claim(SUT).isPrunFactory({
      hasValidator: true,
      mapsEnv: ['url'],
    });

    describe('when called', () => {
      const createClient = sinon.stub();
      const ioc = iocFactory({
        redis: { createClient },
      });
      describe('and initiation goes well', () => {
        const mockClient = mockRedis();
        claim(SUT, 'default').behavior({
          ioc,
          before: () => createClient.returns(mockClient),
        });

        describe('and shutdown signal is sent', () => {
          before(done => {
            mockClient.disconnect = () => {
              mockClient.quitted = true;
              done();
            };
            ioc.bus.emit('shutdown');
          });

          it('should quit', () => {
            Should(mockClient.quitted).eql(true);
          });
          it('should unref the client', () => {
            Should(mockClient.unref.called).be.True();
          });
        });

        describe('and redis emits error', () => {
          const mockClient = mockRedis();
          const ctx = claim(SUT, 'default').behavior({
            ioc,
            before: () => createClient.returns(mockClient),
            awaiting: () => mockClient.emit('connect'),
          });

          ioc.bus.on('error', err => {
            ctx.err = err;
          });
          describe('and error is about unecessary AUTH command', () => {
            before(() => {
              ctx.err = null;
              mockClient.emit('error', new Error('ERR AUTH is called without any password configured'));
            });
            it('should suppress the error', () => {
              Should.not.exist(ctx.err);
            });
          });

          describe('and error is a real error', () => {
            const mockError = new Error('oupsy dazy');
            before(() => {
              ctx.err = null;
              mockClient.emit('error', mockError);
            });
            it('should propagate the error to the bus', () => {
              Should(ctx.err).equal(mockError);
            });
          });
        });
      });
      describe('and connection fails', () => {
        const mockClient = mockRedis();
        const mockError = new Error('The Underlying Error');
        mockClient.ping = sinon.stub().rejects(mockError);
        claim(SUT, 'default').behavior({
          ioc,
          before: () => createClient.returns(mockClient),
          expect: {
            reject: /The Underlying Error/,
          },
        });
      });
    });

    function mockRedis() {
      const mockClient = new EventEmitter();
      mockClient.connected = true;
      mockClient.unref = sinon.stub();
      mockClient.disconnect = sinon.stub();
      mockClient.connect = sinon.stub().resolves(mockClient);
      mockClient.ping = sinon.stub().resolves('PONG');
      return mockClient;
    }
  });

  describe('static apis', () => {
    describe('.validate(options, ioc)', () => {
      const ioc = iocFactory({});
      claim(SUT, 'validate').behaviors([{
        title: 'with minimal valid options',
        options: {
          url: 'redis://localhost',
        },
        ioc,
      }, {
        title: 'without url',
        ioc,
        options: {
          //url - missing
        },
        expect: {
          reject: /Invalid Redis Options/,
          props: {
            name: /PrunConfigError/,
            description: Array,
            options: Object,
            reject: /options.url is not a string/,
          },
        },
      }, {
        title: 'with url not starting with \'redis\'',
        ioc,
        options: {
          url:'http://localhost',
        },
        expect: {
          reject: /Invalid Redis Options/,
          props: {
            name: /PrunConfigError/,
            description: Array,
            options: Object,
            reject: /options.url must be a redis connection string/,
          },
        },

      }]);
    });
  });
});
