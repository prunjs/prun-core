# npm-package-example

Example of npm-pacakge with tests and coverage to develop the CI with.


# Build lifecycle

## Overview
 
1. checkout
2. `npm install`
3. versioning
4. `npm test`
5. `npm run cover`
6. `npm run lint`
7. `npm publish`

## checkout
checkout of the repo to the worker workspace

## npm install

get npm packages

## versioning

A builder step that checks the version in your `package.json` and makes sure it's publishable.

Semver dictates that packages can be published only once - once a version is published it cannot be changed.
For this - we need to promote the version in `package.json`.

The version checked in as part of `package.json` must specify the patch numner as '0'. e.g. - 1.2.0, 2.2.0, 6.3.0.
The build step should find the next available patch inside your major/minor, and update it in the `package.json`.

We may use this step to provide more ops services, such as static-code analysis and security dependency scans.

## test
This step must exit well, or the build fails.
You may build this step in segments, as long as they are all approachaged through this single entry-point.

## cover
This step must exit well, or the build fails.
You may build this step in segments, as long as they are all approachaged through this single entry-point.

**It's also expected to leave a `coverage` directory that will be collected as artifact.**

## lint
This step must exit well, or the build fails.

## publish
If all ended well and all tests have passed - the package will be published under the version computed in the 2nd stage.
 
 

 

