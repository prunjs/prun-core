module.exports = () => ({
  add(...a) { return a.reduce((a, b) => a + b, 0); },
  factor(...a) { return a.reduce((a, b) => a * b, 1); },
});
