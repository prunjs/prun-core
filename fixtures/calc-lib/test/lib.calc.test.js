import calcFactory from '../lib/calc.js';

const SUT = calcFactory();

describe('lib/calc', () => {
  describe('.add(...n)', () => {
    it('should be a method', () => {
      Should(SUT).have.property('add').Function();
    });
    describe('when called with a set of numbers - should return their sum', () => {
      [{
        args: [1, 2, 3, 4],
        expect: 10,
      }, {
        args: [-1, -2, -3, -4],
        expect: -10,
      }, {
        args: [-1, 2, -3, 4],
        expect: 2,
      }].forEach(({ args, expect }) => {
        it(`calc.add(${args}) --> ${expect}`, () => {
          Should(SUT.add(...args)).eql(expect);
        });
      });
    });
  });

  describe('.factor(...n)', () => {
    it('should be a method', () => {
      Should(SUT).have.property('factor').Function();
    });
    describe('when called with a set of numbers - should return their factor', () => {
      [{
        args: [1, 2, 3, 4],
        expect: 24,
      }, {
        args: [-1, -2, -3, 4],
        expect: -24,
      }, {
        args: [-1, 2, -3, 4],
        expect: 24,
      }].forEach(({ args, expect }) => {
        it(`calc.factor(${args}) --> ${expect}`, () => {
          Should(SUT.factor(...args)).eql(expect);
        });
      });
    });
  });
});
